<?
ob_start();
session_start();
$type_user = $_SESSION['usertype'];

if ($type_user != "teacher") {
    header("Location:user_login.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="EN" lang="EN" dir="ltr"><!-- InstanceBegin template="/Templates/index_full.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head profile="http://gmpg.orgs/xfn/11">
<title>ระบบสารสนเทศฝึกประสบการณ์วิชาชีพฯ :<?php echo $_SESSION['usertype']; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="imagetoolbar" content="no" />
<link rel="stylesheet" href="css/layout.css" type="text/css" />
<link rel="stylesheet" href="css/layout.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/style-mix.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="style.css" />
   
        
        <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="jquery-1.7.2.min.js"></script>
     

</head>
<body id="top">
<div class="wrapper row1">
  <div id="header" class="clear">
    <div class="fl_left">
    <p><img src="img/logo5.png" width="80" height="100"></p>
    </div>
      <div class="fl_center">
      <div class="fl_right" align="right">
      <ul>
        <li><a href="index.php">หนัาหลัก</a></li>
        <li><a href="Webboard.php">เว็บบอร์ด</a></li>
         <?php if(!$_SESSION['username']){?>
        <li><a href="user_login.php">เข้าสู่ระบบ</a></li>
        <li><a href="submit3.php">สมัครสมาชิก</a></li>
        <?php } else { ?>
        <li class="last"><a href="user_logout.php">ออกจากระบบ</a></li>
        <?php } ?>
      </ul>
    </div>
      <br>
      <br>
      <h1>&nbsp;&nbsp;ระบบสารสนเทศฝึกประสบการณ์วิชาชีพ</h1>
      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;สาขาวิทยาการคอมพิวเตอร์และเทคโนโลยีสารสนเทศ</p>
    </div>
 
  </div>
</div>

<div class="wrapper row2">
  <div id="topnav">
                                    <div class="row-fluid">
                                        <div class="span9">
                                            <ul>

                                        <!-- admin -->
                                        <? if ($_SESSION["usertype"] == "admin") { ?>
                                            <li><a href="show_std.php">จัดการข้อมูลนักศึกษา</a></li>
                                            <li><a href="show_teacher.php">จัดการข้อมูลอาจารย์</a></li>
                                            <li><a href="show_company.php">จัดการข้อมูลหน่วยงาน</a></li>

                                            <!-- company -->
                                        <? } else if ($_SESSION["usertype"] == "company") { ?>
                                         <li><a href="company_detail.php">ข้อมูลหน่วยงาน</a></li>
                                            <li><a href="show_request_company.php">แจ้งความจำนงค์รับนักศึกษา</a></li>
                                            <li><a href="report_StdResume.php">รายงานประวัติส่วนตัวของนักศึกษา</a></li>
                                            <li><a href="manage_score_forcompany.php">กรอกข้อมูลประเมินผล</a></li>

                                        <? } else if ($_SESSION["usertype"] == "officer") { ?>

                                            <!-- officer -->
                                            <li><a href="#">จัดการข้อมูลหนังสือฝึกประสบการณ์วิชาชีพ</a>
                                                <ul>
                                                    <li><a href="manage_send_request.php">หนังสือขอความอนุเคราะห์ฝึกประสบการณ์วิชาชีพ</a></li>
                                                    <li><a href="manage_sendSupervision.php">หนังสือนิเทศนักศึกษา</a></li>
                                                    <li><a href="manage_sendStd.php">หนังสือส่งตัวนักศึกษา</a></li>
                                                    <li><a href="manage_sendStd1.php">หนังสือขอตัวนักศึกษา</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">จัดการข้อมูลหนังสือโครงการ</a>
                                                <ul>
                                                    <li><a href="manage_sendProject.php">โครงการปฐมนิเทศก่อนฝึกประสบการณ์</a></li>
                                                    <li><a href="manage_sendSamana.php">โครงการสัมนาหลังฝึกประสบการณ์</a></li>
                                                </ul>
                                            </li>

                                        <? } else if ($_SESSION["usertype"] == "student") { ?>
                                            <!-- student -->
                                            <li><a href="resume2.php">ประวัติส่วนตัว</a></li>
                                            <?php 
											 include "connect2.php";
										   $sql = "select * from register,student where student.std_id = register.std_id and student.std_id = '".$_SESSION['std_id']."'  ";
										   $rs = mysql_query($sql);
										   $row = mysql_fetch_array($rs);
										   $num_rows = mysql_num_rows($rs);
										   
										   $position = $row['position']; 
										   
											if($num_rows != 0){?>
												
                                            <li><a href="StdRequest2.php">ส่งคำร้องขอฝึกประสบการณ์</a></li>
                                            <li><a href="result_request.php">ผลอนุมัติการฝึกประสบการณ์</a></li>
                                            
                                            <?php  if ($position == 3){?>
                                            
                                            <li><a href="show_diary.php">บันทึกประจำวัน</a></li>
                                            
                                            <?php }
											} ?>

                                        <? } else if ($_SESSION["usertype"] == "teacher_general") { ?>
                                            <!-- teacher_general -->
                                            <li><a href="show_supervision_forTG.php">ตารางการนิเทศ</a></li>
                                            <li><a href="show_std_TG.php">ข้อมูลนักศึกษาฝึกประสบการณ์</a></li>
                                            <li><a href="show_company_TG.php">ข้อมูลแหล่งฝึกประสบการณ์</a></li>
                                            <li><a href="show_news_forteachergeneral.php">จัดการข่าวประชาสัมพันธ์</a></li>

                                        <? } else if ($_SESSION["usertype"] == "teacher") { ?>
                                            <!-- teacher -->
                                            <li><a href="#">จัดการข้อมูลทั่วไป</a>
                                                <ul>
                                                    <li><a href="show_std_forteacher.php">ข้อมูลนักศึกษา</a></li>
                                                    <li><a href="show_company_forteacher.php">ข้อมูลแหล่งฝึกประสบการณ์</a></li>
                                                    <li><a href="show_project.php">ข้อมูลโครงการ</a></li>
                                                    <li><a href="show_group.php">ข้อมูลหมู่เรียน</a></li>
                                                    <li><a href="register_new3.php">จัดการหมู่เรียนนักศึกษา</a></li>
                                                      <li><a href="manage_score.php">กรอกข้อมูลประเมินผล</a></li>
                                                          <li><a href="show_titlescore.php">ข้อมูลหัวข้อการประเมินผล</a></li>
                                                    <li><a href="show_news.php">ข้อมูลข่าวประชาสัมพันธ์</a></li>
                                                    <li><a href="show_webboard.php">ข้อมูลกระทู้</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">จัดการข้อมูลการนิเทศ</a>
                                                <ul>
                                                    <li><a href="show_supervision2.php">ตารางการนิเทศนักศึกษา</a></li>
                                                    <li><a href="show_advice.php">ปัญหาที่พบและข้อเสนอแนะจากการนิเทศนักศึกษา</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="confirmRQ.php">อนุมัติแหล่งฝึกงาน</a></li>
                                            <li><a href="company_comfirmRQ.php">เปลี่ยนสถานะตอบรับการฝึก</a></li>
                                            <li><a href="#">ดูรายงาน</a>
                                                <ul>
                                                 <li><a href="show_std_mange.php">ดูรายชื่อนักศึกษา</a></li>
                                                    <li><a href="report_std+company.php">รายชื่อแหล่งฝึกประสบการณ์
                                                            พร้อมทั้งชื่อนักศึกษา</a></li>
                                                             <li><a href="show_request.php">การแจ้งความจำนงขอรับนักศึกษาฝึกประสบการณ์วิชาชีพ
                                                            </a></li>
                                                    <li><a href="Report_Supervision.php">ตารางนิเทศนักศึกษา</a></li>
                                                    <li><a href="report_diary.php">บันทึกประจำวันของนักศึกษา</a></li>
                                                    <li><a href="report_advice.php">ปัญหาที่พบและ
                                                            ข้อเสนอแนะจากหน่วยงาน</a></li>
                                                    <li><a href="show_stdScore.php">ผลการประเมินจากหน่วยงาน</a></li>
                                                    <li><a href="Report_Score.php">คะแนนของนักศึกษา</a></li>
                                                    <li><a href="Report_ScoreSupervision.php">คะแนนการนิเทศ</a></li>
                                                    <li><a href="Report_ScoreDocument.php">คะแนนการดำเนินงานเอกสาร</a></li>
                                                    <li><a href="Report_ScoreActivity.php">คะแนนเข้าร่วมกิจกรรม</a></li>
                                                    <li><a href="Report_ScoreSendDoc.php">คะแนนการส่งเอกสาร</a></li>
                                                    <li><a href="report_grade.php">ผลการประเมิน</a></li>
                                                </ul>
                                            </li>

                                        <? } else { ?>
                                            <li><a href="index.php">หน้าหลัก</a></li>
                                            <li><a href="Webboard.php">เว็บบอร์ด</a></li>
                                            <?php if (!$_SESSION['username']) { ?>
                                                <li><a href="user_login.php">เข้าสู่ระบบ</a></li>
                                                <li><a href="submit3.php">สมัครสมาชิก</a></li>
                                            <?php } else { ?>
                                                <li><a href="user_logout.php">ออกจากระบบ</a></li>
                                                </li><?php } ?>

                                        <? } ?>

                                    </ul>
                                        </div>
                                        <div class="span3" style="height: 50px" align="right">
                                            <?php
                                            if($_SESSION['username']) {
                                            ?>
                                            <p style="margin-top: 16px;">ยินดีต้อนรับ คุณ  <?= $_SESSION['username'] ?> </p>
                                                <?php
                                            }
                                                ?>
                                        </div>
                                       
                                    </div>
    <div  class="clear"></div>
  </div>
</div>

<div class="wrapper row4">
  <div id="container" class="clear">
  
  <!-- InstanceBeginEditable name="center_body" -->
                                    <div align="center"><meta http-equiv="Content-Type" content="text/html; charset=utf-8" ></meta>


                                        <div align="center"><strong>จัดการหมู่เรียนนักศึกษา</strong></div>
                                        <br>
                                            <br>
                                                <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
                                                <script type="text/javascript">
    
                                                    function select_grp()
                                                    {
                                                        $.ajax({
                                                            url:'ajax.php',
                                                            data: 'grp_no='+$('#grp_no').val(),
                                                            success: function(html){
                                                                $('#select_grp').html();
                                                                $('#select_grp').html(html);
                                                            }
                                                        });
		
                                                        $.ajax({
                                                            url:'ajax2.php',
                                                            data: 'grp_no='+$('#grp_no').val(),
                                                            success: function(html){
                                                                $('#show_grp').html();
                                                                $('#show_grp').html(html);
                                                            }
                                                        });
                                                    }
    
                                                    function add_std(label)
                                                    {
                                                        $('#label'+label).html('<p><input type="text" name="std_id[]" id="std_id'+label+'" onkeyup="searchSTD('+label+')"/>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="std_name'+label+'">กรุณากรอกรหัสนักศึกษา</span> <input type="button" value="เคลียร์" onclick="clear_std('+label+')"/></p><span id="label'+(label+1)+'"></p>');
                                                        $('#add_btn').attr('onclick','add_std('+(label+1)+')');
                                                    }

                                                    function clear_std(label)
                                                    {
                                                        $("#std_id"+label).val('');
                                                        $("#std_name"+label).val('');
                                                    }
	
                                                    function del_std(std_id)
                                                    {
                                                        if(confirm("ยืนยันการลบ?")){
                                                            $.ajax({
                                                                url:'ajax3.php',
                                                                data: 'std_id='+std_id+'&grp_no='+$('#grp_no').val(),
                                                                success: function(html){
                                                                    $('#'+std_id).html();
                                                                    $('#'+std_id).html(html);
                                                                }
                                                            });
                                                        }
                                                    }
        
                                                    function searchSTD(label)
                                                    {
                                                        var std_id = $("#std_id"+label).val();
         
        

                                        
                                                        $.ajax({
                                                            url:'ajax4.php',
                                                            data: 'std_id='+std_id,
                                                            success: function(html){
                                                                $('#std_name'+label).html();
                                                                $('#std_name'+label).html(html);
                                                            }
                                                        });
                                                    }
                                                </script>


                                                <em></em>
                                                <?php
                                                include("connect2.php");

                                                if ($_POST['submit']) {



                                                    for ($i = 0; $i < sizeof($_POST['std_id']); $i++) {//เช็ครหัสนักศึกษาซ้ำ
                                                        $sql = "SELECT * FROM register WHERE std_id = '" . $_POST['std_id'][$i] . "' ";
                                                        $rs = mysql_query($sql);



                                                        if (!mysql_num_rows($rs)) {


                                                            $sql = "INSERT INTO `register` (
`register_id`,
`position` ,
`std_id` ,
`groups_num` ,
`company_id`
)
VALUES (
NULL ,  '0',  '" . $_POST['std_id'][$i] . "',  '" . $_POST['grp_no'] . "', 0
);";
                                                            $rs = mysql_query($sql);
                                                            /* 	 $row = mysql_fetch_array($rs);

                                                              if($row != 0)
                                                              {
                                                              echo "<script>
                                                              alert('ชื่อนี้มีอยู่แล้ว' );
                                                              history.back();
                                                              </script>";

                                                              } */
                                                            if ($rs) {
                                                                ?>
                                                                <script>alert('ข้อมูลถูกบันทึกแล้ว' );
                                                                    //                                                                    window.location='register_new3.php';
                                                                </script>
                                                                <?php
                                                            } else {
                                                                echo mysql_error();
                                                            }
                                                        } else {
                                                            ?>
                                                            <script>
                                                                alert('รหัส : <?= $_POST['std_id'][$i] ?> ลงทะเบียนซ้ำ');
                                                                //                                                                    window.location='register_new3.php';
                                                            </script>
                                                            <?php
                                                        }
                                                    }
                                                }

                                                $sql = "SELECT * FROM groups  ";
                                                $rs = mysql_query($sql);

//$row = mysql_fetch_array($rs);
                                                ?>
                                                <form method="post" action="">
                                                    <table width="101%" height="176">
                                                        <tr>
                                                            <td width="16%">เลขที่หมู่</td><td width="3%">:</td>
                                                            <td width="33%">
                                                                <select id="grp_no"  name="grp_no" onChange="select_grp()">
                                                                    <option value="" selected="selected">กรุณาเลือกเลขที่หมู่</option>
<?php while ($row = mysql_fetch_array($rs)) { ?>
                                                                        <option value="<?= $row['groups_num'] ?>"><?= $row['groups_num'] ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </td>
                                                            <td width="15%">หมู่</td><td width="3%">:</td>
                                                            <td width="30%"><input name="grp_name" type="text" id="grp_name" size="15" readonly="readonly"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>รหัสวิชา</td>
                                                            <td>:</td>
                                                            <td><input name="sub_id" type="text" id="sub_id" size="15" readonly="readonly"/></td>
                                                            <td>ชื่อวิชา</td>
                                                            <td>:</td>
                                                            <td><input type="text" name="sub_name" id="sub_name" readonly="readonly"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>ปีการศึกษา</td>
                                                            <td>:</td>
                                                            <td><input name="year" type="text" id="year" size="15" readonly="readonly"/></td>
                                                            <td>ภาคเรียนที่</td>
                                                            <td>:</td>
                                                            <td><input name="term" type="text" id="term" size="10" readonly="readonly"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>สาขา</td><td>:</td><td><input type="text" name="major" id="major" readonly="readonly"/></td>
                                                            <td>&nbsp;</td>
                                                            <td>&nbsp;</td><td>&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                    <br>
                                                        <div align="center"><strong>รายชื่อนักศึกษา</strong></div>
                                                        <br>
                                                            <br>
                                                                <span id="select_grp"></span>
                                                                <span id="show_grp"></span>
                                                                <br>
                                                                    
                                                                    <div >
                                                                        <div >
                                                                            <a href="show-student" target="_bank" class="btn">รายชื่อนักศึกษาที่ยังไม่ลงทะเบียน</a>
                                                                        </div>
                                                                    </div>
                                                                    <br>

                                                                    <p>
                                                                        <input type="text" name="std_id[]" id="std_id0" onkeyup="searchSTD(0)"/>
                                                                        &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <span id="std_name0">กรุณากรอกรหัสนักศึกษา</span>
                                                                        <input type="button" value="เคลียร์" onClick="clear_std(0)"/>
                                                                        <input type="button" value="เพิ่มรายชื่อ" onClick="add_std(1)" id="add_btn"/>
                                                                    </p>
                                                                    <span id="label1"></span>
                                                                    <input type="submit" name="submit" value="เพิ่มข้อมูล"/>
                                                                    </form></div>
                                                                    <!-- InstanceEndEditable --></div>
</div>


<div class="wrapper">
  <div id="copyright" class="clear">
  <div style="width:960px;margin:0 auto; ">
    <p class="fl_left">Copyright (c) 2012 <a href = "http://webhosting.udru.ac.th/~std52040249439" target="_blank">http://www.gen-dev-soft.com/experience-csit/</a> All rights reserved. 
<br>
Design by Nittaya Kakulphin & Benjawan Sriralat @ Udonthani Rajabhat University.</p>
    </div>
  </div>
</div>
</body>
<!-- InstanceEnd --></html>
