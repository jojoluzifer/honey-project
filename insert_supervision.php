<?
ob_start();
session_start();
$type_user = $_SESSION['usertype'];

if ($type_user != "teacher") {
//echo "กรุณาล๊อกอินเข้าสู่ระบบก่อน!";
    header("Location:user_login.php");
//echo "<a href = login_user.php>หน้าล๊อกอิน</a>";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="EN" lang="EN" dir="ltr"><!-- InstanceBegin template="/Templates/index_full.dwt.php" codeOutsideHTMLIsLocked="false" -->
    <head profile="http://gmpg.orgs/xfn/11">
        <title>ระบบสารสนเทศฝึกประสบการณ์วิชาชีพฯ :<?php echo $_SESSION['usertype']; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="imagetoolbar" content="no" />
        <link rel="stylesheet" href="css/layout.css" type="text/css" />
        <link rel="stylesheet" href="css/layout.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/style-mix.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="style.css" />


        <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="jquery-1.7.2.min.js"></script>


    </head>
    <body id="top">
        <div class="wrapper row1">
            <div id="header" class="clear">
                <div class="fl_left">
                    <p><img src="img/logo5.png" width="80" height="100"></p>
                </div>
                <div class="fl_center">
                    <div class="fl_right" align="right">
                        <ul>
                            <li><a href="index.php">หนัาหลัก</a></li>
                            <li><a href="Webboard.php">เว็บบอร์ด</a></li>
                            <?php if (!$_SESSION['username']) { ?>
                                <li><a href="user_login.php">เข้าสู่ระบบ</a></li>
                                <li><a href="submit3.php">สมัครสมาชิก</a></li>
                            <?php } else { ?>
                                <li class="last"><a href="user_logout.php">ออกจากระบบ</a></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <br>
                        <br>
                            <h1>&nbsp;&nbsp;ระบบสารสนเทศฝึกประสบการณ์วิชาชีพ</h1>
                            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;สาขาวิทยาการคอมพิวเตอร์และเทคโนโลยีสารสนเทศ</p>
                            </div>

                            </div>
                            </div>

                            <div class="wrapper row2">
                                <div id="topnav">
                                    <div class="row-fluid">
                                        <div class="span9">

                                            <ul>
                                        <!-- admin -->
                                        <? if ($_SESSION["usertype"] == "admin") { ?>
                                            <li><a href="show_std.php">จัดการข้อมูลนักศึกษา</a></li>
                                            <li><a href="show_teacher.php">จัดการข้อมูลอาจารย์</a></li>
                                            <li><a href="show_company.php">จัดการข้อมูลหน่วยงาน</a></li>

                                            <!-- company -->
                                        <? } else if ($_SESSION["usertype"] == "company") { ?>
                                         <li><a href="company_detail.php">ข้อมูลหน่วยงาน</a></li>
                                            <li><a href="show_request_company.php">แจ้งความจำนงค์รับนักศึกษา</a></li>
                                            <li><a href="report_StdResume.php">รายงานประวัติส่วนตัวของนักศึกษา</a></li>
                                            <li><a href="manage_score_forcompany.php">กรอกข้อมูลประเมินผล</a></li>

                                        <? } else if ($_SESSION["usertype"] == "officer") { ?>

                                            <!-- officer -->
                                            <li><a href="#">จัดการข้อมูลหนังสือฝึกประสบการณ์วิชาชีพ</a>
                                                <ul>
                                                    <li><a href="manage_send_request.php">หนังสือขอความอนุเคราะห์ฝึกประสบการณ์วิชาชีพ</a></li>
                                                    <li><a href="manage_sendSupervision.php">หนังสือนิเทศนักศึกษา</a></li>
                                                    <li><a href="manage_sendStd.php">หนังสือส่งตัวนักศึกษา</a></li>
                                                    <li><a href="manage_sendStd1.php">หนังสือขอตัวนักศึกษา</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">จัดการข้อมูลหนังสือโครงการ</a>
                                                <ul>
                                                    <li><a href="manage_sendProject.php">โครงการปฐมนิเทศก่อนฝึกประสบการณ์</a></li>
                                                    <li><a href="manage_sendSamana.php">โครงการสัมนาหลังฝึกประสบการณ์</a></li>
                                                </ul>
                                            </li>

                                        <? } else if ($_SESSION["usertype"] == "student") { ?>
                                            <!-- student -->
                                            <li><a href="resume2.php">ประวัติส่วนตัว</a></li>
                                            <?php 
											 include "connect2.php";
										   $sql = "select * from register,student where student.std_id = register.std_id and student.std_id = '".$_SESSION['std_id']."'  ";
										   $rs = mysql_query($sql);
										   $row = mysql_fetch_array($rs);
										   $num_rows = mysql_num_rows($rs);
										   
										   $position = $row['position']; 
										   
											if($num_rows != 0){?>
												
                                            <li><a href="StdRequest2.php">ส่งคำร้องขอฝึกประสบการณ์</a></li>
                                            <li><a href="result_request.php">ผลอนุมัติการฝึกประสบการณ์</a></li>
                                            
                                            <?php  if ($position == 3){?>
                                            
                                            <li><a href="show_diary.php">บันทึกประจำวัน</a></li>
                                            
                                            <?php }
											} ?>

                                        <? } else if ($_SESSION["usertype"] == "teacher_general") { ?>
                                            <!-- teacher_general -->
                                            <li><a href="show_supervision_forTG.php">ตารางการนิเทศ</a></li>
                                            <li><a href="show_std_TG.php">ข้อมูลนักศึกษาฝึกประสบการณ์</a></li>
                                            <li><a href="show_company_TG.php">ข้อมูลแหล่งฝึกประสบการณ์</a></li>
                                            <li><a href="show_news_forteachergeneral.php">จัดการข่าวประชาสัมพันธ์</a></li>

                                        <? } else if ($_SESSION["usertype"] == "teacher") { ?>
                                            <!-- teacher -->
                                            <li><a href="#">จัดการข้อมูลทั่วไป</a>
                                                <ul>
                                                    <li><a href="show_std_forteacher.php">ข้อมูลนักศึกษา</a></li>
                                                    <li><a href="show_company_forteacher.php">ข้อมูลแหล่งฝึกประสบการณ์</a></li>
                                                    <li><a href="show_project.php">ข้อมูลโครงการ</a></li>
                                                    <li><a href="show_group.php">ข้อมูลหมู่เรียน</a></li>
                                                    <li><a href="register_new3.php">จัดการหมู่เรียนนักศึกษา</a></li>
                                                      <li><a href="manage_score.php">กรอกข้อมูลประเมินผล</a></li>
                                                          <li><a href="show_titlescore.php">ข้อมูลหัวข้อการประเมินผล</a></li>
                                                    <li><a href="show_news.php">ข้อมูลข่าวประชาสัมพันธ์</a></li>
                                                    <li><a href="show_webboard.php">ข้อมูลกระทู้</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">จัดการข้อมูลการนิเทศ</a>
                                                <ul>
                                                    <li><a href="show_supervision2.php">ตารางการนิเทศนักศึกษา</a></li>
                                                    <li><a href="show_advice.php">ปัญหาที่พบและข้อเสนอแนะจากการนิเทศนักศึกษา</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="confirmRQ.php">อนุมัติแหล่งฝึกงาน</a></li>
                                            <li><a href="company_comfirmRQ.php">เปลี่ยนสถานะตอบรับการฝึก</a></li>
                                            <li><a href="#">ดูรายงาน</a>
                                                <ul>
                                                 <li><a href="show_std_mange.php">ดูรายชื่อนักศึกษา</a></li>
                                                    <li><a href="report_std+company.php">รายชื่อแหล่งฝึกประสบการณ์
                                                            พร้อมทั้งชื่อนักศึกษา</a></li>
                                                             <li><a href="show_request.php">การแจ้งความจำนงขอรับนักศึกษาฝึกประสบการณ์วิชาชีพ
                                                            </a></li>
                                                    <li><a href="Report_Supervision.php">ตารางนิเทศนักศึกษา</a></li>
                                                    <li><a href="report_diary.php">บันทึกประจำวันของนักศึกษา</a></li>
                                                    <li><a href="report_advice.php">ปัญหาที่พบและ
                                                            ข้อเสนอแนะจากหน่วยงาน</a></li>
                                                    <li><a href="show_stdScore.php">ผลการประเมินจากหน่วยงาน</a></li>
                                                    <li><a href="Report_Score.php">คะแนนของนักศึกษา</a></li>
                                                    <li><a href="Report_ScoreSupervision.php">คะแนนการนิเทศ</a></li>
                                                    <li><a href="Report_ScoreDocument.php">คะแนนการดำเนินงานเอกสาร</a></li>
                                                    <li><a href="Report_ScoreActivity.php">คะแนนเข้าร่วมกิจกรรม</a></li>
                                                    <li><a href="Report_ScoreSendDoc.php">คะแนนการส่งเอกสาร</a></li>
                                                    <li><a href="report_grade.php">ผลการประเมิน</a></li>
                                                </ul>
                                            </li>

                                        <? } else { ?>
                                            <li><a href="index.php">หน้าหลัก</a></li>
                                            <li><a href="Webboard.php">เว็บบอร์ด</a></li>
                                            <?php if (!$_SESSION['username']) { ?>
                                                <li><a href="user_login.php">เข้าสู่ระบบ</a></li>
                                                <li><a href="submit3.php">สมัครสมาชิก</a></li>
                                            <?php } else { ?>
                                                <li><a href="user_logout.php">ออกจากระบบ</a></li>
                                                </li><?php } ?>

                                        <? } ?>

                                    </ul>

                                        </div>
                                        <div class="span3" style="height: 50px" align="right">
                                            <?php
                                            if ($_SESSION['username']) {
                                                ?>
                                                <p style="margin-top: 16px;">ยินดีต้อนรับ คุณ  <?= $_SESSION['username'] ?> </p>
                                                <?php
                                            }
                                            ?>
                                        </div>

                                    </div>
                                    <div  class="clear"></div>
                                </div>
                            </div>

                            <div class="wrapper row4">
                                <div id="container" class="clear">

                                    <!-- InstanceBeginEditable name="center_body" -->


                                    <?php
                                    include ("connect2.php");
                                    ?>
                                    <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
                                    <script type="text/javascript">
                                        
                                        function select_com(){
                                            
                                            $.ajax({
                                                
                                                url:'ajax7.php',
                                                data: 'company_id='+ $('#company_id').val(),
                                                success: function(html){
                                                    
                                                    $('#select_com').html();
                                                    $('#select_com').html(html);
                                                }
                                        
                                            });
                                            
                                            $.ajax({
                                                url: 'ajax8.php',
                                                data: 'company_id='+$('#company_id').val(),
                                                success: function(html){
                                                    $('#list_teacher').html();
                                                    $('#list_teacher').html(html);
                                                }
                                            
                                            });
                                            
                                            $.ajax({
                                                url: 'ajax11.php',
                                                data: 'company_id='+$('#company_id').val(),
                                                success: function(html){
                                                    $('#chk_date').html();
                                                    $('#chk_date').html(html);
                                                }
                                                
                                            });
                                            
                                        }
                                        
                                        function del_t(t_id){
                                            
                                            if(confirm("ยืนยันการลบ ?")){
                                                
                                                $.ajax({
                                                    
                                                    url:'ajax9.php',
                                                    data: "t_id="+ t_id + "&company_id="+ $('#company_id').val(),
                                                    success: 
                                                        function(html){
                                                        $('#'+t_id).html();
                                                        $('#'+t_id).html(html);
                                                    }
                                            
                                                });
                                                
                                            }
                                        }
                                        
                                        function search_TName(label){
                                            
                                            var t_name = $('#t_name'+label).val();
                                            
                                            $.ajax({
                                                
                                                url:'ajax10.php',
                                                data:'t_name='+ t_name,
                                                success: function(html){
                                                    
                                                    $('#chk_name'+label).html ;
                                                    $('#chk_name'+label).html(html); 
                                                    
                                                }
                                                
                                            });
                                        
                                        }
                                        
                                        function add_t(label){
                                        
                                            $('#label'+label).html('<p><input type="text" name="t_name[]" id="t_name'+label+'" onfocus="auto_c('+label+')"/>&nbsp;&nbsp;&nbsp;<span id="chk_name'+label+'">กรุณากรอกชื่ออาจารย์</span> <input type="button" value="เคลียร์" onclick="clear_tname('+label+')"/></p><span id="label'+(label+1)+'"></p>');
                                            $('#add_btn').attr('onclick','add_t('+(label+1)+')');
                                        }
                                        
                                        function clear_tname(label){
                                            
                                            $('#chk_name'+label).val('');
                                            $('#t_name'+label).val('');
                                        }
               
                                        
                                        function auto_c(id){
                                            $( "#t_name"+id ).autocomplete(
                                            {
                                                source:'ajax15.php'
                                            })
                                            
                                            
                                        }
                                        
    
      
                                    </script>

                                    <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
                                    <script type='text/javascript' src='js/jquery.validate.js'></script> 
                                    <link type="text/css" href="css/ui-lightness/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" media="screen"/>
                                    <script type="text/javascript">
                                        
                                        
                                        $(document).ready(function(){
                                            $('#form1').validate();
                                            select_com();
                                         
                                          
                                            
                                        });
                                            
                                        
                                        
                                        
                                    </script>

                                    <?php
                                    if ($_POST['submit']) {

                                        $t_id = "";

                                        if ($_POST['no_student']) {
                                            ?>
                                            <script>alert('มีข้อผิดพลาด : หน่วยงานนี้ยังไม่มีนักศึกษามาฝึกงาน' );
                                                //                                                                    window.location='register_new3.php';
                                            </script>
                                            <?php
                                        } else {

                                            if ($_POST['date']) {
                                                
                                            } else {
                                                
                                            }
                                            for ($i = 0; $i < sizeof($_POST['t_name']); $i++) {//เช็คอาจารย์ซ้ำ
                                                $sql = "select t_id from teacher where t_name LIKE '" . $_POST['t_name'][$i] . "' ";
                                                $rs = mysql_query($sql);



                                                $t_id = mysql_fetch_array($rs);





                                                $sql = "SELECT * FROM supervision WHERE t_id = '" . $t_id[0][t_id] . "'
                                                    AND company_id =  '" . $_POST['company_id'] . "'   ";
                                                $result = mysql_query($sql);



                                                if (!mysql_num_rows($result)) {




                                                    $sql = "INSERT INTO  `supervision` (
`su_id` ,
`company_id` ,
`date` ,
`t_id` 
)
VALUES (
NULL ,  '" . $_POST['company_id'] . "',  '" . $_POST['date'] . "',  '" . $t_id[0] . "'
);";
                                                    $rs = mysql_query($sql);
                                                    /* 	 $row = mysql_fetch_array($rs);

                                                      if($row != 0)
                                                      {
                                                      echo "<script>
                                                      alert('ชื่อนี้มีอยู่แล้ว' );
                                                      history.back();
                                                      </script>";

                                                      } */
                                                    if ($rs) {
                                                        ?>
                                                        <script>alert('ข้อมูลถูกบันทึกแล้ว' );
                                                            //                                                                    window.location='register_new3.php';
                                                        </script>
                                                        <?php
                                                    } else {
                                                        echo mysql_error();
                                                    }
                                                } else {
                                                    ?>
                                                    <script>
                                                        alert('ชื่ออาจารย์ : <?= $_POST['t_name'][$i] ?> มีการลงเวลานิเทศแล้ว');
                                                        //                                                                    window.location='register_new3.php';
                                                    </script>
                                                    <?php
                                                }
                                            }
                                        }
                                    }



                                    $sql = "select * from company ";

                                    $rs = mysql_query($sql);
                                    ?>



                                    <form method="post" action="">
                                        <div class="super-vision" >
                                            <div style="width: 450px;margin: 0 auto;">
                                                <p class="main-title"  >.:: ตารางการจัดการนิเทศนักศึกษา ::.</p>
                                            </div>
                                            <div style="width:650px; margin: 0 auto;">
                                                <div class="row-fluid" >
                                                    <div class="span3" style="padding-top: 5px;"><p class="title-name">หน่วยงาน :</p></div>
                                                    <div class="span6">

                                                        <select name="company_id" id="company_id" onchange="select_com()">
                                                            <option value="*">กรุณาเลือกหน่วยงาน</option>

                                                            <?php while ($result = mysql_fetch_array($rs)) { ?>  
                                                                <?php
                                                                if ($_GET[company_id] == $result['company_id']) {
                                                                    ?>

                                                                    <option selected="selected" value="<?= $result['company_id'] ?>"><?= $result['company_name'] ?></option>
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <option value="<?= $result['company_id'] ?>"><?= $result['company_name'] ?></option>
                                                                    <?php
                                                                }
                                                                ?>



<?php } ?>
                                                        </select>




                                                    </div>
                                                </div>
                                                <div class="row-fluid">
                                                    <div class="span4" style="padding-top: 5px;"><p class="title-name">รายชื่อนักศึกษา :</p></div>
                                                    <div class="span6">  </div>
                                                </div>
                                                <div id="select_com" style="width:600px;margin-left: 50px;margin-top: 15px;"></div>
                                                <div class="row-fluid" style="margin-top: 10px;">
                                                    <div class="span3" style="padding-top: 5px;"><p class="title-name">รายชื่ออาจารย์ :</p></div>
                                                    <div class="span6">  </div>
                                                </div>
                                                <div id="list_teacher" style="margin-top:15px;"></div>

                                                <div >
                                                    <div class="row-fluid" >
                                                        <div class="span4">


                                                            <div id="chk_date"></div>

                                                        </div>
                                                        <div class="span4 offset1">
                                                            เลือกวันที่นิเทศ
                                                        </div>
                                                    </div>

                                                </div>
                                              
                                                <p>
                                                    <input type="text" name="t_name[]" id="t_name0" onfocus="auto_c(0)" />
                                                    &nbsp;&nbsp;&nbsp;
                                                    <span id="chk_name0">กรุณากรอกชื่ออาจารย์</span>
                                                    <input type="button" value="เคลียร์" onClick="clear_tname(0)"/>
                                                    <input type="button" value="เพิ่มรายชื่อ" onClick="add_t(1)" id="add_btn"/>
                                                </p>
                                                <span id="label1"></span>
                                                <div style="width:200px;margin: 0 auto;margin-top: 15x;"> 
                                                    <input class="btn" type="submit" name="submit" value="เพิ่มข้อมูล"/>
                                                    <span > <a  href="show_supervision2.php" class="btn">ยกเลิก</a></span>
                                                </div>

                                            </div>
                                        </div>
                                    </form>



                                    <!-- InstanceEndEditable --></div>
                            </div>


                            <div class="wrapper">
                                <div id="copyright" class="clear">
                                    <div style="width:960px;margin:0 auto; ">
                                        <p class="fl_left">Copyright (c) 2012 <a href = "http://webhosting.udru.ac.th/~std52040249439" target="_blank">http://www.gen-dev-soft.com/experience-csit/</a> All rights reserved. 
                                            <br>
                                                Design by Nittaya Kakulphin & Benjawan Sriralat @ Udonthani Rajabhat University.</p>
                                    </div>
                                </div>
                            </div>
                            </body>
                            <!-- InstanceEnd --></html>
