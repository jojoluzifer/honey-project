<?
	session_start();
$type_user = $_SESSION['usertype'];

if ($type_user != "teacher")
	{
	
		header("Location:user_login.php");
		
		
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>ระบบสารสนเทศฝึกประสบการณ์วิชาชีพฯ :<?php echo $_SESSION['usertype']; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="jquery.dropotron-1.0.js"></script>
<script type="text/javascript" src="jquery.slidertron-1.1.js"></script>
<!--<script type="text/javascript">
	$(function() {
		$('#menu > ul').dropotron({
			mode: 'fade',
			globalOffsetY: 11,
			offsetY: -15
		});
		$('#slider').slidertron({
			viewerSelector: '.viewer',
			indicatorSelector: '.indicator span',
			reelSelector: '.reel',
			slidesSelector: '.slide',
			speed: 'slow',
			advanceDelay: 4000
		});
	});
</script>-->
<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">
  <div id="header"></div>
  <div id="menu">
  <ul>
			<li class="first">
<a href="index_teacher.php">หน้าหลัก</a></li>
			<li><a href="Webboard.php">เว็บบอร์ด</a></li>

            <?php if(!$_SESSION['username']){?>
            			<li><a href="submit3.php">สมัครสมาชิก</a></li>
			<li>
				<a href="user_login.php">เข้าสู่ระบบ</a>
	  </li>
      <?php }else{ ?>
			<li class="last"><a href="user_logout.php">ออกจากระบบ</a></li>
            <?php } ?>
    </ul>
		<br class="clearfix" />
  </div>
   <div align="right">ยินดีต้อนรับ : <?= $_SESSION['username']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
	<div id="page">
		<div id="content" align="left"><br class="clearfix" />
	  </div>
	  <div id="sidebar">
      <div class="box">
		<h4>เมนู</h4>
				<ul class="list">
					<li class="first"><a href="report_std+company.php" target="_blank">รายงานรายชื่อแหล่งฝึกประสบการณ์<br>พร้อมทั้งชื่อนักศึกษา
              </a></li>
					
					<li><a href="Report_Supervision.php" target="_blank">รายงานข้อมูลโครงการนิเทศนักศึกษา</a></li>
				<li><a href="report_diary.php">ข้อมูลบันทึกประจำวันของนักศึกษา</a></li>
          <li><a href="report_advice.php">รายงานปัญหาที่พบและ<br>ข้อเสนอแนะจากหน่วยงาน</a></li>
					<li><a href="Report_Score.php" target="_blank">รายงานคะแนนของนักศึกษา</a></li>
					<li><a href="Report_ScoreSupervision.php">รายงานคะแนนการนิเทศ</a></li>
					<li><a href="Report_ScoreDocument.php">รายงานคะแนนการดำเนินงานเอกสาร</a></li>
					<li><a href="Report_ScoreActivity.php">รายงานคะแนนเข้าร่วมกิจกรรม
</a></li>
					<li><a href="Report_ScoreSendDoc.php">รายงานคะแนนการส่งเอกสาร</a></li>
					
				  <li><a href="report_grade.php" class="libra">รายงานผลการประเมิน</a></li>
							
				</ul>
	    </div>
		<div class="box">
				<h4>ลิงค์ภายใน</h4>
				<ul class="list">
					<li class="first"><a href="http://www.udru.ac.th" target="_blank">มหาวิทยาลัยราชภัฎอุดรธานี</a></li>
					<li><a href="http://sci.udru.ac.th" target="_blank">คณะวิทยาศาสตร์</a></li>
					<li  class="last"><a href="http://ce.udru.ac.th" target="_blank">สาขาวิชาวิทยาการคอมพิวเตอร์<br>
        และเทคโนโลยีสารสนเทศ</a></li>		
				</ul>
	    </div>
	  </div>
		<br class="clearfix" />
  </div>
	<div id="page-bottom">Copyright (c) 2012 <a href = "http://webhosting.udru.ac.th/~std52040249439" target="_blank">http://webhosting.udru.ac.th/~std52040249439</a> All rights reserved. 
<br>
Design by Nittaya Kakulphin & Benjawan Sriralat @ Udonthani Rajabhat University.
<br>		 
	  <br class="clearfix" />
	</div>
</div>
<br>
<br>
</body>
</html>