-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- โฮสต์: localhost
-- เวลาในการสร้าง: 
-- รุ่นของเซิร์ฟเวอร์: 5.0.51
-- รุ่นของ PHP: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- ฐานข้อมูล: `experience`
-- 

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `company`
-- 

CREATE TABLE `company` (
  `company_id` int(5) unsigned zerofill NOT NULL auto_increment,
  `company_name` varchar(50) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `boos` varchar(30) NOT NULL,
  `email` varchar(50) default NULL,
  `contact_name` varchar(50) NOT NULL,
  `location` varchar(50) NOT NULL,
  `approval` varchar(50) default '0',
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `peradd` varchar(30) NOT NULL default 'company',
  PRIMARY KEY  (`company_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1259 ;

-- 
-- dump ตาราง `company`
-- 

INSERT INTO `company` VALUES (00001, 'ThaiCopperation', '0904564321', 'pop', 'pop@hotmail.com', 'pop', 'thailand', '0', 'pop', 'pop', 'company');
INSERT INTO `company` VALUES (00002, 'FristChoei', '0904564321', 'ft', 'apple@gmail.com', 'ft', 'tt', '0', '33', '33', 'company');
INSERT INTO `company` VALUES (00003, 'EateSofteware', '0850845666', 'ee', 'pukky_do@hotmail.com', 'ee', 'thailand', '1', '55', '55', 'company');
INSERT INTO `company` VALUES (00004, 'LopeWhile', '0850845666', 'lop', 'numnim@gmail.com', 'lop', 'thai', '0', 'lop', 'lop', 'company');
INSERT INTO `company` VALUES (00005, 'TeeneeThai', '0909987677', 'tup', 'master_bank@hotmail.com', 'tup', 'thai', '1', '56', '56', 'company');
INSERT INTO `company` VALUES (00006, 'toyota', '0984455332', 'toyatoi', 'toyata@gmail.com', 'toyatoi', 'thailand', '0', 'toyota', '1234', 'company');
INSERT INTO `company` VALUES (00007, 'er', '0850845666', 'er', 'numnim@gmail.com', 'er', 'th', '0', 'toyata', '123', 'company');
INSERT INTO `company` VALUES (00008, 'Apple', '0904564321', 'Apple', 'pukky_do@hotmail.com', 'Apple', 'yj', '0', 'Apple', 'apple', 'company');
INSERT INTO `company` VALUES (01253, 'บริษัทยูไนเต็ด', '0807411833', 'yy', 'myberry-@hotmail.co.th', 'yy', 'yy', '', 'yy', 'yy', 'company');
INSERT INTO `company` VALUES (01254, 'rr', '0850487069', 'rr', 'rungtiwa.cs@gmail.com', 'rr', 'rr', '', 'rr', 'rr', 'company');
INSERT INTO `company` VALUES (01255, 'lollypop', '0807411833', 'ks', 'jam@hotmail.com', 'ks', 'thailand', '0', '', '', 'company');
INSERT INTO `company` VALUES (01256, 'uu', '0807511838', 'uu', 'uu@hotmail.com', 'uu', 'thai', '0', 'hatyai', '7777', 'company');
INSERT INTO `company` VALUES (01257, 'jojo', '0807511839', 'jojo', 'cutefulllife__honey@windowslive.com', 'jojo', 'thai', '0', '', '', 'student');
INSERT INTO `company` VALUES (01258, 'telcom', '0894455435', 'jojo', 'telcom@hotmail.com', 'jojo', 'thai', '0', '', '', 'student');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `register`
-- 

CREATE TABLE `register` (
  `register_id` int(5) unsigned zerofill NOT NULL auto_increment,
  `points` decimal(10,2) default '0.00',
  `position` varchar(50) default NULL,
  `std_id` varchar(11) NOT NULL,
  `groups_num` varchar(10) NOT NULL,
  `company_id` int(5) unsigned zerofill default NULL,
  PRIMARY KEY  (`register_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=198 ;

-- 
-- dump ตาราง `register`
-- 

INSERT INTO `register` VALUES (00146, 50.00, 'ฝึกประสบการณ์', '52040249411', '00003', 01253);
INSERT INTO `register` VALUES (00147, 80.00, 'ฝึกประสบการณ์', '52040249412', '00002', 01254);
INSERT INTO `register` VALUES (00173, 0.00, 'ยังไม่ฝึกประสบการณ์', '52040249406', '00002', 00001);
INSERT INTO `register` VALUES (00153, 0.00, 'ยังไม่ฝึกประสบการณ์', '25425252', '00002', 00002);
INSERT INTO `register` VALUES (00197, 0.00, 'ยังไม่ฝึกประสบการณ์', '52040235201', '00004', NULL);
INSERT INTO `register` VALUES (00155, 0.00, 'ยังไม่ฝึกประสบการณ์', '42424242424', '00003', 00004);
INSERT INTO `register` VALUES (00156, 0.00, 'ยังไม่ฝึกประสบการณ์', '3253523', '00002', 00005);
INSERT INTO `register` VALUES (00157, 0.00, 'ยังไม่ฝึกประสบการณ์', '235352', '00002', 00006);
INSERT INTO `register` VALUES (00159, 0.00, 'ยังไม่ฝึกประสบการณ์', '45756754876', '00004', 00007);
INSERT INTO `register` VALUES (00160, 0.00, 'ยังไม่ฝึกประสบการณ์', '53674567', '00004', 00008);
INSERT INTO `register` VALUES (00161, 0.00, 'ยังไม่ฝึกประสบการณ์', '78098690890', '00004', 01254);
INSERT INTO `register` VALUES (00162, 0.00, 'ยังไม่ฝึกประสบการณ์', '56576767', '00004', 01255);
INSERT INTO `register` VALUES (00164, 0.00, 'ยังไม่ฝึกประสบการณ์', '5555555555', '00005', 01257);
INSERT INTO `register` VALUES (00165, 0.00, 'ยังไม่ฝึกประสบการณ์', '52040249333', '00005', 01258);
INSERT INTO `register` VALUES (00180, 0.00, 'ยังไม่ฝึกประสบการณ์', '52040249439', '00003', 00003);
INSERT INTO `register` VALUES (00183, 0.00, 'ยังไม่ฝึกประสบการณ์', '52040249307', '00002', 00001);
INSERT INTO `register` VALUES (00195, 0.00, 'ยังไม่ฝึกประสบการณ์', 'ee', '00001', NULL);
INSERT INTO `register` VALUES (00186, 0.00, 'ยังไม่ฝึกประสบการณ์', '52040249404', '00002', 00001);

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `student`
-- 

CREATE TABLE `student` (
  `std_id` varchar(11) NOT NULL,
  `std_name` varchar(50) NOT NULL,
  `address` varchar(200) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `major` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `resume` varchar(100) default NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `picture` varchar(100) NOT NULL,
  PRIMARY KEY  (`std_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- dump ตาราง `student`
-- 

INSERT INTO `student` VALUES ('52040249406', 'น้ำใส  ใจสอาด55', 'อุดร      55                  ', '080741183355', 'วิทยาการคอมพิวเตอร์', 'namk@hotmail.com', '52040249406.doc', 'nam', 'nam      ', '52040249406.jpg');
INSERT INTO `student` VALUES ('52040240414', 'ยลดา สมเมตร', 'thai        ', '0984455332', 'เทคโนโลยีสารสนเทศ', 'yy@gmail.com', '52040240414.txt', 'yoly', 'yol123   ', '52040240414.png');
INSERT INTO `student` VALUES ('52040235201', 'กก กก', 'thailand  ', '0809878765', 'เทคโนโลยีสารสนเทศ', 'kk@hotmail.com', '52040235201.doc', 'korkor', '1234', '52040235201.jpg');
INSERT INTO `student` VALUES ('52040249439', 'ขข ขข', 'thailand  ', '0888954564', 'เทคโนโลยีสารสนเทศ', 'kun@hotmail.com', '52040249439.docx', 'gorgor', '4567', '52040249439.JPG');
INSERT INTO `student` VALUES ('52040249409', 'น้ำสาย', 'tt                                                                                                                                                                                                      ', '0909871234', 'วิทยาการคอมพิวเตอร์', 'master_bank@hotmail.com', '52040249409.docx', 'namsay', '4545         ', '52040249409.JPG');
INSERT INTO `student` VALUES ('52040249512', 'ศิริวรรณ มั่นใจ', '5656            ', '0894455435', 'เทคโนโลยีสารสนเทศ', 'uu@hotmail.com', '52040249512.txt', 'ringtone', '5656      ', '52040249512.JPG');
INSERT INTO `student` VALUES ('52040249404', 'น้ำใส  ใจสอาด', 'อุดร    ', '0807411831', 'วิทยาการคอมพิวเตอร์', 'nam@hotmail.com', '52040249404.docx', 'pokko', '7897', '52040249404.JPG');
INSERT INTO `student` VALUES ('52040249408', 'น้ำใจ', 'อุดร    ', '0807411830', 'เทคโนโลยีสารสนเทศ', 'namjai@hotmail.com', '52040249408.docx', 'namjai', 'namjai  ', '52040249408.JPG');
INSERT INTO `student` VALUES ('51240234567', 'สมปอง คลองศักดิ์', 'frffrfe  ', '0807411833', 'วิทยาการคอมพิวเตอร์', 'gfgf@hotmail.com', '51240234567.docx', 'aaa', 'aaa ', '51240234567.JPG');
INSERT INTO `student` VALUES ('52040249307', 'สายน้ำ', 'fjkdiovj    ', '0807411811', 'เทคโนโลยีสารสนเทศ', 'say@hotmail.com', '52040249307.docx', 'song', '9874', '52040249307.JPG');
INSERT INTO `student` VALUES ('51234534321', 'สมศรี มั่งมี', 'thailand', '0850487069', 'เทคโนโลยีสารสนเทศ', 'rungtiwa.cs@gmail.com', '51234534321.docx', 'w', 'w ', '51234534321.JPG');
INSERT INTO `student` VALUES ('51040135219', 'ทดสอบ', 'tt', '0807511838', 'วิทยาการคอมพิวเตอร์', 'pukky-do@hotmail.com', 'สอบเขียนโปรแกรม.pdf', 'hu', '123 ', '');
INSERT INTO `student` VALUES ('51040354345', 'สมชาย', '  dfghjkl', '0807511838', 'วิทยาการคอมพิวเตอร์', 'pukky_do@hotmail.com', 'คู่มือการลงทะเบียนเข้าร่วมโครงการในระบบ.doc', 'hy', 'hy ', '');
INSERT INTO `student` VALUES ('51323445412', '325325', '  sdfghjkl', '0807511838', 'วิทยาการคอมพิวเตอร์', 'pukky_do@hotmail.com', 'คู่มือการลงทะเบียนเข้าร่วมโครงการในระบบ.doc', 'tr', 'tr ', '');
INSERT INTO `student` VALUES ('51232345634', '325325', '  ajkl;', '0807511839', 'เทคโนโลยีสารสนเทศ', 'cutefulllife__honey@windowslive.com', 'คู่มือการลงทะเบียนเข้าร่วมโครงการในระบบ.doc', 'hg', 'hg ', '');
INSERT INTO `student` VALUES ('52040249411', 'รจนา ศาลไทย', 'thailand      ', '0894453212', 'วิทยาการคอมพิวเตอร์', 'rotjana@hotmail.com', '_ SIPA _.pdf', 'rotjana', 'rotjana   ', '');
INSERT INTO `student` VALUES ('52040243817', 'fgh', 'th', '0807511838', 'เทคโนโลยีสารสนเทศ', 'pukky_do@hotmail.com', 'test.txt', 'fgh', 'fgh', '');
INSERT INTO `student` VALUES ('52040132123', '7878', '7878', '0894455435', 'วิทยาการคอมพิวเตอร์', 'pukky_do@hotmail.com', '123.txt', '7878', '7878', '');
INSERT INTO `student` VALUES ('52040235203', 'แก้วการ บุญเกิด', 'thailand    ', '0894455435', 'วิทยาการคอมพิวเตอร์', 'pukky-do@hotmail.com', '52040235203.', 'root', '1234', '52040235203.');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `supervision`
-- 

CREATE TABLE `supervision` (
  `su_id` int(5) unsigned zerofill NOT NULL auto_increment,
  `date` date NOT NULL,
  `advice` text NOT NULL,
  `t_id` int(5) unsigned zerofill NOT NULL,
  `register_id` int(5) unsigned zerofill NOT NULL,
  PRIMARY KEY  (`su_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

-- 
-- dump ตาราง `supervision`
-- 

INSERT INTO `supervision` VALUES (00001, '2012-06-05', 'นักศึกษา', 00001, 00146);
INSERT INTO `supervision` VALUES (00002, '2012-06-14', 'มาไม่ตรงเวลา', 00002, 00147);
INSERT INTO `supervision` VALUES (00003, '2012-04-10', 'นักศึกษาไม่เคารพ', 00003, 00173);
INSERT INTO `supervision` VALUES (00004, '2011-06-14', 'การเดินทาง', 00004, 00153);
INSERT INTO `supervision` VALUES (00011, '2012-09-09', '', 01678, 00181);
INSERT INTO `supervision` VALUES (00010, '2012-09-09', '', 01678, 00153);
INSERT INTO `supervision` VALUES (00012, '2012-09-10', '', 00003, 00169);
INSERT INTO `supervision` VALUES (00013, '2012-09-10', '', 00001, 00173);

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `teacher`
-- 

CREATE TABLE `teacher` (
  `t_id` int(5) unsigned zerofill NOT NULL auto_increment,
  `t_name` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY  (`t_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7789 ;

-- 
-- dump ตาราง `teacher`
-- 

INSERT INTO `teacher` VALUES (00001, 'cc', '0987656543', 'cc@hotmail.com', 'c', '9', 'admin');
INSERT INTO `teacher` VALUES (00002, 'admin', 'd', 'd', 'admin', 'admin', 'admin');
INSERT INTO `teacher` VALUES (00003, 'e', 'e', 'e', 'e', 'e', 'teacher_general');
INSERT INTO `teacher` VALUES (00004, 'f', 'f', 'f', 'f', 'f', 'officer');
INSERT INTO `teacher` VALUES (03344, 'น้ำใจ 55', '0987776652', 'goppy55@gmail.com', 'general', 'general', 'teacher_general');
INSERT INTO `teacher` VALUES (07788, 'สมร', '0871234543', 'samon@gmail.com', 'officer', 'officer', 'officer');
INSERT INTO `teacher` VALUES (01678, 'รัตนา ศรีลศาล', '0893454345', 'rattana@hotmail.com', 'teacher', 'teacher', 'teacher');
