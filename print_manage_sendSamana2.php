<?php
define('FPDF_FONTPATH', 'font/');
require('fpdf.php');

function convert($str)
{
    return iconv('UTF-8', 'cp874', $str);
}
function thai_date($date)
{
	$date = $date+0;
	
	$thai_date = "";
	for($i=0 ; $i<strlen($date) ; $i++)
	{
		switch(substr($date,$i,1))
		{
			case '0' : $thai_date.="๐"; break;
			case '1' : $thai_date.="๑"; break;
			case '2' : $thai_date.="๒"; break;
			case '3' : $thai_date.="๓"; break;
			case '4' : $thai_date.="๔"; break;
			case '5' : $thai_date.="๕"; break;
			case '6' : $thai_date.="๖"; break;
			case '7' : $thai_date.="๗"; break;
			case '8' : $thai_date.="๘"; break;
			case '9' : $thai_date.="๙"; break;
			
		}
	}
	return $thai_date;
}


$month = array(
    '1' => 'มกราคม',
    '2' => 'กุมภาพันธ์',
    '3' => 'มีนาคม',
    '4' => 'เมษายน',
    '5' => 'พฤษภาคม',
    '6' => 'มิถุนายน',
    '7' => 'กรกฎาคม',
    '8' => 'สิงหาคม',
    '9' => 'กันยายน',
    '10' => 'ตุลาคม',
    '11' => 'พฤศจิกายน',
    '12' => 'ธันวาคม',
    );
	
	include "connect2.php";

$sql = "select * from company,student,register where ( register.std_id = student.std_id 
and register.company_id = company.company_id and register_id = '".$_GET['register_id']."' ) ";
$result = mysql_query($sql);

$rs = mysql_fetch_array($result);

#CREATE PDF
$pdf = new FPDF('P','mm','A4');

$pdf->AddFont('angsana', '', 'angsa.php');

// เพิ่มฟอนต์ภาษาไทยเข้ามา ตัวหนา  กำหนด ชื่อ เป็น angsana
$pdf->AddFont('angsana', 'B', 'angsab.php');

// เพิ่มฟอนต์ภาษาไทยเข้ามา ตัวหนา  กำหนด ชื่อ เป็น angsana
$pdf->AddFont('angsana', 'I', 'angsai.php');

// เพิ่มฟอนต์ภาษาไทยเข้ามา ตัวหนา  กำหนด ชื่อ เป็น angsana
$pdf->AddFont('angsana', 'BI', 'angsaz.php');
#END CREATE PDF

$pdf->AddPage();

$pdf->Image("img/export.png", 95, 10, 20, 20);

$pdf->SetFont('angsana', '', 16);
$pdf->Ln(16);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(100, 0, convert("ที่  ศธ ๐๕๔๓.๖/ว๒๑0"), 0, 0, 'L');
$pdf->Cell(120, 0, convert("คณะวิทยาศาสตร์"), 0, 0, 'L');
$pdf->Ln(7);
$pdf->Cell(120, 0, convert(""), 0, 0, 'L');
$pdf->Cell(120, 0, convert("มหาวิทยาลัยราชภัฏอุดรธานี"), 0, 0, 'L');
$pdf->Ln(7);
$pdf->Cell(120, 0, convert(""), 0, 0, 'L');
$pdf->Cell(120, 0, convert("อ.เมือง    จ.อุดรธานี  ๔๑๐๐๐"), 0, 0, 'L');
$pdf->Ln(10);
$pdf->Cell(100, 0, convert(""), 0, 0, 'L');
$pdf->Cell(120, 0, convert(thai_date(date('d'))." ".$month[date('m')+0]." ".thai_date(date('Y')+543)), 0, 0, 'L');

$pdf->Ln(10);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(10, 0, convert("เรื่อง"), 0, 0, 'L');
$pdf->Cell(100,0,convert("โครงการสัมมนาหลังฝึกประสบการณ์วิชาชีพ"),0,0,"L");

$pdf->Ln(7);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(10, 0, convert("เรียน"), 0, 0, 'L');
$pdf->Cell(100,0,convert($rs['boos']),0,0,"L");

$pdf->Ln(7);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(23, 0, convert("สิ่งที่ส่งมาด้วย"), 0, 0, 'L');
$pdf->Cell(100,0,convert("แบบฟอร์มโครงการสัมมนาหลังฝึกประสบการณ์วิชาชีพ"),0,0,"L");

$pdf->Ln(12);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(15, 0, convert(""), 0, 0, 'L');
$pdf->Cell(100,0,convert("ด้วยคณะวิทยาศาสตร์  มหาวิทยาลัยราชภัฏอุดรธานี  ได้เปิดสอนระดับปริญญาตรี  สาขาวิชา"),0,0,"L");

$pdf->Ln(7);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(100,0,convert("เทคโนโลยีสารสนเทศ ซึ่งกำหนดให้นักศึกษาต้องฝึกประสบการณ์วิชาชีพเป็นเวลาไม่น้อยกว่า ๒๘๐ "),0,0,"L");

$pdf->Ln(7);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(100,0,convert("ชั่วโมง  โดยนักศึกษาออกฝึกประสบการณ์วิชาชีพ  ระหว่างวันที่ ๑  เมษายน ๒๕๕๕  ถึงวันที่"),0,0,"L");

$pdf->Ln(7);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(100,0,convert("๓๑ กันยายน ๒๕๕๕"),0,0,"L");

$pdf->Ln(12);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(15, 0, convert(""), 0, 0, 'L');
$pdf->Cell(100,0,convert("ตามที่มหาวิทยาลัยฯ ได้ติดต่อขอความอนุเคราะห์รับนักศึกษาเข้าฝึกประสบการณ์วิชาชีพกับทาง"),0,0,"L");

$pdf->Ln(7);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(100,0,convert("หน่วยงานของท่านใว้และหน่วยงานของท่านประสงคที่จะรับนั้น เพื่อให้การฝึกประสบการณ์วิชาชีพบรรลุ"),0,0,"L");

$pdf->Ln(7);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(100,0,convert("วัตถุประสงค์ จึงใคร่ขอส่งรายชื่อนักศึกษาได้แก่ ".$rs[perfix].$rs[std_name]." เพื่อฝึกประสบการณ์วิชาชีพใน"),0,0,"L");

$pdf->Ln(7);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(100,0,convert("หน่อยงานของท่าน"),0,0,"L");

/*$sql = "SELECT * FROM register,student WHERE register.std_id=student.std_id AND register_id='".$_GET['std_id']."' ";
$result_std = mysql_query($sql);

$chk = mysql_num_rows($result_std);

$i=1;

while($row_show = mysql_fetch_array($result_std)){
$pdf->Ln(7);
$pdf->Cell(50, 0, convert(""), 0, 0, 'L');
$pdf->Cell(100,0,convert(($chk>1 ? thai_date($i).". " : " ").$row_show['std_name']),0,0,"L");
$i++;
}*/
$pdf->Ln(10);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(15, 0, convert(""), 0, 0, 'L');
$pdf->Cell(100,0,convert("จึงเรียนมาเพื่อโปรดทราบ"),0,0,"L");

$pdf->Ln(15);
$pdf->Cell(100, 0, convert(""), 0, 0, 'L');
$pdf->Cell(60,0,convert("ขอแสดงความนับถือ"),0,0,"C");

$pdf->Ln(20);
$pdf->Cell(100, 0, convert(""), 0, 0, 'L');
$pdf->Cell(60,0,convert("(นายอนุกูล  อ่อนสวัสดิ์)"),0,0,"C");

$pdf->Ln(6);
$pdf->Cell(100, 0, convert(""), 0, 0, 'L');
$pdf->Cell(60,0,convert("คณบดีคณะวิทยาศาสตร์"),0,0,"C");

$pdf->SetFont('angsana', '', 13);
$pdf->Ln(15);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(60,0,convert("สาขาวิชาเทคโนโลยีสารสนเทศ  คณะวิทยาศาสตร์"),0,0,"L");
$pdf->Ln(6);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(60,0,convert("โทรศัพท์  ๐-๔๒๒๑-๑๐๔๐  ต่อ  ๒๐๕"),0,0,"L");
$pdf->Ln(6);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(60,0,convert("โทรสาร  ๐-๔๒๓๔-๑๖๑๕"),0,0,"L");

$pdf->Output();

?>
