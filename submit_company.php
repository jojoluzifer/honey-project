<?php
ob_start();
session_start();


?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="EN" lang="EN" dir="ltr"><!-- InstanceBegin template="/Templates/index_full.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head profile="http://gmpg.orgs/xfn/11">
<title>ระบบสารสนเทศฝึกประสบการณ์วิชาชีพฯ :<?php echo $_SESSION['usertype']; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="imagetoolbar" content="no" />
<link rel="stylesheet" href="css/layout.css" type="text/css" />
<link rel="stylesheet" href="css/layout.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/style-mix.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="style.css" />
   
        
        <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="jquery-1.7.2.min.js"></script>
     

</head>
<body id="top">
<div class="wrapper row1">
  <div id="header" class="clear">
    <div class="fl_left">
    <p><img src="img/logo5.png" width="80" height="100"></p>
    </div>
      <div class="fl_center">
      <div class="fl_right" align="right">
      <ul>
        <li><a href="index.php">หนัาหลัก</a></li>
        <li><a href="Webboard.php">เว็บบอร์ด</a></li>
         <?php if(!$_SESSION['username']){?>
        <li><a href="user_login.php">เข้าสู่ระบบ</a></li>
        <li><a href="submit3.php">สมัครสมาชิก</a></li>
        <?php } else { ?>
        <li class="last"><a href="user_logout.php">ออกจากระบบ</a></li>
        <?php } ?>
      </ul>
    </div>
      <br>
      <br>
      <h1>&nbsp;&nbsp;ระบบสารสนเทศฝึกประสบการณ์วิชาชีพ</h1>
      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;สาขาวิทยาการคอมพิวเตอร์และเทคโนโลยีสารสนเทศ</p>
    </div>
 
  </div>
</div>

<div class="wrapper row2">
  <div id="topnav">
                                    <div class="row-fluid">
                                        <div class="span9">
                                            <ul>

                                        <!-- admin -->
                                        <? if ($_SESSION["usertype"] == "admin") { ?>
                                            <li><a href="show_std.php">จัดการข้อมูลนักศึกษา</a></li>
                                            <li><a href="show_teacher.php">จัดการข้อมูลอาจารย์</a></li>
                                            <li><a href="show_company.php">จัดการข้อมูลหน่วยงาน</a></li>

                                            <!-- company -->
                                        <? } else if ($_SESSION["usertype"] == "company") { ?>
                                         <li><a href="company_detail.php">ข้อมูลหน่วยงาน</a></li>
                                            <li><a href="show_request_company.php">แจ้งความจำนงค์รับนักศึกษา</a></li>
                                            <li><a href="report_StdResume.php">รายงานประวัติส่วนตัวของนักศึกษา</a></li>
                                            <li><a href="manage_score_forcompany.php">กรอกข้อมูลประเมินผล</a></li>

                                        <? } else if ($_SESSION["usertype"] == "officer") { ?>

                                            <!-- officer -->
                                            <li><a href="#">จัดการข้อมูลหนังสือฝึกประสบการณ์วิชาชีพ</a>
                                                <ul>
                                                    <li><a href="manage_send_request.php">หนังสือขอความอนุเคราะห์ฝึกประสบการณ์วิชาชีพ</a></li>
                                                    <li><a href="manage_sendSupervision.php">หนังสือนิเทศนักศึกษา</a></li>
                                                    <li><a href="manage_sendStd.php">หนังสือส่งตัวนักศึกษา</a></li>
                                                    <li><a href="manage_sendStd1.php">หนังสือขอตัวนักศึกษา</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">จัดการข้อมูลหนังสือโครงการ</a>
                                                <ul>
                                                    <li><a href="manage_sendProject.php">โครงการปฐมนิเทศก่อนฝึกประสบการณ์</a></li>
                                                    <li><a href="manage_sendSamana.php">โครงการสัมนาหลังฝึกประสบการณ์</a></li>
                                                </ul>
                                            </li>

                                        <? } else if ($_SESSION["usertype"] == "student") { ?>
                                            <!-- student -->
                                            <li><a href="resume2.php">ประวัติส่วนตัว</a></li>
                                            <?php 
											 include "connect2.php";
										   $sql = "select * from register,student where student.std_id = register.std_id and student.std_id = '".$_SESSION['std_id']."'  ";
										   $rs = mysql_query($sql);
										   $row = mysql_fetch_array($rs);
										   $num_rows = mysql_num_rows($rs);
										   
										   $position = $row['position']; 
										   
											if($num_rows != 0){?>
												
                                            <li><a href="StdRequest2.php">ส่งคำร้องขอฝึกประสบการณ์</a></li>
                                            <li><a href="result_request.php">ผลอนุมัติการฝึกประสบการณ์</a></li>
                                            
                                            <?php  if ($position == 3){?>
                                            
                                            <li><a href="show_diary.php">บันทึกประจำวัน</a></li>
                                            
                                            <?php }
											} ?>

                                        <? } else if ($_SESSION["usertype"] == "teacher_general") { ?>
                                            <!-- teacher_general -->
                                            <li><a href="show_supervision_forTG.php">ตารางการนิเทศ</a></li>
                                            <li><a href="show_std_TG.php">ข้อมูลนักศึกษาฝึกประสบการณ์</a></li>
                                            <li><a href="show_company_TG.php">ข้อมูลแหล่งฝึกประสบการณ์</a></li>
                                            <li><a href="show_news_forteachergeneral.php">จัดการข่าวประชาสัมพันธ์</a></li>

                                        <? } else if ($_SESSION["usertype"] == "teacher") { ?>
                                            <!-- teacher -->
                                            <li><a href="#">จัดการข้อมูลทั่วไป</a>
                                                <ul>
                                                    <li><a href="show_std_forteacher.php">ข้อมูลนักศึกษา</a></li>
                                                    <li><a href="show_company_forteacher.php">ข้อมูลแหล่งฝึกประสบการณ์</a></li>
                                                    <li><a href="show_project.php">ข้อมูลโครงการ</a></li>
                                                    <li><a href="show_group.php">ข้อมูลหมู่เรียน</a></li>
                                                    <li><a href="register_new3.php">จัดการหมู่เรียนนักศึกษา</a></li>
                                                      <li><a href="manage_score.php">กรอกข้อมูลประเมินผล</a></li>
                                                          <li><a href="show_titlescore.php">ข้อมูลหัวข้อการประเมินผล</a></li>
                                                    <li><a href="show_news.php">ข้อมูลข่าวประชาสัมพันธ์</a></li>
                                                    <li><a href="show_webboard.php">ข้อมูลกระทู้</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">จัดการข้อมูลการนิเทศ</a>
                                                <ul>
                                                    <li><a href="show_supervision2.php">ตารางการนิเทศนักศึกษา</a></li>
                                                    <li><a href="show_advice.php">ปัญหาที่พบและข้อเสนอแนะจากการนิเทศนักศึกษา</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="confirmRQ.php">อนุมัติแหล่งฝึกงาน</a></li>
                                            <li><a href="company_comfirmRQ.php">เปลี่ยนสถานะตอบรับการฝึก</a></li>
                                            <li><a href="#">ดูรายงาน</a>
                                                <ul>
                                                 <li><a href="show_std_mange.php">ดูรายชื่อนักศึกษา</a></li>
                                                    <li><a href="report_std+company.php">รายชื่อแหล่งฝึกประสบการณ์
                                                            พร้อมทั้งชื่อนักศึกษา</a></li>
                                                             <li><a href="show_request.php">การแจ้งความจำนงขอรับนักศึกษาฝึกประสบการณ์วิชาชีพ
                                                            </a></li>
                                                    <li><a href="Report_Supervision.php">ตารางนิเทศนักศึกษา</a></li>
                                                    <li><a href="report_diary.php">บันทึกประจำวันของนักศึกษา</a></li>
                                                    <li><a href="report_advice.php">ปัญหาที่พบและ
                                                            ข้อเสนอแนะจากหน่วยงาน</a></li>
                                                    <li><a href="show_stdScore.php">ผลการประเมินจากหน่วยงาน</a></li>
                                                    <li><a href="Report_Score.php">คะแนนของนักศึกษา</a></li>
                                                    <li><a href="Report_ScoreSupervision.php">คะแนนการนิเทศ</a></li>
                                                    <li><a href="Report_ScoreDocument.php">คะแนนการดำเนินงานเอกสาร</a></li>
                                                    <li><a href="Report_ScoreActivity.php">คะแนนเข้าร่วมกิจกรรม</a></li>
                                                    <li><a href="Report_ScoreSendDoc.php">คะแนนการส่งเอกสาร</a></li>
                                                    <li><a href="report_grade.php">ผลการประเมิน</a></li>
                                                </ul>
                                            </li>

                                        <? } else { ?>
                                            <li><a href="index.php">หน้าหลัก</a></li>
                                            <li><a href="Webboard.php">เว็บบอร์ด</a></li>
                                            <?php if (!$_SESSION['username']) { ?>
                                                <li><a href="user_login.php">เข้าสู่ระบบ</a></li>
                                                <li><a href="submit3.php">สมัครสมาชิก</a></li>
                                            <?php } else { ?>
                                                <li><a href="user_logout.php">ออกจากระบบ</a></li>
                                                </li><?php } ?>

                                        <? } ?>

                                    </ul>
                                        </div>
                                        <div class="span3" style="height: 50px" align="right">
                                            <?php
                                            if($_SESSION['username']) {
                                            ?>
                                            <p style="margin-top: 16px;">ยินดีต้อนรับ คุณ  <?= $_SESSION['username'] ?> </p>
                                                <?php
                                            }
                                                ?>
                                        </div>
                                       
                                    </div>
    <div  class="clear"></div>
  </div>
</div>

<div class="wrapper row4">
  <div id="container" class="clear">
  
  <!-- InstanceBeginEditable name="center_body" -->
<form id="form1" name="form1" method="post" action="submit_company.php">
<script type='text/javascript' src='js/jquery.validate.js'></script> 
 <script>
        $(document).ready(function(){
            $('#form1').validate();
        });
    </script>
<?php


	include("connect2.php");

		if($_POST)
		{ //3
	
			$username = $_POST['username'];
			$pwd = $_POST['pwd'];
			$conpwd = $_POST['conpwd'];
			$id = $_POST['id'];
			$companyname = $_POST['companyname'];
			$boos = $_POST['boos'];
			$contact = $_POST['contact'];
			$email = $_POST['email'];
			$phone = $_POST['phone'];
			$location = $_POST['location'];
		
	
	$sql2="SELECT * FROM company WHERE username='$username'";
	$result2 = mysql_query($sql2, $conn) or die(mysql_error());
	
	if(mysql_num_rows($result2) > 0)
	{
		echo "<script>alert('ชื่อผู้ใช้ซ้ำ!!' );history.back();</script>"; exit();
	}

		if(empty($companyname))
			{
				echo "<script>alert('กรุณากรอกชื่อหน่วยงานของท่าน' );history.back();</script>"; exit();
			}
			else if(empty($username))
			{
				echo "<script>alert('กรุณากรอก username' );history.back();</script>"; exit();
			}
				else if(empty($boos))
			{
				echo "<script>alert('กรุณากรอกชื่อหัวหน้างาน' );history.back();</script>"; exit();
			}
				else if(empty($contact))
			{
				echo "<script>alert('กรุณากรอกชื่อผู้ติดต่อ' );history.back();</script>"; exit();
			}
				else if(empty($location))
			{
				echo "<script>alert('กรุณากรอกที่อยู่ทางไปรษณย์' );history.back();</script>"; exit();
			}
				else if(empty($boos))
			{
				echo "<script>alert('กรุณากรอกชื่อหัวหน้างาน' );history.back();</script>"; exit();
			}
				else if(empty($phone))
			{
				echo "<script>alert('กรุณากรอกเบอร์โทร' );history.back();</script>"; exit();
			}
			else if (!filter_var($email,FILTER_VALIDATE_EMAIL))
				{
				echo "<script>alert('อีเมลลไม่ถูกต้องตามรูปแบบ' );history.back();</script>"; exit();
				}
			else if (empty($pwd))
				{
				echo "<script>alert('กรุณากรอกรหัสผ่าน' );history.back();</script>"; exit();
				}
			else if($pwd != $conpwd)
				{
				echo "<script>alert('รหัสผ่านไม่ตรงกัน' );history.back();</script>"; exit();
				}
		
			else
			{
						
				$sql = "insert into company(company_id, company_name, phone, boos, email,contact_name, location, username, password )";
				$sql.= "values('0', '$companyname', '$phone' ,'$boos', '$email', '$contact', '$location', '$username' ,'$pwd' )";
				$result = mysql_query($sql,$conn);
					
			if($result)
			{
				echo "<script>alert('สมัครสมาชิกเรียบร้อยแล้วค่ะ' );
					window.location='user_login.php';</script>"; exit();
			}
			else
			{
				echo "<script>
				alert('เกิดข้อผิดพลาด!! ไม่สามารถสมัครสมาชิกได้' );
				history.back();</script>"; 
				exit();
			}
				mysql_close($conn);
			}
				exit;
					
	} //3

 	
?>

<br>

  <p class="libra2" align="center"><strong>หน่วยงานสมัครสมาชิก</strong></p>
 <div align="center"> <table width="79%" height="132" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="40%" align="right" class="bodyresome">ชื่อผู้ใช้ :</td>
      <td width="60%" class="bodyresome">
        <input type="text" name="username" id="username" class="required"/></td>
      </tr>
    <tr>
      <td align="right" class="bodyresome">รหัสผ่าน :</td>
      <td class="bodyresome"><input type="password" name="pwd" id="pwd" class="required , equalTo" minlength="6"  maxlength="15"/></td>
      </tr>
    <tr>
      <td align="right" class="bodyresome">ยืนยันรหัสผ่าน :</td>
      <td class="bodyresome">
        <input type="password" name="conpwd" id="conpwd" class="required , equalTo" minlength="6"  maxlength="15" equalTo="#pwd"/></td>
      </tr>
</table>
<br>
<hr></hr>
<br>
  <table width="79%" height="481" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="40%" align="right"><span class="bodyresome">ชื่อหน่วยงาน :</span></td>
      <td width="60%">        <span class="bodyresome">
        <input name="companyname" type="text" id="companyname" size="30" class="required"/>
      </span></td>
    </tr>
    <tr>
      <td align="right"><span class="bodyresome">ชื่อหัวหน้างาน : </span></td>
      <td>      <span class="bodyresome">
        <input name="boos" type="text" id="boos" size="30" class="required"/>
      </span></td>
    </tr>
    <tr>
      <td align="right"><span class="bodyresome">ชื่อผู้ติดต่อ : </span></td>
      <td>
        <input name="contact" type="text" id="contact" size="30" class="required"/></td>
    </tr>
    <tr>
      <td align="right"><span class="bodyresome">อีเมล :</span></td>
      <td>        <span class="bodyresome">
        <input name="email" type="text" id="email" size="35" class="email , required"/>
      </span></td>
    </tr>
    <tr>
      <td align="right"><span class="bodyresome">เบอร์โทร  :</span></td>
      <td><span class="bodyresome">
        <input type="text" name="phone" id="phone" class="required , number " minlength="10"  maxlength="10"/>
      </span></td>
    </tr>
    <tr>
      <td align="right"><span class="bodyresome">ที่อยู่ทางไปรษณีย์ :</span></td>
      <td>
        <textarea name="location" id="location" cols="45" rows="5" class="required"></textarea></td>
    </tr>
    <tr>
      <td height="84" colspan="2" align="center"><span class="bodyresome">
        <input name="OK" type="submit" class="btn btn-primary" id="OK"  value="ตกลง" />
      </span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input name="clear" type="reset" class="btn" id="button" value="เคลียร์" /></td>
    </tr>
    <tr>
      <td height="73" colspan="2" align="center"><a href="submit3.php" class="link">ย้อนกลับ</a></td>
      </tr>
  </table></div>
</form>
<!-- InstanceEndEditable --></div>
</div>


<div class="wrapper">
  <div id="copyright" class="clear">
  <div style="width:960px;margin:0 auto; ">
    <p class="fl_left">Copyright (c) 2012 <a href = "http://webhosting.udru.ac.th/~std52040249439" target="_blank">http://www.gen-dev-soft.com/experience-csit/</a> All rights reserved. 
<br>
Design by Nittaya Kakulphin & Benjawan Sriralat @ Udonthani Rajabhat University.</p>
    </div>
  </div>
</div>
</body>
<!-- InstanceEnd --></html>