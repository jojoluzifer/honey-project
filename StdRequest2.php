<?php
ob_start();
session_start();
$type_user = $_SESSION['usertype'];

if ($type_user != "student")
	{

		header("Location:user_login.php");

		
	}
	
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="EN" lang="EN" dir="ltr"><!-- InstanceBegin template="/Templates/index_full.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head profile="http://gmpg.orgs/xfn/11">
<title>ระบบสารสนเทศฝึกประสบการณ์วิชาชีพฯ :<?php echo $_SESSION['usertype']; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="imagetoolbar" content="no" />
<link rel="stylesheet" href="css/layout.css" type="text/css" />
<link rel="stylesheet" href="css/layout.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/style-mix.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="style.css" />
   
        
        <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="jquery-1.7.2.min.js"></script>
     

</head>
<body id="top">
<div class="wrapper row1">
  <div id="header" class="clear">
    <div class="fl_left">
    <p><img src="img/logo5.png" width="80" height="100"></p>
    </div>
      <div class="fl_center">
      <div class="fl_right" align="right">
      <ul>
        <li><a href="index.php">หนัาหลัก</a></li>
        <li><a href="Webboard.php">เว็บบอร์ด</a></li>
         <?php if(!$_SESSION['username']){?>
        <li><a href="user_login.php">เข้าสู่ระบบ</a></li>
        <li><a href="submit3.php">สมัครสมาชิก</a></li>
        <?php } else { ?>
        <li class="last"><a href="user_logout.php">ออกจากระบบ</a></li>
        <?php } ?>
      </ul>
    </div>
      <br>
      <br>
      <h1>&nbsp;&nbsp;ระบบสารสนเทศฝึกประสบการณ์วิชาชีพ</h1>
      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;สาขาวิทยาการคอมพิวเตอร์และเทคโนโลยีสารสนเทศ</p>
    </div>
 
  </div>
</div>

<div class="wrapper row2">
  <div id="topnav">
                                    <div class="row-fluid">
                                        <div class="span9">
                                            <ul>

                                        <!-- admin -->
                                        <? if ($_SESSION["usertype"] == "admin") { ?>
                                            <li><a href="show_std.php">จัดการข้อมูลนักศึกษา</a></li>
                                            <li><a href="show_teacher.php">จัดการข้อมูลอาจารย์</a></li>
                                            <li><a href="show_company.php">จัดการข้อมูลหน่วยงาน</a></li>

                                            <!-- company -->
                                        <? } else if ($_SESSION["usertype"] == "company") { ?>
                                         <li><a href="company_detail.php">ข้อมูลหน่วยงาน</a></li>
                                            <li><a href="show_request_company.php">แจ้งความจำนงค์รับนักศึกษา</a></li>
                                            <li><a href="report_StdResume.php">รายงานประวัติส่วนตัวของนักศึกษา</a></li>
                                            <li><a href="manage_score_forcompany.php">กรอกข้อมูลประเมินผล</a></li>

                                        <? } else if ($_SESSION["usertype"] == "officer") { ?>

                                            <!-- officer -->
                                            <li><a href="#">จัดการข้อมูลหนังสือฝึกประสบการณ์วิชาชีพ</a>
                                                <ul>
                                                    <li><a href="manage_send_request.php">หนังสือขอความอนุเคราะห์ฝึกประสบการณ์วิชาชีพ</a></li>
                                                    <li><a href="manage_sendSupervision.php">หนังสือนิเทศนักศึกษา</a></li>
                                                    <li><a href="manage_sendStd.php">หนังสือส่งตัวนักศึกษา</a></li>
                                                    <li><a href="manage_sendStd1.php">หนังสือขอตัวนักศึกษา</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">จัดการข้อมูลหนังสือโครงการ</a>
                                                <ul>
                                                    <li><a href="manage_sendProject.php">โครงการปฐมนิเทศก่อนฝึกประสบการณ์</a></li>
                                                    <li><a href="manage_sendSamana.php">โครงการสัมนาหลังฝึกประสบการณ์</a></li>
                                                </ul>
                                            </li>

                                        <? } else if ($_SESSION["usertype"] == "student") { ?>
                                            <!-- student -->
                                            <li><a href="resume2.php">ประวัติส่วนตัว</a></li>
                                            <?php 
											 include "connect2.php";
										   $sql = "select * from register,student where student.std_id = register.std_id and student.std_id = '".$_SESSION['std_id']."'  ";
										   $rs = mysql_query($sql);
										   $row = mysql_fetch_array($rs);
										   $num_rows = mysql_num_rows($rs);
										   
										   $position = $row['position']; 
										   
											if($num_rows != 0){?>
												
                                            <li><a href="StdRequest2.php">ส่งคำร้องขอฝึกประสบการณ์</a></li>
                                            <li><a href="result_request.php">ผลอนุมัติการฝึกประสบการณ์</a></li>
                                            
                                            <?php  if ($position == 3){?>
                                            
                                            <li><a href="show_diary.php">บันทึกประจำวัน</a></li>
                                            
                                            <?php }
											} ?>

                                        <? } else if ($_SESSION["usertype"] == "teacher_general") { ?>
                                            <!-- teacher_general -->
                                            <li><a href="show_supervision_forTG.php">ตารางการนิเทศ</a></li>
                                            <li><a href="show_std_TG.php">ข้อมูลนักศึกษาฝึกประสบการณ์</a></li>
                                            <li><a href="show_company_TG.php">ข้อมูลแหล่งฝึกประสบการณ์</a></li>
                                            <li><a href="show_news_forteachergeneral.php">จัดการข่าวประชาสัมพันธ์</a></li>

                                        <? } else if ($_SESSION["usertype"] == "teacher") { ?>
                                            <!-- teacher -->
                                            <li><a href="#">จัดการข้อมูลทั่วไป</a>
                                                <ul>
                                                    <li><a href="show_std_forteacher.php">ข้อมูลนักศึกษา</a></li>
                                                    <li><a href="show_company_forteacher.php">ข้อมูลแหล่งฝึกประสบการณ์</a></li>
                                                    <li><a href="show_project.php">ข้อมูลโครงการ</a></li>
                                                    <li><a href="show_group.php">ข้อมูลหมู่เรียน</a></li>
                                                    <li><a href="register_new3.php">จัดการหมู่เรียนนักศึกษา</a></li>
                                                      <li><a href="manage_score.php">กรอกข้อมูลประเมินผล</a></li>
                                                          <li><a href="show_titlescore.php">ข้อมูลหัวข้อการประเมินผล</a></li>
                                                    <li><a href="show_news.php">ข้อมูลข่าวประชาสัมพันธ์</a></li>
                                                    <li><a href="show_webboard.php">ข้อมูลกระทู้</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">จัดการข้อมูลการนิเทศ</a>
                                                <ul>
                                                    <li><a href="show_supervision2.php">ตารางการนิเทศนักศึกษา</a></li>
                                                    <li><a href="show_advice.php">ปัญหาที่พบและข้อเสนอแนะจากการนิเทศนักศึกษา</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="confirmRQ.php">อนุมัติแหล่งฝึกงาน</a></li>
                                            <li><a href="company_comfirmRQ.php">เปลี่ยนสถานะตอบรับการฝึก</a></li>
                                            <li><a href="#">ดูรายงาน</a>
                                                <ul>
                                                 <li><a href="show_std_mange.php">ดูรายชื่อนักศึกษา</a></li>
                                                    <li><a href="report_std+company.php">รายชื่อแหล่งฝึกประสบการณ์
                                                            พร้อมทั้งชื่อนักศึกษา</a></li>
                                                             <li><a href="show_request.php">การแจ้งความจำนงขอรับนักศึกษาฝึกประสบการณ์วิชาชีพ
                                                            </a></li>
                                                    <li><a href="Report_Supervision.php">ตารางนิเทศนักศึกษา</a></li>
                                                    <li><a href="report_diary.php">บันทึกประจำวันของนักศึกษา</a></li>
                                                    <li><a href="report_advice.php">ปัญหาที่พบและ
                                                            ข้อเสนอแนะจากหน่วยงาน</a></li>
                                                    <li><a href="show_stdScore.php">ผลการประเมินจากหน่วยงาน</a></li>
                                                    <li><a href="Report_Score.php">คะแนนของนักศึกษา</a></li>
                                                    <li><a href="Report_ScoreSupervision.php">คะแนนการนิเทศ</a></li>
                                                    <li><a href="Report_ScoreDocument.php">คะแนนการดำเนินงานเอกสาร</a></li>
                                                    <li><a href="Report_ScoreActivity.php">คะแนนเข้าร่วมกิจกรรม</a></li>
                                                    <li><a href="Report_ScoreSendDoc.php">คะแนนการส่งเอกสาร</a></li>
                                                    <li><a href="report_grade.php">ผลการประเมิน</a></li>
                                                </ul>
                                            </li>

                                        <? } else { ?>
                                            <li><a href="index.php">หน้าหลัก</a></li>
                                            <li><a href="Webboard.php">เว็บบอร์ด</a></li>
                                            <?php if (!$_SESSION['username']) { ?>
                                                <li><a href="user_login.php">เข้าสู่ระบบ</a></li>
                                                <li><a href="submit3.php">สมัครสมาชิก</a></li>
                                            <?php } else { ?>
                                                <li><a href="user_logout.php">ออกจากระบบ</a></li>
                                                </li><?php } ?>

                                        <? } ?>

                                    </ul>
                                        </div>
                                        <div class="span3" style="height: 50px" align="right">
                                            <?php
                                            if($_SESSION['username']) {
                                            ?>
                                            <p style="margin-top: 16px;">ยินดีต้อนรับ คุณ  <?= $_SESSION['username'] ?> </p>
                                                <?php
                                            }
                                                ?>
                                        </div>
                                       
                                    </div>
    <div  class="clear"></div>
  </div>
</div>

<div class="wrapper row4">
  <div id="container" class="clear">
  
  <!-- InstanceBeginEditable name="center_body" -->
<?
include 'connect2.php';
?>
                    <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            resutName($('#select_cpn').val());
                        });
					
                        function resutName(ComID)
                        {
                            switch(ComID)
                            {
<?
$strSQL = "SELECT * FROM company ORDER BY company_id ASC";
$objQuery = mysql_query($strSQL);
while ($objResult = mysql_fetch_array($objQuery)) {
    ?>
              case "<?= $objResult["company_id"]; ?>":
             	 frmMain.txtPhone.value = "<?= $objResult["phone"]; ?>";
                 frmMain.txtBoos.value = "<?= $objResult["boos"]; ?>";
                 frmMain.txtEmail.value = "<?= $objResult["email"]; ?>";
                 frmMain.txtContact.value = "<?= $objResult["contact_name"]; ?>";
                 frmMain.txtLocation.value = "<?= $objResult["location"]; ?>";
                 break;
    <?
}
?>
                default:
                    frmMain.txtPhone.value = "";
                    frmMain.txtBoos.value = "";
                    frmMain.txtEmail.value = "";
                    frmMaim.txtContact.value = "";
                    frmMain.txtLacation.value = "";
                }
            }
                    </script>

<?php

include("connect2.php");

$sql = "SELECT * FROM register WHERE std_id='" . $_SESSION['std_id'] . "' ";
$result = mysql_query($sql);

$register = mysql_num_rows($result);
$rs2 = mysql_fetch_array($result);
$position = $rs2['position'];

if ($_POST) {

    $company_id = $_POST['select_cpn'];
    $std_id = $_SESSION["std_id"];


    if (empty($company_id)) {
        echo "<script>alert('กรุณาเลือกหน่วยงาน' );history.back();</script>";
        exit();
    } else {

        

        if ($register > 0) {
            $sql = "UPDATE register SET company_id = '$company_id' WHERE std_id = '$std_id'";
        } else {

            $sql = "insert into register(register_id,company_id,std_id,position)";
            $sql.= "values('0','$company_id','$std_id',0)";
        }
        $result = mysql_query($sql, $conn);
       
        if ($result) {
            echo "<script>alert('ข้อมูลถูกบันทึกแล้ว' );
					window.location='StdRequest2.php';</script>";
            exit();
        } else {
            echo mysql_error();
        }
    }
}



$sql = "SELECT * FROM student WHERE student.std_id='" . $_SESSION['std_id'] . "' ";
$obj = mysql_query($sql);

$rs = mysql_fetch_array($obj);


$sql = "SELECT * FROM student,register WHERE student.std_id='" . $_SESSION['std_id'] . "'
    AND register.std_id=student.std_id ";
$obj = mysql_query($sql);

$result = mysql_fetch_array($obj);
?>
                    <div align="center"><form action="" method="post" id="frmMain">
                            <table width="517" height="867" border="0" class="table table-striped">
                                <tr>
                                    <td height="96" colspan="4" align="center"><strong>ส่งคำร้องขอฝึกประสบการณ์</strong></td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="left"><font color="#009933"><b>ข้อมูลส่วนตัว</b></font>
          <table width="200" border="0" align="right">
                                            <tr>
                                                <td align="right"><img src="picture/<?= $rs['picture'] ? $rs['picture'] : "default.png" ?>" width="100"/></td>
                                            </tr>
                                      </table></td>
                                </tr>
                                <tr>
                                    <td align="right">รหัสนักศึกษา : </td>
                                    <td align="left"><?= $rs['std_id'] ?></td>
                                </tr>
                                <tr>
                                    <td align="right">ชื่อ : </td>
                                    <td align="left"><?= $rs['std_name'] ?></td>
                                </tr>
                                <tr>
                                    <td align="right">ที่อยู่ : </td>
                                    <td align="left"><?= $rs['address'] ?></td>
                                </tr>
                                <tr>
                                    <td align="right">เบอร์โทร : </td>
                                    <td align="left"><?= $rs['phone'] ?></td>
                                </tr>
                                <tr>
                                    <td align="right">อีเมลล์ : </td>
                                    <td align="left"><?= $rs['email'] ?></td>
                                </tr>
                                <tr>
                                    <td align="right">สาขา : </td>
                                    <td align="left"><?= $rs['major'] ?></td>
                                </tr>
                                <tr>

                                    <td height="58" colspan="2" align="left"><p><font color="#009933"><b>รายละเอียดหน่วยงาน</b></font></p></td>
                              </tr>
                                <tr>
                                    <td width="128" height="44" align="right">หน่วยงาน : </td>
                                    <td width="340" align="left"><select name="select_cpn" id="select_cpn" onChange="resutName(this.value);">
                                            <option value="">&lt;-- กรุณาเลือกหน่วยงาน --&gt;</option>
<?
$strSQL = "SELECT * FROM company ORDER BY company_id ASC";
$objQuery = mysql_query($strSQL);
while ($objResult = mysql_fetch_array($objQuery)) {
    ?>
                                                <option <?php if ($result['company_id'] == $objResult['company_id']) { ?> selected="selected" <?php } ?> value="<?= $objResult["company_id"]; ?>">
                                                <?= $objResult["company_name"]; ?>
                                                </option>
                                                <?
                                            }
                                            ?>
                                        </select>
                                      &nbsp;<?php if ($position == 0 || $position == 1 || $position == 4) { ?>
                                      <a href="insert_RQcompany.php" class="btn btn-primary"> เพิ่มหน่วยงาน</a>
									  <?php } ?> </td>

                                </tr>
                                <tr>
                                    <td align="right">เบอร์โทร : </td>
                                    <td align="left"><input name="txtPhone" type="text" id="txtPhone" value="" readonly="readonly" /></td>

                                </tr>
                                <tr>
                                    <td align="right">อีเมลล์ : </td>
                                    <td align="left"><input name="txtEmail" type="text" id="txtEmail" value="" size="30" readonly="readonly" /></td> 

                                </tr>
                                <tr>
                                    <td align="right">ชื่อหัวหน้างาน : </td>
                                    <td align="left"><input name="txtBoos" type="text" id="txtBoos" value="" readonly="readonly" /></td>

                                </tr>
                                <tr>
                                    <td align="right">ชื่อผู้ติดต่อ : </td>
                                    <td align="left"><input name="txtContact" type="text" id="txtContact" value="" readonly="readonly" /></td>

                                </tr>
                                <tr>
                                    <td height="106" align="right">สถานที่ติดต่อ : </td>
                                    <td align="left"><label for="txtLocation"></label>
                                        <textarea name="txtLocation" id="txtLocation" cols="45" rows="5" readonly="readonly"></textarea></td>

                                </tr>
                            </table>
                      <br /><br />
                        
                          
                            <br />
                            <div align="center">
                                <p>
									
                                    <input type="submit" value="ส่งคำร้อง" class="btn"<?php 
									if ($position == 2 || $position == 3) { ?>  disabled="disabled"  <?php } ?> />
									
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="reset" value="ยกเลิก" class="btn" 
									<?php if ($position == 2 || $position == 3) { ?>
									  disabled="disabled"  <?php } ?> />
                                </p>
                                <p>&nbsp;</p>
                            </div>
                        </form></div>
<!-- InstanceEndEditable --></div>
</div>


<div class="wrapper">
  <div id="copyright" class="clear">
  <div style="width:960px;margin:0 auto; ">
    <p class="fl_left">Copyright (c) 2012 <a href = "http://webhosting.udru.ac.th/~std52040249439" target="_blank">http://www.gen-dev-soft.com/experience-csit/</a> All rights reserved. 
<br>
Design by Nittaya Kakulphin & Benjawan Sriralat @ Udonthani Rajabhat University.</p>
    </div>
  </div>
</div>
</body>
<!-- InstanceEnd --></html>