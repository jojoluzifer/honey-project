<?
	session_start();
$type_user = $_SESSION['usertype'];

if ($type_user != "student")
	{
		//echo "กรุณาล๊อกอินเข้าสู่ระบบก่อน!";
		header("Location:user_login.php");
		//echo "<a href = login_user.php>หน้าล๊อกอิน</a>";
		
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>ระบบสารสนเทศฝึกประสบการณ์วิชาชีพฯ :<?php echo $_SESSION['usertype']; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" src="jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="jquery.dropotron-1.0.js"></script>
<script type="text/javascript" src="jquery.slidertron-1.1.js"></script>
<script type="text/javascript">
	$(function() {
		$('#menu > ul').dropotron({
			mode: 'fade',
			globalOffsetY: 11,
			offsetY: -15
		});
		$('#slider').slidertron({
			viewerSelector: '.viewer',
			indicatorSelector: '.indicator span',
			reelSelector: '.reel',
			slidesSelector: '.slide',
			speed: 'slow',
			advanceDelay: 4000
		});
	});
</script>
</head>
<body>
<div id="wrapper">
<div id="header"></div>
	<div id="menu">
		<ul>
        	<li class="first">
<a href="index_std.php">หน้าหลัก</a></li>
			<li><a href="Webboard.php">เว็บบอร์ด</a></li>

            <?php if(!$_SESSION['username']){?>
            			<li><a href="submit3.php">สมัครสมาชิก</a></li>
			<li>
				<a href="user_login.php">เข้าสู่ระบบ</a>
	  </li>
      <?php }else{ ?>
			<li class="last"><a href="user_logout.php">ออกจากระบบ</a></li>
            <?php } ?>
	  </ul>
		<br class="clearfix" />
  </div>
   <div align="right">ยินดีต้อนรับ : <?= $_SESSION['username']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
	<div id="slider">
		<div class="viewer">
			<div class="reel">
				<div class="slide">
					<img src="img/slide01.jpg" alt="" />
				</div>
				<div class="slide">
					<img src="img/slide02.jpg" alt="" />
				</div>
				<div class="slide">
					<img src="img/slide03.jpg" alt="" />
				</div>
				<div class="slide">
					<img src="img/slide04.jpg" alt="" />
				</div>
				<div class="slide">
					<img src="img/slide05.jpg" alt="" />
				</div>
			</div>
		</div>
		<div class="indicator">
			<span>1</span>
			<span>2</span>
			<span>3</span>
			<span>4</span>
			<span>5</span>
		</div>
	</div>
	<div id="page">
		<div id="content">
			<div class="box"><br>
				<h2>ยินดีต้อนรับเข้าสู่<br><br>
                ระบบสารสนเทศฝึกประสบการณ์วิชาชีพ
                </h2>
				
			</div>
			<div class="box" id="content-box1">
				<h4>หน่วยงานที่แนะนำฝึกประสบการณ์</h4>
				<ul class="section-list">
					<li class="first">
					<img class="pic alignleft" src="img/pic01.jpg" width="70" height="70" alt="" />บริษัท ABC Telecom</li>
					<li>
						<img class="pic alignleft" src="img/pic02.jpg" width="70" height="70" alt="" />บริษัท Think Coperation</li>
					<li class="last">
					<img class="pic alignleft" src="img/pic03.jpg" width="70" height="70" alt="" />บริษัท SIPA</li>
				</ul>
			</div>
		  <div class="box" id="content-box2">
	      <h3>ข่าวประชาสัมพันธ์</h3>
				<p><span class="libra">ขอให้นักศึกษาชั้นปีที่ 4 ส่งรายชื่อแหล่งฝึกประสบการณ์
ภายในวันที่ 2 กรกฎาคม 2555 ที่กล่องหน้าห้อง อ.คุณาวุติ</span></p>
			  
		  </div>
			<br class="clearfix" />
	  </div>
		<div id="sidebar">
		  <div class="box">
		    <h4>เมนู</h4>
		    <ul class="list">
            <li class="first"><a href="resume2.php">จัดการข้อมูลประวัติส่วนตัว</a></li>
		     
		      <li><a href="StdRequest2.php">ส่งคำร้องขอฝึกประสบการณ์</a></li>
		      <li><a href="result_request.php">ผลอนุมัติการฝึกประสบการณ์</a></li>
		      <li >สถานะการส่งเอกสาร</li>
		      <li  class="last"><a href="show_diary.php">ข้อมูลบันทึกประจำวัน</a></li>
	        </ul>
	      </div>
		  <div class="box">
			<h4>ลิงค์ภายใน</h4>
				<ul class="list">
					<li class="first"><a href="http://www.udru.ac.th" target="_blank">มหาวิทยาลัยราชภัฎอุดรธานี</a></li>
					<li><a href="http://sci.udru.ac.th" target="_blank">คณะวิทยาศาสตร์</a></li>
					<li  class="last"><a href="http://ce.udru.ac.th" target="_blank">สาขาวิชาวิทยาการคอมพิวเตอร์<br>
        และเทคโนโลยีสารสนเทศ</a></li>		
				</ul>
		  </div>
		</div>
		<br class="clearfix" />
	</div>
	<div id="page-bottom">Copyright (c) 2012 <a href = "http://webhosting.udru.ac.th/~std52040249439" target="_blank">http://webhosting.udru.ac.th/~std52040249439</a> All rights reserved. 
<br>
Design by Nittaya Kakulphin & Benjawan Sriralat @ Udonthani Rajabhat University.
<br>		 
	  <br class="clearfix" />
	</div>
</div>
<br>
<br>
</body>
</html>