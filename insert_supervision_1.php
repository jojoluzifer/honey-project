<?
ob_start();
session_start();
$type_user = $_SESSION['usertype'];

if ($type_user != "teacher") {
    //echo "กรุณาล๊อกอินเข้าสู่ระบบก่อน!";
    header("Location:user_login.php");
    //echo "<a href = login_user.php>หน้าล๊อกอิน</a>";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="EN" lang="EN" dir="ltr"><!-- InstanceBegin template="/Templates/index_full.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head profile="http://gmpg.orgs/xfn/11">
<title>ระบบสารสนเทศฝึกประสบการณ์วิชาชีพฯ :<?php echo $_SESSION['usertype']; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="imagetoolbar" content="no" />
<link rel="stylesheet" href="css/layout.css" type="text/css" />
<link rel="stylesheet" href="css/layout.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/style-mix.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="style.css" />
   
        
        <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="jquery-1.7.2.min.js"></script>
     

</head>
<body id="top">
<div class="wrapper row1">
  <div id="header" class="clear">
    <div class="fl_left">
    <p><img src="img/logo5.png" width="80" height="100"></p>
    </div>
      <div class="fl_center">
      <div class="fl_right" align="right">
      <ul>
        <li><a href="index.php">หนัาหลัก</a></li>
        <li><a href="Webboard.php">เว็บบอร์ด</a></li>
         <?php if(!$_SESSION['username']){?>
        <li><a href="user_login.php">เข้าสู่ระบบ</a></li>
        <li><a href="submit3.php">สมัครสมาชิก</a></li>
        <?php } else { ?>
        <li class="last"><a href="user_logout.php">ออกจากระบบ</a></li>
        <?php } ?>
      </ul>
    </div>
      <br>
      <br>
      <h1>&nbsp;&nbsp;ระบบสารสนเทศฝึกประสบการณ์วิชาชีพ</h1>
      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;สาขาวิทยาการคอมพิวเตอร์และเทคโนโลยีสารสนเทศ</p>
    </div>
 
  </div>
</div>

<div class="wrapper row2">
  <div id="topnav">
                                    <div class="row-fluid">
                                        <div class="span9">
                                            <ul>

                                        <!-- admin -->
                                        <? if ($_SESSION["usertype"] == "admin") { ?>
                                            <li><a href="show_std.php">จัดการข้อมูลนักศึกษา</a></li>
                                            <li><a href="show_teacher.php">จัดการข้อมูลอาจารย์</a></li>
                                            <li><a href="show_company.php">จัดการข้อมูลหน่วยงาน</a></li>

                                            <!-- company -->
                                        <? } else if ($_SESSION["usertype"] == "company") { ?>
                                         <li><a href="company_detail.php">ข้อมูลหน่วยงาน</a></li>
                                            <li><a href="show_request_company.php">แจ้งความจำนงค์รับนักศึกษา</a></li>
                                            <li><a href="report_StdResume.php">รายงานประวัติส่วนตัวของนักศึกษา</a></li>
                                            <li><a href="manage_score_forcompany.php">กรอกข้อมูลประเมินผล</a></li>

                                        <? } else if ($_SESSION["usertype"] == "officer") { ?>

                                            <!-- officer -->
                                            <li><a href="#">จัดการข้อมูลหนังสือฝึกประสบการณ์วิชาชีพ</a>
                                                <ul>
                                                    <li><a href="manage_send_request.php">หนังสือขอความอนุเคราะห์ฝึกประสบการณ์วิชาชีพ</a></li>
                                                    <li><a href="manage_sendSupervision.php">หนังสือนิเทศนักศึกษา</a></li>
                                                    <li><a href="manage_sendStd.php">หนังสือส่งตัวนักศึกษา</a></li>
                                                    <li><a href="manage_sendStd1.php">หนังสือขอตัวนักศึกษา</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">จัดการข้อมูลหนังสือโครงการ</a>
                                                <ul>
                                                    <li><a href="manage_sendProject.php">โครงการปฐมนิเทศก่อนฝึกประสบการณ์</a></li>
                                                    <li><a href="manage_sendSamana.php">โครงการสัมนาหลังฝึกประสบการณ์</a></li>
                                                </ul>
                                            </li>

                                        <? } else if ($_SESSION["usertype"] == "student") { ?>
                                            <!-- student -->
                                            <li><a href="resume2.php">ประวัติส่วนตัว</a></li>
                                            <?php 
											 include "connect2.php";
										   $sql = "select * from register,student where student.std_id = register.std_id and student.std_id = '".$_SESSION['std_id']."'  ";
										   $rs = mysql_query($sql);
										   $row = mysql_fetch_array($rs);
										   $num_rows = mysql_num_rows($rs);
										   
										   $position = $row['position']; 
										   
											if($num_rows != 0){?>
												
                                            <li><a href="StdRequest2.php">ส่งคำร้องขอฝึกประสบการณ์</a></li>
                                            <li><a href="result_request.php">ผลอนุมัติการฝึกประสบการณ์</a></li>
                                            
                                            <?php  if ($position == 3){?>
                                            
                                            <li><a href="show_diary.php">บันทึกประจำวัน</a></li>
                                            
                                            <?php }
											} ?>

                                        <? } else if ($_SESSION["usertype"] == "teacher_general") { ?>
                                            <!-- teacher_general -->
                                            <li><a href="show_supervision_forTG.php">ตารางการนิเทศ</a></li>
                                            <li><a href="show_std_TG.php">ข้อมูลนักศึกษาฝึกประสบการณ์</a></li>
                                            <li><a href="show_company_TG.php">ข้อมูลแหล่งฝึกประสบการณ์</a></li>
                                            <li><a href="show_news_forteachergeneral.php">จัดการข่าวประชาสัมพันธ์</a></li>

                                        <? } else if ($_SESSION["usertype"] == "teacher") { ?>
                                            <!-- teacher -->
                                            <li><a href="#">จัดการข้อมูลทั่วไป</a>
                                                <ul>
                                                    <li><a href="show_std_forteacher.php">ข้อมูลนักศึกษา</a></li>
                                                    <li><a href="show_company_forteacher.php">ข้อมูลแหล่งฝึกประสบการณ์</a></li>
                                                    <li><a href="show_project.php">ข้อมูลโครงการ</a></li>
                                                    <li><a href="show_group.php">ข้อมูลหมู่เรียน</a></li>
                                                    <li><a href="register_new3.php">จัดการหมู่เรียนนักศึกษา</a></li>
                                                      <li><a href="manage_score.php">กรอกข้อมูลประเมินผล</a></li>
                                                          <li><a href="show_titlescore.php">ข้อมูลหัวข้อการประเมินผล</a></li>
                                                    <li><a href="show_news.php">ข้อมูลข่าวประชาสัมพันธ์</a></li>
                                                    <li><a href="show_webboard.php">ข้อมูลกระทู้</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">จัดการข้อมูลการนิเทศ</a>
                                                <ul>
                                                    <li><a href="show_supervision2.php">ตารางการนิเทศนักศึกษา</a></li>
                                                    <li><a href="show_advice.php">ปัญหาที่พบและข้อเสนอแนะจากการนิเทศนักศึกษา</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="confirmRQ.php">อนุมัติแหล่งฝึกงาน</a></li>
                                            <li><a href="company_comfirmRQ.php">เปลี่ยนสถานะตอบรับการฝึก</a></li>
                                            <li><a href="#">ดูรายงาน</a>
                                                <ul>
                                                 <li><a href="show_std_mange.php">ดูรายชื่อนักศึกษา</a></li>
                                                    <li><a href="report_std+company.php">รายชื่อแหล่งฝึกประสบการณ์
                                                            พร้อมทั้งชื่อนักศึกษา</a></li>
                                                             <li><a href="show_request.php">การแจ้งความจำนงขอรับนักศึกษาฝึกประสบการณ์วิชาชีพ
                                                            </a></li>
                                                    <li><a href="Report_Supervision.php">ตารางนิเทศนักศึกษา</a></li>
                                                    <li><a href="report_diary.php">บันทึกประจำวันของนักศึกษา</a></li>
                                                    <li><a href="report_advice.php">ปัญหาที่พบและ
                                                            ข้อเสนอแนะจากหน่วยงาน</a></li>
                                                    <li><a href="show_stdScore.php">ผลการประเมินจากหน่วยงาน</a></li>
                                                    <li><a href="Report_Score.php">คะแนนของนักศึกษา</a></li>
                                                    <li><a href="Report_ScoreSupervision.php">คะแนนการนิเทศ</a></li>
                                                    <li><a href="Report_ScoreDocument.php">คะแนนการดำเนินงานเอกสาร</a></li>
                                                    <li><a href="Report_ScoreActivity.php">คะแนนเข้าร่วมกิจกรรม</a></li>
                                                    <li><a href="Report_ScoreSendDoc.php">คะแนนการส่งเอกสาร</a></li>
                                                    <li><a href="report_grade.php">ผลการประเมิน</a></li>
                                                </ul>
                                            </li>

                                        <? } else { ?>
                                            <li><a href="index.php">หน้าหลัก</a></li>
                                            <li><a href="Webboard.php">เว็บบอร์ด</a></li>
                                            <?php if (!$_SESSION['username']) { ?>
                                                <li><a href="user_login.php">เข้าสู่ระบบ</a></li>
                                                <li><a href="submit3.php">สมัครสมาชิก</a></li>
                                            <?php } else { ?>
                                                <li><a href="user_logout.php">ออกจากระบบ</a></li>
                                                </li><?php } ?>

                                        <? } ?>

                                    </ul>
                                        </div>
                                        <div class="span3" style="height: 50px" align="right">
                                            <?php
                                            if($_SESSION['username']) {
                                            ?>
                                            <p style="margin-top: 16px;">ยินดีต้อนรับ คุณ  <?= $_SESSION['username'] ?> </p>
                                                <?php
                                            }
                                                ?>
                                        </div>
                                       
                                    </div>
    <div  class="clear"></div>
  </div>
</div>

<div class="wrapper row4">
  <div id="container" class="clear">
  
  <!-- InstanceBeginEditable name="center_body" -->


                                    <?php
                                    include ("connect2.php");

                                    $get_company_id = $_GET[company_id];
                                    $i = 0;

                                    $arr_t_id = array();


                                    if ($get_company_id) {

                                        $sql = "SELECT *FROM company, student, supervision, register, teacher
WHERE 
        register.company_id = supervision.company_id AND
            register.std_id = student.std_id AND
                teacher.t_id = supervision.t_id AND
                 position = 3 AND
                    supervision.company_id = company.company_id
                                            AND supervision.company_id = $get_company_id
                                          order by supervision.company_id,supervision.t_id desc";

                                        $rs = mysql_query($sql);

                                        $request = array();
                                        $i = 0;
                                        $j = 0;
                                        $k = 0;
                                        $x = 0;

                                        $company_id = 0;
                                        $t_id = 0;
                                        $std_id = 0;



                                        while ($row = mysql_fetch_array($rs)) {


                                            if ($company_id != $row['company_id']) {

                                                if ($company_id != 0)
                                                    $i++;
                                                $j = 0;
                                                $k = 0;
                                                $x = 0;

                                                $company_id = $row['company_id'];
                                                $request[$i]['date'] = $row['date'];
                                                $request[$i]['company_id'] = $row['company_id'];
                                                $request[$i]['company_name'] = $row['company_name'];
                                                $request[$i]['std'][$j]['std_name'] = $row['std_name'];
                                                $request[$i]['teacher'][$x]['t_name'] = $row['t_name'];

                                                $t_id = $row['t_id'];
                                                $std_id = $row['stdt_id'];
                                            } else if ($company_id == $row['company_id']) {

                                                $j++;
                                                $k++;


                                                if ($t_id != $row['t_id']) {
                                                    $x++;
                                                    $t_id = $row['t_id'];
                                                    $request[$i]['teacher'][$x]['t_name'] = $row['t_name'];
                                                } else {

                                                    $request[$i]['teacher'][$x]['t_name'] = $row['t_name'];
                                                }

                                                if ($std_id != $row['std_id']) {
                                                    $j++;
                                                    $std_id = $row['t_id'];
                                                    $request[$i]['std'][$j]['std_name'] = $row['std_name'];
                                                } else {

                                                    $request[$i]['std'][$j]['std_name'] = $row['std_name'];
                                                }




//		$request[$i]['date'][$k]['date'] = $row['date'];
                                            }
                                        }
                                    }




                                    for ($i = 0; $i < sizeof($request[0]['teacher']); $i++) {
//                                        echo $request[0]['teacher'][$i ]['t_name'];
                                    }


                                    if ($_POST['Submit']) {




                                        $chk;


                                        $date = $_POST['date'];
                                        $t_name = $_POST['t_name'];
                                        $reg_id = $_POST['reg_id'];
                                        $company_id = $_POST['company_name2'];


                                        $t_id = "";




                                        $result;
                                        include("connect2.php");


                                        if ($_POST['company_name3']) {

                                            $post_company = $_POST['company_name3'];

                                            $all_teacher = $_POST[t_name];

                                            for ($i = 0; $i < sizeof($all_teacher); $i++) {

                                                if ($all_teacher[$i]) {

                                                    $sql = "select t_id from teacher where t_name =  '$all_teacher[$i]'";

                                                    $rs = mysql_query($sql);

                                                    while ($result = mysql_fetch_array($rs)) {

                                                        $t_id = $result[t_id];
                                                    }

                                                    $sql = "select * from supervision where company_id = '$post_company'
                                                              and t_id =  '$t_id' ";

                                                    $rs = mysql_query($sql);

                                                    if (mysql_num_rows($rs) > 0) {

                                                        $sql = "UPDATE  `experience(5)`.`supervision` SET 
                                                     `t_id` =  '.$t_id.',
                                                         `date` =  '$date', 
                                                     WHERE  `supervision`.`company_id` = '$post_company' ;";

                                                        mysql_query($sql);
                                                    } else {

                                                        $sql = "INSERT INTO `experience(5)`.`supervision` 
                                                            (`su_id`, `company_id`, `date`, `t_id`) 
                                                                     VALUES 
                                                              (NULL, '$post_company', '$date', '$t_id');";

                                                        mysql_query($sql);
                                                    }
                                                }
                                            }
                                            ?>
                                            <script type="text/javascript">
                                                alert("อัพเดตข้อมูลเรียบร้อย");
                                            </script>
        <?php
    } else {

        $sql = "SELECT * FROM supervision WHERE company_id = '" . $company_id . "' "; //เช็ค หน่วยงานซ้ำ
        $result2 = mysql_query($sql);



        if (mysql_num_rows($result2) == 0) {



            for ($i = 0; $i < sizeof($t_name); $i++) {


                $sql = "SELECT t_id FROM teacher WHERE t_name = '" . $t_name[$i] . "' "; // คิวรี่ t_id
                $rs = mysql_query($sql);

                while ($rs2 = mysql_fetch_array($rs)) {

                    $t_id = $rs2[t_id];
                }

                $sql = "select std_id from register,supervision where ";
                $sql .= "supervision.company_id = register.company_id";
                $sql .= "AND position = 3";

                $rs3 = mysql_query($sql);

                for ($j = 0; $j < sizeof($rs3); $j++) {

                    $sql = "SELECT * FROM supervision WHERE company_id = '" . $company_id . "' "; //เช็ค หน่วยงานซ้ำ
                    $result3 = mysql_query($sql);

                    if (mysql_num_rows($result3) == 0) {

                        $sql = "insert into supervision(su_id,date,company_id,t_id)";
                        $sql.= "values(NULL,'$date','" . $company_id . "','$t_id')";

                        $result = mysql_query($sql);
                    } else {


                        $sql = "SELECT * FROM supervision WHERE t_id = '" . $t_id . "' ";
                        $result4 = mysql_query($sql);

                        if (mysql_num_rows($result4) == 0) {

                            $sql = "insert into supervision(su_id,date,company_id,t_id)";
                            $sql.= "values(NULL,'$date','" . $company_id . "','$t_id')";

                            $result = mysql_query($sql);
                        } else {
                            $chk = true;
                            ?>
                                                                <script type="text/javascript">
                                                                    alert(" ชื่ออาจารย์ซ้ำ หรือ ไม่มีชื่ออาจารย์ท่านนี้");
                                                                </script>
                            <?php
                        }
                    }
                }
                ?>




                <?php
            }

            if ($result && !$chk) {
                echo "<script>alert('ข้อมูลถูกบันทึกแล้ว' );
					window.location='show_supervision2.php';</script>";
            } else {
                echo mysql_error();
            }
        } else {
            ?>
                                                <script type="text/javascript">
                                                    alert("หน่วยงานซ้ำ");
                                                </script>
            <?php
        }
    }
}
?>
                                    <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
                                    <script type="text/javascript">
    
                                        function select_company()
                                        {
                                            $.ajax({
                                                url:'search_std_company.php',
                                                data: 'company_id='+$('#company_name').val(),
                                                success: function(html){
                                                    $('#show_std').html();
                                                    $('#show_std').html(html);
                                                }
                                            });
                                        }
                                        function clear_tch(label)
                                        {
                                           
                                            $("#search"+label).val('');
                                        }
                            
                                        function select_tch(){
                                            $.get("ajax5.php", { data: $("#t_id").val()}, 
                                            function(data){
                                                $("#show_tch").html(data);
                                            }
                                        );
                                        }
                                        function delete_tch(label){
                                            
                                            $("#search"+label).remove();
                                            $("#btn_delete"+label).remove();
                                            $("#btn_clear"+label).remove();
                                            $("#p_id"+label).remove();
                                           
                                        }
                                    

                                        function search_tch(label){
                                            
                                            var t_id = $("#search"+label).val();
                                            
                                              
                                                
                                            $.ajax({
                                                url:'ajax5.php',
                                                data: 't_id='+t_id,
                                                success: function(html){
                                                    $('#check'+label).html();
                                                    $('#check'+label).html(html);
                                                }
                                            });
                                       
		
                                        }
                                        
                                        
                                        function test(test){
                                            $( "#search"+label ).autocomplete(
                                            {
                                                source:'ajax6.php'
                                            })
                                            
                                        }
                                        
                                        var test2 ;
                                        function add_tch(test2)
                                        {
                                            $('#label'+test2).html('<p id="p_id'+test2+'"><input   type="text" class="required" name="t_name[]" id="search'+test2+'" onkeyup="search_tch('+test2+')"/>&nbsp;<span id="check'+test2+'">กรุณากรอกชื่ออาจารย์</span> <input type="button" value="เคลียร์" id="btn_clear'+ test2+'" onclick="clear_tch('+test2+')"/><input type="button" value="ลบ" id="btn_delete'+test2+'" style="width:40px;margin-left:5px;" onclick="delete_tch('+test2+')" /> </p><span id="label'+(test2+1)+'"></p>');
                                            $('#add_btn').attr('onclick','add_tch('+(test2+1)+')');
                                        }
                                        function add_tch2(test2){
                                            $('#label'+test2).html('<p id="p_id'+test2+'"><input readonly   type="text" class="required" name="t_name[]" id="search'+test2+'" onkeyup="search_tch('+test2+')"/>&nbsp;<span id="check'+test2+'">กรุณากรอกชื่ออาจารย์</span> <input type="button" value="เคลียร์" id="btn_clear'+ test2+'" onclick="clear_tch('+test2+')"/><input type="button" value="ลบ" id="btn_delete'+test2+'" style="width:40px;margin-left:5px;" onclick="delete_tch('+test2+')" /> </p><span id="label'+(test2+1)+'"></p>');
                                            $('#add_btn').attr('onclick','add_tch('+(test2+1)+')');
                                        }
                                      
      
                                    </script>

                                    <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
                                    <script type='text/javascript' src='js/jquery.validate.js'></script> 
                                    <link type="text/css" href="css/ui-lightness/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" media="screen"/>
                                    <script type="text/javascript">
                                        $(function(){
                                            $.datepicker.setDefaults(
                                            $.extend($.datepicker.regional[""])
                                        );
                                            $("#datepicker").datepicker({dateFormat: "yy-mm-dd"});
                                        });
                                        
                                        $(document).ready(function(){
                                            $('#form1').validate();
                                            select_company();
                                            
                                         
                                          
                                            
                                        });
                                            
                                        
                                        
                                        
                                    </script>





                                    <div align="center"><form id="form1" name="form1" method="post" action="">

                                            <table width="608" height="396" border="0">
                                                <tr>
                                                    <td height="102" colspan="2" align="center" class="head_submit"><strong>เพิ่มข้อมูลตารางการจัดการนิเทศนักศึกษา</strong> </td>
                                                </tr>
                                                <tr>
                                                    <td width="381" align="right"><span class="bodyresome">วันที่นิเทศ : </span></td>
                                                    <td width="567"><span class="bodyresome">
                                                            <label>
                                                                <input type="text" name="date" id="datepicker" class="required" value="<?php echo $get_company_id ? $request[0]['date'] : '' ?>" />
                                                            </label>
                                                        </span></td>
                                                </tr>
                                                <tr>
                                                    <td align="right">หน่วยงาน :</td>
                                                    <td>

<?php
if ($get_company_id) {
    ?>
                                                            <input type="hidden" name="company_name3" value="<?= $get_company_id ?>" />
                                                            <select disabled name="company_name3" id="company_name" onchange="select_company()">


    <?php
} else {
    ?>
                                                                <select  name="company_name2" id="company_name" onchange="select_company()">
                                                                <?php
                                                            }
                                                            ?>


                                                                <option value="">กรุณาเลือกหน่วยงาน</option>
<?php
include("connect2.php");

$sql = "SELECT * FROM register,company WHERE register.company_id = company.company_id 
															AND position = 3
															GROUP BY company.company_id";
$rs = mysql_query($sql);



while ($row = mysql_fetch_array($rs)) {
    ?>                                      <?php
                                                                if ($get_company_id == $row['company_id']) {
        ?>



                                                                        <option  selected="selected" value="<?= $row['company_id'] ?>"   >
        <?= $row['company_name'] ?>
                                                                        </option>
                                                                        <?php } else { ?>
                                                                        <option value="<?= $row['company_id'] ?>"   >
                                                                        <?= $row['company_name'] ?>
                                                                        </option>
                                                                        <?php } ?>


                                                                <?php } ?>
                                                            </select>        <label for="select2"></label></td>
                                                </tr>
                                                <tr valign="top">
                                                    <td align="right">รายชื่อนักศึกษา : </td>
                                                    <td><span id="show_std"></span></td>
                                                </tr>

<?php if ($get_company_id) { ?>

                                                    <tr>
                                                        <td align="right">เพิ่มอาจารย์ :</td>    
                                                        <td>
                                                            <input type="text"  id="search<?= sizeof($request[0]['teacher']) ?>" name="t_name[]" onkeyup="search_tch(<?= sizeof($request[0]['teacher']) ?>)"  />
                                                            <span id="check<?= sizeof($request[0]['teacher']) ?>">กรุณากรอกชื่ออาจารย์</span>
                                                            <input type="button" value="เคลียร์" onClick="clear_tch(<?= sizeof($request[0]['teacher']) ?>)"/>
                                                            <input type="button" value="เพิ่มรายชื่อ" onClick="add_tch(<?= sizeof($request[0]['teacher']) + 1 ?>)" id="add_btn"/>
                                                        </td>  
                                                    </tr>


<?php } else { ?>

                                                    <tr>
                                                        <td align="right">เพิ่มอาจารย์ :</td>    
                                                        <td>
                                                            <input type="text"  id="search0" name="t_name[]" onkeyup="search_tch(0)"  />
                                                            <span id="check0">กรุณากรอกชื่ออาจารย์</span>
                                                            <input type="button" value="เคลียร์" onClick="clear_tch(0)"/>
                                                            <input type="button" value="เพิ่มรายชื่อ" onClick="add_tch(1)" id="add_btn"/>
                                                        </td>  
                                                    </tr>
<?php } ?>

                                                <?php
                                                if ($get_company_id) {

                                                    for ($i = 0; $i < sizeof($request[0]['teacher']); $i++) {

//                                                        echo $request[0]['teacher'][$i][t_name];
                                                        ?>
                                                        <tr>
                                                            <td></td>
                                                            <td>
                                                                <input readonly type="text" id="search<?= $i ?>" value="<?= $request[0]['teacher'][$i]['t_name']; ?>" name="t_name[]" onkeyup="search_tch(<?= $i ?>)"  />
                                                                <span id="check<?= $i ?>"><p id="p_id<?= $i ?>" style="color:green;display: inline;">เพิ่มอาจารย์</p></span>
                                                                <input type="button" id="btn_clear<?= $i ?>" value="เคลียร์" onClick="clear_tch(<?= $i ?>)"/>
                                                                <input type="button" value="ลบ" id="btn_delete<?= $i ?>" style="width:40px;margin-left:5px;" onclick="delete_tch(<?= $i ?>)" /> </p><span id="label<?= $i ?>"></p>
                                                            </td>
                                                        </tr>


    <?php }
    ?>
                                                    <tr >
                                                        <td></td>
                                                        <td id="label<?php echo sizeof($request[0]['teacher']) + 1 ?>"></td>
                                                    </tr>
<?php } else {
    ?>

                                                    <tr >
                                                        <td></td>
                                                        <td id="label1"></td>
                                                    </tr>
<?php } ?>

                                                <tr>,
                                                    <td height="80" colspan="2" align="center"><input name="Submit" type="submit" class="bodyresome" id="Sumit" value="บันทึก" />
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input name="Reset" type="reset" class="bodyresome" id="Reset" value="เคลียร์" /></td>
                                                </tr>
                                            </table>

                                        </form></div>
                                    <p></p>
                                    <div align="left"><a href="show_supervision2.php" class="link">ย้อนกลับ</a></div>
                                    &nbsp;
                                    &nbsp;
                                    <!-- InstanceEndEditable --></div>
</div>


<div class="wrapper">
  <div id="copyright" class="clear">
  <div style="width:960px;margin:0 auto; ">
    <p class="fl_left">Copyright (c) 2012 <a href = "http://webhosting.udru.ac.th/~std52040249439" target="_blank">http://www.gen-dev-soft.com/experience-csit/</a> All rights reserved. 
<br>
Design by Nittaya Kakulphin & Benjawan Sriralat @ Udonthani Rajabhat University.</p>
    </div>
  </div>
</div>
</body>
<!-- InstanceEnd --></html>
