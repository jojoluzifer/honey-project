<?
ob_start();
	session_start();

$type_user = $_SESSION['usertype'];

if ($type_user != "teacher")
	{
	
		header("Location:user_login.php");
		
		
	}
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="EN" lang="EN" dir="ltr"><!-- InstanceBegin template="/Templates/index_full.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head profile="http://gmpg.orgs/xfn/11">
<title>ระบบสารสนเทศฝึกประสบการณ์วิชาชีพฯ :<?php echo $_SESSION['usertype']; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="imagetoolbar" content="no" />
<link rel="stylesheet" href="css/layout.css" type="text/css" />
<link rel="stylesheet" href="css/layout.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/style-mix.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="style.css" />
   
        
        <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="jquery-1.7.2.min.js"></script>
     

</head>
<body id="top">
<div class="wrapper row1">
  <div id="header" class="clear">
    <div class="fl_left">
    <p><img src="img/logo5.png" width="80" height="100"></p>
    </div>
      <div class="fl_center">
      <div class="fl_right" align="right">
      <ul>
        <li><a href="index.php">หนัาหลัก</a></li>
        <li><a href="Webboard.php">เว็บบอร์ด</a></li>
         <?php if(!$_SESSION['username']){?>
        <li><a href="user_login.php">เข้าสู่ระบบ</a></li>
        <li><a href="submit3.php">สมัครสมาชิก</a></li>
        <?php } else { ?>
        <li class="last"><a href="user_logout.php">ออกจากระบบ</a></li>
        <?php } ?>
      </ul>
    </div>
      <br>
      <br>
      <h1>&nbsp;&nbsp;ระบบสารสนเทศฝึกประสบการณ์วิชาชีพ</h1>
      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;สาขาวิทยาการคอมพิวเตอร์และเทคโนโลยีสารสนเทศ</p>
    </div>
 
  </div>
</div>

<div class="wrapper row2">
  <div id="topnav">
                                    <div class="row-fluid">
                                        <div class="span9">
                                            <ul>

                                        <!-- admin -->
                                        <? if ($_SESSION["usertype"] == "admin") { ?>
                                            <li><a href="show_std.php">จัดการข้อมูลนักศึกษา</a></li>
                                            <li><a href="show_teacher.php">จัดการข้อมูลอาจารย์</a></li>
                                            <li><a href="show_company.php">จัดการข้อมูลหน่วยงาน</a></li>

                                            <!-- company -->
                                        <? } else if ($_SESSION["usertype"] == "company") { ?>
                                         <li><a href="company_detail.php">ข้อมูลหน่วยงาน</a></li>
                                            <li><a href="show_request_company.php">แจ้งความจำนงค์รับนักศึกษา</a></li>
                                            <li><a href="report_StdResume.php">รายงานประวัติส่วนตัวของนักศึกษา</a></li>
                                            <li><a href="manage_score_forcompany.php">กรอกข้อมูลประเมินผล</a></li>

                                        <? } else if ($_SESSION["usertype"] == "officer") { ?>

                                            <!-- officer -->
                                            <li><a href="#">จัดการข้อมูลหนังสือฝึกประสบการณ์วิชาชีพ</a>
                                                <ul>
                                                    <li><a href="manage_send_request.php">หนังสือขอความอนุเคราะห์ฝึกประสบการณ์วิชาชีพ</a></li>
                                                    <li><a href="manage_sendSupervision.php">หนังสือนิเทศนักศึกษา</a></li>
                                                    <li><a href="manage_sendStd.php">หนังสือส่งตัวนักศึกษา</a></li>
                                                    <li><a href="manage_sendStd1.php">หนังสือขอตัวนักศึกษา</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">จัดการข้อมูลหนังสือโครงการ</a>
                                                <ul>
                                                    <li><a href="manage_sendProject.php">โครงการปฐมนิเทศก่อนฝึกประสบการณ์</a></li>
                                                    <li><a href="manage_sendSamana.php">โครงการสัมนาหลังฝึกประสบการณ์</a></li>
                                                </ul>
                                            </li>

                                        <? } else if ($_SESSION["usertype"] == "student") { ?>
                                            <!-- student -->
                                            <li><a href="resume2.php">ประวัติส่วนตัว</a></li>
                                            <?php 
											 include "connect2.php";
										   $sql = "select * from register,student where student.std_id = register.std_id and student.std_id = '".$_SESSION['std_id']."'  ";
										   $rs = mysql_query($sql);
										   $row = mysql_fetch_array($rs);
										   $num_rows = mysql_num_rows($rs);
										   
										   $position = $row['position']; 
										   
											if($num_rows != 0){?>
												
                                            <li><a href="StdRequest2.php">ส่งคำร้องขอฝึกประสบการณ์</a></li>
                                            <li><a href="result_request.php">ผลอนุมัติการฝึกประสบการณ์</a></li>
                                            
                                            <?php  if ($position == 3){?>
                                            
                                            <li><a href="show_diary.php">บันทึกประจำวัน</a></li>
                                            
                                            <?php }
											} ?>

                                        <? } else if ($_SESSION["usertype"] == "teacher_general") { ?>
                                            <!-- teacher_general -->
                                            <li><a href="show_supervision_forTG.php">ตารางการนิเทศ</a></li>
                                            <li><a href="show_std_TG.php">ข้อมูลนักศึกษาฝึกประสบการณ์</a></li>
                                            <li><a href="show_company_TG.php">ข้อมูลแหล่งฝึกประสบการณ์</a></li>
                                            <li><a href="show_news_forteachergeneral.php">จัดการข่าวประชาสัมพันธ์</a></li>

                                        <? } else if ($_SESSION["usertype"] == "teacher") { ?>
                                            <!-- teacher -->
                                            <li><a href="#">จัดการข้อมูลทั่วไป</a>
                                                <ul>
                                                    <li><a href="show_std_forteacher.php">ข้อมูลนักศึกษา</a></li>
                                                    <li><a href="show_company_forteacher.php">ข้อมูลแหล่งฝึกประสบการณ์</a></li>
                                                    <li><a href="show_project.php">ข้อมูลโครงการ</a></li>
                                                    <li><a href="show_group.php">ข้อมูลหมู่เรียน</a></li>
                                                    <li><a href="register_new3.php">จัดการหมู่เรียนนักศึกษา</a></li>
                                                      <li><a href="manage_score.php">กรอกข้อมูลประเมินผล</a></li>
                                                          <li><a href="show_titlescore.php">ข้อมูลหัวข้อการประเมินผล</a></li>
                                                    <li><a href="show_news.php">ข้อมูลข่าวประชาสัมพันธ์</a></li>
                                                    <li><a href="show_webboard.php">ข้อมูลกระทู้</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">จัดการข้อมูลการนิเทศ</a>
                                                <ul>
                                                    <li><a href="show_supervision2.php">ตารางการนิเทศนักศึกษา</a></li>
                                                    <li><a href="show_advice.php">ปัญหาที่พบและข้อเสนอแนะจากการนิเทศนักศึกษา</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="confirmRQ.php">อนุมัติแหล่งฝึกงาน</a></li>
                                            <li><a href="company_comfirmRQ.php">เปลี่ยนสถานะตอบรับการฝึก</a></li>
                                            <li><a href="#">ดูรายงาน</a>
                                                <ul>
                                                 <li><a href="show_std_mange.php">ดูรายชื่อนักศึกษา</a></li>
                                                    <li><a href="report_std+company.php">รายชื่อแหล่งฝึกประสบการณ์
                                                            พร้อมทั้งชื่อนักศึกษา</a></li>
                                                             <li><a href="show_request.php">การแจ้งความจำนงขอรับนักศึกษาฝึกประสบการณ์วิชาชีพ
                                                            </a></li>
                                                    <li><a href="Report_Supervision.php">ตารางนิเทศนักศึกษา</a></li>
                                                    <li><a href="report_diary.php">บันทึกประจำวันของนักศึกษา</a></li>
                                                    <li><a href="report_advice.php">ปัญหาที่พบและ
                                                            ข้อเสนอแนะจากหน่วยงาน</a></li>
                                                    <li><a href="show_stdScore.php">ผลการประเมินจากหน่วยงาน</a></li>
                                                    <li><a href="Report_Score.php">คะแนนของนักศึกษา</a></li>
                                                    <li><a href="Report_ScoreSupervision.php">คะแนนการนิเทศ</a></li>
                                                    <li><a href="Report_ScoreDocument.php">คะแนนการดำเนินงานเอกสาร</a></li>
                                                    <li><a href="Report_ScoreActivity.php">คะแนนเข้าร่วมกิจกรรม</a></li>
                                                    <li><a href="Report_ScoreSendDoc.php">คะแนนการส่งเอกสาร</a></li>
                                                    <li><a href="report_grade.php">ผลการประเมิน</a></li>
                                                </ul>
                                            </li>

                                        <? } else { ?>
                                            <li><a href="index.php">หน้าหลัก</a></li>
                                            <li><a href="Webboard.php">เว็บบอร์ด</a></li>
                                            <?php if (!$_SESSION['username']) { ?>
                                                <li><a href="user_login.php">เข้าสู่ระบบ</a></li>
                                                <li><a href="submit3.php">สมัครสมาชิก</a></li>
                                            <?php } else { ?>
                                                <li><a href="user_logout.php">ออกจากระบบ</a></li>
                                                </li><?php } ?>

                                        <? } ?>

                                    </ul>
                                        </div>
                                        <div class="span3" style="height: 50px" align="right">
                                            <?php
                                            if($_SESSION['username']) {
                                            ?>
                                            <p style="margin-top: 16px;">ยินดีต้อนรับ คุณ  <?= $_SESSION['username'] ?> </p>
                                                <?php
                                            }
                                                ?>
                                        </div>
                                       
                                    </div>
    <div  class="clear"></div>
  </div>
</div>

<div class="wrapper row4">
  <div id="container" class="clear">
  
  <!-- InstanceBeginEditable name="center_body" -->

        <form id="form1" method="post" action="">
        <div align="left">
    <p>
 <?php
include "connect2.php";
?>
    </p>
    <p align="center"><strong>จัดการข้อมูลปัญหาที่พบและข้อเสนอแนะจากการนิเทศ</strong></p>
    <br>
    <br>
  <form id="form1" name="form2" method="post" action="">
       
       <div class="form-search">
           <div class="row-fluid">
               <div class="span3">
                   <div class="row-fluid">
                       <div class="span5">รหัสนักศึกษา :</div>
                       <div class="span7">
                           <input type="text" name="id" />
                       </div>
                   </div>
               </div>
                 <div class="span3">
                    <div class="row-fluid">
                       <div class="span5">คำนำหน้า :</div>
                       <div class="span7">
                           <input type="text" name="perfix" />
                       </div>
                   </div>
               </div>
               <div class="span3">
                    <div class="row-fluid">
                       <div class="span5">ชื่อนักศึกษา :</div>
                       <div class="span7">
                           <input type="text" name="name" />
                       </div>
                   </div>
               </div>
             
               <div class="span1">
               <input name="Search" type="submit" value="ค้นหา" />
               </div>
           </div>
           
           <div class="row-fluid">
             <div class="span3">
                    <div class="row-fluid">
                       <div class="span5">สาขา :</div>
                       <div class="span7">
                           <input type="text" name="major" />
                       </div>
                          
                    </div>
               </div>
               <div class="span3">
                   <div class="row-fluid">
                       <div class="span5">หมู่เรียน :</div>
                       <div class="span7">
                           <input type="text" name="group" />
                       </div>
                   </div>
               </div>
               <div class="span3">
                    <div class="row-fluid"></div>
               </div>
               <div class="span3">
                    <div class="row-fluid"></div>
               </div>
               
           </div>
           
       </div>
         </form>     
         <br>
   <div align="center"><table width="600" border="1" cellspacing="0" cellpadding="0">
      <tr>
        <td bgcolor="#66CCCC" align="center" width="100">รหัสนักศึกษา</td>
        <td bgcolor="#66CCCC" align="center" width="100">คำนำหน้า</td>
        <td bgcolor="#66CCCC" align="center" width="140" >ชื่อนักศึกษา</td>
        <td bgcolor="#66CCCC" align="center" width="100">หน่วยงาน</td>
        <td bgcolor="#66CCCC" align="center" width="140">คะแนนรวมบุคลิกภาพ</td>
        <td bgcolor="#66CCCC" align="center" width="140">คะแนนรวมการปฎิบัติงาน</td>
        <td bgcolor="#66CCCC" align="center" width="140">คะแนนรวมเวลาในการปฎิบัติงาน</td>
         <td bgcolor="#66CCCC" align="center" width="50">รวม</td>
       
      </tr>
      <?

 $sql_show = "SELECT sum(choice) as sum_score,register.std_id as std_id,company_name as company_name,
     std_name as std_name,register.register_id as register_id,perfix as perfix
     
     
FROM `score`,student,register ,company
WHERE tscore_id = 17 
and score.register_id = register.register_id
and student.std_id = register.std_id 
and register.company_id = company.company_id
";
if($_POST['Search'])
{
if ($_POST['id']) {

        $id = $_POST['id'];

        $sql_show .= " and student.std_id like '%$id%'";
    }
	 if ($_POST['perfix']) {

        $perfix = $_POST['perfix'];

        $sql_show .= " and perfix like '%$perfix%'";
    }
    if ($_POST['name']) {

        $name = $_POST['name'];

        $sql_show .= " and std_name like '%$name%'";
    }
    if ($_POST['major']) {

        $major = $_POST['major'];

        $sql_show .= " and major like '%$major%'";
    }
	 if ($_POST['group']) {

        $group = $_POST['group'];

        $sql_show .= " and register.groups_num like '%$group%'";
    }
}



$sql_show .= "group by register.std_id order by company.company_id";




$result_show = mysql_query($sql_show,$conn) or die(mysql_error());



while($row_show = mysql_fetch_array($result_show))
{
    $id = $row_show['register_id'] ;
    $work_sum_score = '' ;
    $time_sum_score = '' ;
    
     $sql2 = "SELECT sum(choice) as sum_score2,register.std_id as std_id,company_name as company_name,
     std_name as std_name
     
     
FROM `score`,student,register ,company
WHERE tscore_id = 20
and score.register_id = register.register_id
and student.std_id = register.std_id 
and register.company_id = company.company_id
and score.register_id = '$id'
group by register.std_id
order by company.company_id
";
$rs2 = mysql_query($sql2);


 $sql3 = "SELECT time_id,time
FROM `score`
WHERE tscore_id = 24
and score.register_id = '$id'
    order by time_id;
";
$rs3 = mysql_query($sql3);



while($rs4 = mysql_fetch_array($rs2)){
    $work_sum_score = $rs4['sum_score2'];
}

while($rs5 = mysql_fetch_array($rs3)){
    
        if($rs5['time_id'] == '1'){
           $time_sum_score += ($rs5[time] * 2);
        }
        if($rs5['time_id'] == '2'){
          $time_sum_score += ($rs5[time] * 2);
        }
        if($rs5['time_id'] == '3'){
          $time_sum_score += ($rs5[time] * 1);
        }
        if($rs5['time_id'] == '4'){
          $time_sum_score += ($rs5[time] * 4);
        }
 
 
}
 if($time_sum_score > 20){
      $time_sum_score = 0;
  }
  else{
      $time_sum_score = 20 - $time_sum_score;
  }

$sum_all_score = 0;
$sum_all_score = intval($row_show['sum_score']) + intval($work_sum_score) + intval($time_sum_score) ;

?>
      <tr>
        <td style="text-align:center"><?=$row_show['std_id']?></td>
         <td ><?=$row_show['perfix']?></td>
        <td ><?=$row_show['std_name']?></td>
        <td ><?=$row_show['company_name']?></td>
        <td style="text-align:center"><?=$row_show['sum_score']?></td>
        <td style="text-align:center"><?= $work_sum_score?></td>
        <td style="text-align:center"><?= $time_sum_score ?> </td>
        <td style="text-align:center"><?= $sum_all_score ?> </td>

     
      </tr>
      <?
}
?>

    </table></div>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <div align="left"><p><a href="show_stdScore.php">ย้อนกลับ</a></p></div>
  </div>
  


        </form>
		<!-- InstanceEndEditable --></div>
</div>


<div class="wrapper">
  <div id="copyright" class="clear">
  <div style="width:960px;margin:0 auto; ">
    <p class="fl_left">Copyright (c) 2012 <a href = "http://webhosting.udru.ac.th/~std52040249439" target="_blank">http://www.gen-dev-soft.com/experience-csit/</a> All rights reserved. 
<br>
Design by Nittaya Kakulphin & Benjawan Sriralat @ Udonthani Rajabhat University.</p>
    </div>
  </div>
</div>
</body>
<!-- InstanceEnd --></html>