<?php
define('FPDF_FONTPATH', 'font/');
require('fpdf.php');

function convert($str)
{
    return iconv('UTF-8', 'cp874', $str);
}


function thai_date($date)
{
	$date = $date+0;
	
	$thai_date = "";
	for($i=0 ; $i<strlen($date) ; $i++)
	{
		switch(substr($date,$i,1))
		{
			case '0' : $thai_date.="๐"; break;
			case '1' : $thai_date.="๑"; break;
			case '2' : $thai_date.="๒"; break;
			case '3' : $thai_date.="๓"; break;
			case '4' : $thai_date.="๔"; break;
			case '5' : $thai_date.="๕"; break;
			case '6' : $thai_date.="๖"; break;
			case '7' : $thai_date.="๗"; break;
			case '8' : $thai_date.="๘"; break;
			case '9' : $thai_date.="๙"; break;
			
		}
	}
	return $thai_date;
}


$month = array(
    '1' => 'มกราคม',
    '2' => 'กุมภาพันธ์',
    '3' => 'มีนาคม',
    '4' => 'เมษายน',
    '5' => 'พฤษภาคม',
    '6' => 'มิถุนายน',
    '7' => 'กรกฎาคม',
    '8' => 'สิงหาคม',
    '9' => 'กันยายน',
    '10' => 'ตุลาคม',
    '11' => 'พฤศจิกายน',
    '12' => 'ธันวาคม',
    );

include "connect2.php";

$sql = "select * from company where ( company.company_id='".$_GET['company_id']."' ) ";
$result = mysql_query($sql);

$rs = mysql_fetch_array($result);

#CREATE PDF
$pdf = new FPDF('P','mm','A4');

$pdf->AddFont('angsana', '', 'angsa.php');

// เพิ่มฟอนต์ภาษาไทยเข้ามา ตัวหนา  กำหนด ชื่อ เป็น angsana
$pdf->AddFont('angsana', 'B', 'angsab.php');

// เพิ่มฟอนต์ภาษาไทยเข้ามา ตัวหนา  กำหนด ชื่อ เป็น angsana
$pdf->AddFont('angsana', 'I', 'angsai.php');

// เพิ่มฟอนต์ภาษาไทยเข้ามา ตัวหนา  กำหนด ชื่อ เป็น angsana
$pdf->AddFont('angsana', 'BI', 'angsaz.php');
#END CREATE PDF


$pdf->AddPage();

$pdf->Image("img/export.png", 95, 10, 20, 20);

$pdf->SetFont('angsana', '', 16);
$pdf->Ln(16);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
if($_POST[num]){
$pdf->Cell(100, 0, convert($num), 0, 0, 'L');}else{
$pdf->Cell(100, 0, convert("ที่  ศธ ๐๕๔๓.๖/ว๒๑0"), 0, 0, 'L');}
$pdf->Cell(120, 0, convert("คณะวิทยาศาสตร์"), 0, 0, 'L');
$pdf->Ln(7);
$pdf->Cell(120, 0, convert(""), 0, 0, 'L');
$pdf->Cell(120, 0, convert("มหาวิทยาลัยราชภัฏอุดรธานี"), 0, 0, 'L');
$pdf->Ln(7);
$pdf->Cell(120, 0, convert(""), 0, 0, 'L');
$pdf->Cell(120, 0, convert("อ.เมือง    จ.อุดรธานี  ๔๑๐๐๐"), 0, 0, 'L');
$pdf->Ln(10);
$pdf->Cell(100, 0, convert(""), 0, 0, 'L');
if($_POST[date]){
$pdf->Cell(120, 0, convert($date), 0, 0, 'L');}else{
$pdf->Cell(120, 0, convert(thai_date(date('d'))." ".$month[date('m')+0]." ".thai_date(date('Y')+543)), 0, 0, 'L');}

$pdf->Ln(10);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(10, 0, convert("เรื่อง"), 0, 0, 'L');
$pdf->Cell(100,0,convert("ขอความอนุเคราะห์รับนักศึกษาเข้าฝึกประสบการณ์วิชาชีพ"),0,0,"L");

$pdf->Ln(7);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(10, 0, convert("เรียน"), 0, 0, 'L');
$pdf->Cell(100,0,convert($rs['boos']),0,0,"L");

$pdf->Ln(7);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(23, 0, convert("สิ่งที่ส่งมาด้วย"), 0, 0, 'L');
$pdf->Cell(100,0,convert("แบบฟอร์มยืนยันการตอบรับนักศึกษาเข้าฝึกประสบการณ์วิชาชีพ"),0,0,"L");

$pdf->Ln(12);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(15, 0, convert(""), 0, 0, 'L');
$pdf->Cell(100,0,convert("ด้วยคณะวิทยาศาสตร์  มหาวิทยาลัยราชภัฏอุดรธานี  ได้เปิดสอนระดับปริญญาตรี  สาขาวิชา"),0,0,"L");

$pdf->Ln(7);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(100,0,convert("เทคโนโลยีสารสนเทศ ซึ่งกำหนดให้นักศึกษาต้องฝึกประสบการณ์วิชาชีพเป็นเวลาไม่น้อยกว่า ๒๘๐ "),0,0,"L");

$pdf->Ln(7);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
if($_POST[strat]){
$pdf->Cell(100,0,convert("ชั่วโมง  โดยนักศึกษาออกฝึกประสบการณ์วิชาชีพ  ระหว่างวันที่ ".$strat." ถึงวันที่"),0,0,"L");}else{
$pdf->Cell(100,0,convert("ชั่วโมง  โดยนักศึกษาออกฝึกประสบการณ์วิชาชีพ  ระหว่างวันที่ ๑  เมษายน ๒๕๕๕ ถึงวันที่"),0,0,"L");}

$pdf->Ln(7);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
if($_POST[end]){
$pdf->Cell(100,0,convert($end),0,0,"L");}else{
$pdf->Cell(100,0,convert("๓๑ กันยายน ๒๕๕๕"),0,0,"L");}

$pdf->Ln(12);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(15, 0, convert(""), 0, 0, 'L');
$pdf->Cell(100,0,convert("ตามที่นักศึกษาดังรายชื่อที่ปรากฏในแบบฟอร์มที่แนบมานี้ ได้ติดต่อกับทางหน่วยงานของท่านไว้"),0,0,"L");

$pdf->Ln(7);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(100,0,convert("แล้วอย่างไม่เป็นทางการและหน่วยงานของท่านประสงค์ที่จะรับแล้วนั้น เพื่อให้การฝึกประสบการณ์วิชาชีพ"),0,0,"L");

$pdf->Ln(7);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(100,0,convert("บรรลุวัตถุประสงค์ จึงใคร่ขอความอนุเคราะห์จากหน่วยงานของท่านรับนักศึกษาดังรายชื่อต่อไปนี้เพื่อฝึก"),0,0,"L");

$pdf->Ln(7);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(100,0,convert("ประสบการณ์วิชาชีพ"),0,0,"L");

$sql = "SELECT * FROM register,student WHERE register.std_id=student.std_id AND company_id='".$_GET['company_id']."' ";
$result_std = mysql_query($sql);

$chk = mysql_num_rows($result_std);

$i=1;

while($row_show = mysql_fetch_array($result_std)){
$pdf->Ln(7);
$pdf->Cell(50, 0, convert(""), 0, 0, 'L');
$pdf->Cell(100,0,convert(($chk>1 ? thai_date($i).". " : " ").$row_show['perfix'].$row_show['std_name']),0,0,"L");
$i++;
}
$pdf->Ln(10);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(15, 0, convert(""), 0, 0, 'L');
$pdf->Cell(100,0,convert("จึงเรียนมาเพื่อโปรดพิจารณา  และขอขอบพระคุณเป็นอย่างสูงมา ณ โอกาสนี้"),0,0,"L");

$pdf->Ln(15);
$pdf->Cell(100, 0, convert(""), 0, 0, 'L');
$pdf->Cell(60,0,convert("ขอแสดงความนับถือ"),0,0,"C");

$pdf->Ln(20);
$pdf->Cell(100, 0, convert(""), 0, 0, 'L');
$pdf->Cell(60,0,convert("(นายอนุกูล  อ่อนสวัสดิ์)"),0,0,"C");

$pdf->Ln(6);
$pdf->Cell(100, 0, convert(""), 0, 0, 'L');
$pdf->Cell(60,0,convert("คณบดีคณะวิทยาศาสตร์"),0,0,"C");



$pdf->SetFont('angsana', '', 13);
$pdf->Ln(15);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(60,0,convert("สาขาวิชาเทคโนโลยีสารสนเทศ  คณะวิทยาศาสตร์"),0,0,"L");
$pdf->Ln(6);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(60,0,convert("โทรศัพท์  ๐-๔๒๒๑-๑๐๔๐  ต่อ  ๒๐๕"),0,0,"L");
$pdf->Ln(6);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(60,0,convert("โทรสาร  ๐-๔๒๓๔-๑๖๑๕"),0,0,"L");

$pdf->AddPage();

$pdf->Image("img/logo.png", 95, 10, 20, 26);

$pdf->Ln(35);
$pdf->SetFont('angsana', 'B', 16);
$pdf->Cell(0,0,convert("แบบฟอร์มยืนยันการตอบรับนักศึกษาเข้าฝึกประสบการณ์วิชาชีพ"),0,0,"C");
$pdf->Ln(7);
$pdf->SetFont('angsana', '', 16);
$pdf->Cell(0,0,convert("( นักศึกษาสาขาวิชา".$rs['major']." )"),0,0,"C");

$pdf->Ln(20);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(100,0,convert("๑. ชื่อหน่วยงาน ".$rs['company_name']),0,0,"L");

$pdf->Ln(8);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(100,0,convert("๒. ที่อยู่ ".$rs['location']),0,0,"L");

$pdf->Ln(8);
$pdf->Cell(23, 0, convert(""), 0, 0, 'L');
$pdf->Cell(100,0,convert("  …………………………………………………………………………………………………………"),0,0,"L");

$pdf->Ln(8);
$pdf->Cell(23, 0, convert(""), 0, 0, 'L');
$pdf->Cell(100,0,convert("  หมายเลขโทรศัพท์ ".$rs['phone']),0,0,"L");

$pdf->Ln(8);
$pdf->Cell(23, 0, convert(""), 0, 0, 'L');
$pdf->Cell(100,0,convert("  โทรสาร ".$rs['phone']),0,0,"L");

$pdf->Ln(8);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(100,0,convert("๓. หน่วยงานของท่านสามารถรับนักศึกษาดังรายชื่อต่อไปนี้เพื่อฝึกประสบการณ์วิชาชีพ"),0,0,"L");

$sql = "SELECT * FROM register,student WHERE register.std_id=student.std_id AND company_id='".$_GET['company_id']."' ";
$result_std = mysql_query($sql);

$i=1;

$chk = mysql_num_rows($result_std);

while($row_show = mysql_fetch_array($result_std)){
	$pdf->Ln(7);
	$pdf->Cell(30, 0, convert(""), 0, 0, 'L');
	
	$pdf->Cell(100,0,convert(($chk>1 ? thai_date($i).". " : " ").$row_show['perfix'].$row_show['std_name']),0,0,"L");
	$i++;
}

$pdf->Ln(8);
$pdf->Cell(24, 0, convert(""), 0, 0, 'L');
$pdf->Cell(100,0,convert("ตามรายชื่อข้างต้นเห็นสมควรว่า"),0,0,"L");



$pdf->Ln(8);
$pdf->Cell(40, 0, convert(""), 0, 0, 'L');
$pdf->Cell(20,0,convert("รับ"),0,0,"L");
$pdf->Cell(25,0,convert("ไม่รับ"),0,0,"L");
$pdf->Cell(20,0,convert("อื่นๆ ..................................................................."),0,0,"L");

$pdf->Ln(30);
$pdf->Cell(95, 0, convert(""), 0, 0, 'L');
$pdf->Cell(40,0,convert("(................................................................................................)"),0,0,"C");

$pdf->Ln(10);
$pdf->Cell(95, 0, convert(""), 0, 0, 'L');
$pdf->Cell(40,0,convert("ตำแหน่ง...................................................................................."),0,0,"C");

$pdf->Ln(10);
$pdf->Cell(95, 0, convert(""), 0, 0, 'L');
$pdf->Cell(40,0,convert("ผู้ให้ข้อมูล"),0,0,"C");


$pdf->Ln(20);
$pdf->SetFont('angsana', 'B', 16);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(60,0,convert("กรุณาส่งแบบสำรวจกลับตามที่อยู่นี้"),0,0,"L");
$pdf->SetFont('angsana', '', 16);
$pdf->Ln(7);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(60,0,convert("สาขาวิชาเทคโนโลยีสารสนเทศ  คณะวิทยาศาสตร์"),0,0,"L");
$pdf->Ln(7);
$pdf->Cell(20, 0, convert(""), 0, 0, 'L');
$pdf->Cell(60,0,convert("มหาวิทยาลัยราชภัฏอุดรธานี อ.เมือง จ.อุดรธานี  ๔๑๐๐๐"),0,0,"L");

$pdf->Output();

?>