<? 
ob_start();
session_start(); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="EN" lang="EN" dir="ltr"><!-- InstanceBegin template="/Templates/index_full.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head profile="http://gmpg.orgs/xfn/11">
<title>ระบบสารสนเทศฝึกประสบการณ์วิชาชีพฯ :<?php echo $_SESSION['usertype']; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="imagetoolbar" content="no" />
<link rel="stylesheet" href="css/layout.css" type="text/css" />
<link rel="stylesheet" href="css/layout.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/style-mix.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="style.css" />
   
        
        <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="jquery-1.7.2.min.js"></script>
     

</head>
<body id="top">
<div class="wrapper row1">
  <div id="header" class="clear">
    <div class="fl_left">
    <p><img src="img/logo5.png" width="80" height="100"></p>
    </div>
      <div class="fl_center">
      <div class="fl_right" align="right">
      <ul>
        <li><a href="index.php">หนัาหลัก</a></li>
        <li><a href="Webboard.php">เว็บบอร์ด</a></li>
         <?php if(!$_SESSION['username']){?>
        <li><a href="user_login.php">เข้าสู่ระบบ</a></li>
        <li><a href="submit3.php">สมัครสมาชิก</a></li>
        <?php } else { ?>
        <li class="last"><a href="user_logout.php">ออกจากระบบ</a></li>
        <?php } ?>
      </ul>
    </div>
      <br>
      <br>
      <h1>&nbsp;&nbsp;ระบบสารสนเทศฝึกประสบการณ์วิชาชีพ</h1>
      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;สาขาวิทยาการคอมพิวเตอร์และเทคโนโลยีสารสนเทศ</p>
    </div>
 
  </div>
</div>

<div class="wrapper row2">
  <div id="topnav">
                                    <div class="row-fluid">
                                        <div class="span9">
                                            <ul>

                                        <!-- admin -->
                                        <? if ($_SESSION["usertype"] == "admin") { ?>
                                            <li><a href="show_std.php">จัดการข้อมูลนักศึกษา</a></li>
                                            <li><a href="show_teacher.php">จัดการข้อมูลอาจารย์</a></li>
                                            <li><a href="show_company.php">จัดการข้อมูลหน่วยงาน</a></li>

                                            <!-- company -->
                                        <? } else if ($_SESSION["usertype"] == "company") { ?>
                                         <li><a href="company_detail.php">ข้อมูลหน่วยงาน</a></li>
                                            <li><a href="show_request_company.php">แจ้งความจำนงค์รับนักศึกษา</a></li>
                                            <li><a href="report_StdResume.php">รายงานประวัติส่วนตัวของนักศึกษา</a></li>
                                            <li><a href="manage_score_forcompany.php">กรอกข้อมูลประเมินผล</a></li>

                                        <? } else if ($_SESSION["usertype"] == "officer") { ?>

                                            <!-- officer -->
                                            <li><a href="#">จัดการข้อมูลหนังสือฝึกประสบการณ์วิชาชีพ</a>
                                                <ul>
                                                    <li><a href="manage_send_request.php">หนังสือขอความอนุเคราะห์ฝึกประสบการณ์วิชาชีพ</a></li>
                                                    <li><a href="manage_sendSupervision.php">หนังสือนิเทศนักศึกษา</a></li>
                                                    <li><a href="manage_sendStd.php">หนังสือส่งตัวนักศึกษา</a></li>
                                                    <li><a href="manage_sendStd1.php">หนังสือขอตัวนักศึกษา</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">จัดการข้อมูลหนังสือโครงการ</a>
                                                <ul>
                                                    <li><a href="manage_sendProject.php">โครงการปฐมนิเทศก่อนฝึกประสบการณ์</a></li>
                                                    <li><a href="manage_sendSamana.php">โครงการสัมนาหลังฝึกประสบการณ์</a></li>
                                                </ul>
                                            </li>

                                        <? } else if ($_SESSION["usertype"] == "student") { ?>
                                            <!-- student -->
                                            <li><a href="resume2.php">ประวัติส่วนตัว</a></li>
                                            <?php 
											 include "connect2.php";
										   $sql = "select * from register,student where student.std_id = register.std_id and student.std_id = '".$_SESSION['std_id']."'  ";
										   $rs = mysql_query($sql);
										   $row = mysql_fetch_array($rs);
										   $num_rows = mysql_num_rows($rs);
										   
										   $position = $row['position']; 
										   
											if($num_rows != 0){?>
												
                                            <li><a href="StdRequest2.php">ส่งคำร้องขอฝึกประสบการณ์</a></li>
                                            <li><a href="result_request.php">ผลอนุมัติการฝึกประสบการณ์</a></li>
                                            
                                            <?php  if ($position == 3){?>
                                            
                                            <li><a href="show_diary.php">บันทึกประจำวัน</a></li>
                                            
                                            <?php }
											} ?>

                                        <? } else if ($_SESSION["usertype"] == "teacher_general") { ?>
                                            <!-- teacher_general -->
                                            <li><a href="show_supervision_forTG.php">ตารางการนิเทศ</a></li>
                                            <li><a href="show_std_TG.php">ข้อมูลนักศึกษาฝึกประสบการณ์</a></li>
                                            <li><a href="show_company_TG.php">ข้อมูลแหล่งฝึกประสบการณ์</a></li>
                                            <li><a href="show_news_forteachergeneral.php">จัดการข่าวประชาสัมพันธ์</a></li>

                                        <? } else if ($_SESSION["usertype"] == "teacher") { ?>
                                            <!-- teacher -->
                                            <li><a href="#">จัดการข้อมูลทั่วไป</a>
                                                <ul>
                                                    <li><a href="show_std_forteacher.php">ข้อมูลนักศึกษา</a></li>
                                                    <li><a href="show_company_forteacher.php">ข้อมูลแหล่งฝึกประสบการณ์</a></li>
                                                    <li><a href="show_project.php">ข้อมูลโครงการ</a></li>
                                                    <li><a href="show_group.php">ข้อมูลหมู่เรียน</a></li>
                                                    <li><a href="register_new3.php">จัดการหมู่เรียนนักศึกษา</a></li>
                                                      <li><a href="manage_score.php">กรอกข้อมูลประเมินผล</a></li>
                                                          <li><a href="show_titlescore.php">ข้อมูลหัวข้อการประเมินผล</a></li>
                                                    <li><a href="show_news.php">ข้อมูลข่าวประชาสัมพันธ์</a></li>
                                                    <li><a href="show_webboard.php">ข้อมูลกระทู้</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">จัดการข้อมูลการนิเทศ</a>
                                                <ul>
                                                    <li><a href="show_supervision2.php">ตารางการนิเทศนักศึกษา</a></li>
                                                    <li><a href="show_advice.php">ปัญหาที่พบและข้อเสนอแนะจากการนิเทศนักศึกษา</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="confirmRQ.php">อนุมัติแหล่งฝึกงาน</a></li>
                                            <li><a href="company_comfirmRQ.php">เปลี่ยนสถานะตอบรับการฝึก</a></li>
                                            <li><a href="#">ดูรายงาน</a>
                                                <ul>
                                                 <li><a href="show_std_mange.php">ดูรายชื่อนักศึกษา</a></li>
                                                    <li><a href="report_std+company.php">รายชื่อแหล่งฝึกประสบการณ์
                                                            พร้อมทั้งชื่อนักศึกษา</a></li>
                                                             <li><a href="show_request.php">การแจ้งความจำนงขอรับนักศึกษาฝึกประสบการณ์วิชาชีพ
                                                            </a></li>
                                                    <li><a href="Report_Supervision.php">ตารางนิเทศนักศึกษา</a></li>
                                                    <li><a href="report_diary.php">บันทึกประจำวันของนักศึกษา</a></li>
                                                    <li><a href="report_advice.php">ปัญหาที่พบและ
                                                            ข้อเสนอแนะจากหน่วยงาน</a></li>
                                                    <li><a href="show_stdScore.php">ผลการประเมินจากหน่วยงาน</a></li>
                                                    <li><a href="Report_Score.php">คะแนนของนักศึกษา</a></li>
                                                    <li><a href="Report_ScoreSupervision.php">คะแนนการนิเทศ</a></li>
                                                    <li><a href="Report_ScoreDocument.php">คะแนนการดำเนินงานเอกสาร</a></li>
                                                    <li><a href="Report_ScoreActivity.php">คะแนนเข้าร่วมกิจกรรม</a></li>
                                                    <li><a href="Report_ScoreSendDoc.php">คะแนนการส่งเอกสาร</a></li>
                                                    <li><a href="report_grade.php">ผลการประเมิน</a></li>
                                                </ul>
                                            </li>

                                        <? } else { ?>
                                            <li><a href="index.php">หน้าหลัก</a></li>
                                            <li><a href="Webboard.php">เว็บบอร์ด</a></li>
                                            <?php if (!$_SESSION['username']) { ?>
                                                <li><a href="user_login.php">เข้าสู่ระบบ</a></li>
                                                <li><a href="submit3.php">สมัครสมาชิก</a></li>
                                            <?php } else { ?>
                                                <li><a href="user_logout.php">ออกจากระบบ</a></li>
                                                </li><?php } ?>

                                        <? } ?>

                                    </ul>
                                        </div>
                                        <div class="span3" style="height: 50px" align="right">
                                            <?php
                                            if($_SESSION['username']) {
                                            ?>
                                            <p style="margin-top: 16px;">ยินดีต้อนรับ คุณ  <?= $_SESSION['username'] ?> </p>
                                                <?php
                                            }
                                                ?>
                                        </div>
                                       
                                    </div>
    <div  class="clear"></div>
  </div>
</div>

<div class="wrapper row4">
  <div id="container" class="clear">
  
  <!-- InstanceBeginEditable name="center_body" -->
  

  <script type="text/javascript">
    $(document).ready(function(){
        tap1();
    });
    
    function tap1()
    {
        $.ajax({
            url : 'resume2.php',
            data : '',
            success : function(html){
                $('#show-list').html();
                $('#show-list').html(html);
            }
        });
    }
    
    function tap2()
    {
        if($('#building_name').val()=='')
        {
            $('#building_name').focus();
            return;
        }
        
        if($('#building_type_id').val()=='*')
        {
            $('#building_type_id').focus();
            return;
        }
        if($('#is_rent').val()=='*')
        {
            $('#is_rent').focus();
            return;
        }
        if($('#building_location_1').val()=='')
        {
            $('#building_location_1').focus();
            return;
        }

        if($('#building_rai').val()=='')
        {
            $('#building_rai').focus();
            return;
        }
        if($('#building_ngan').val()=='')
        {
            $('#building_ngan').focus();
            return;
        }
        if($('#building_qmeter').val()=='')
        {
            $('#building_qmeter').focus();
            return;
        }
        if($('#building_total_area').val()=='')
        {
            $('#building_total_area').focus();
            return;
        }
        if($('#building_bedroom').val()=='')
        {
            $('#building_bedroom').focus();
            return;
        }
        if($('#building_toilet').val()=='')
        {
            $('#building_toilet').focus();
            return;
        }
        if($('#building_studio').val()=='')
        {
            $('#building_studio').focus();
            return;
        }

        if($('#building_price').val()=='')
        {
            $('#building_price').focus();
            return;
        }
        
        $.ajax({
            url : 'StdRequest2.php',
            data : 'building_name='+$('#building_name').val()+'&building_type_id='+$('#building_type_id').val()
                +'&building_certificate='+$('#building_certificate').val()+'&building_location_1='+$('#building_location_1').val()+'&building_location_2='+$('#building_location_2').val()
                +'&building_rai='+$('#building_rai').val()+'&building_ngan='+$('#building_ngan').val()+'&building_qmeter='+$('#building_qmeter').val()
                +'&building_total_area='+$('#building_total_area').val()+'&building_bedroom='+$('#building_bedroom').val()
                +'&building_toilet='+$('#building_toilet').val()+'&building_studio='+$('#building_studio').val()
                +'&building_option='+$('#building_option').val()+'&is_rent='+$('#is_rent').val()+'&building_price='+$('#building_price').val(),
 
            success : function(html){
                $('#show-list').html();
                $('#show-list').html(html);
            }
        });
    }
    
    function tap3()
    {
        if($('#building_map1').val()=='')
        {
            $('#building_map1').focus();
            return;
        }
        if($('#building_map2').val()=='')
        {
            $('#building_map2').focus();
            return;
        }

        if($('#building_phone').val()=='')
        {
            $('#building_phone').focus();
            return;
        }
        if($('#building_email').val()=='')
        {
            $('#building_email').focus();
            return;
        }
        if($('#zone_id').val()=='')
        {
            $('#zone_id').focus();
            return;
        }
        
        $.ajax({
            url : '<?= $GLOBALS['server'] ?>ajax/building3.php',
            data : '&building_tel='+$('#building_tel').val()+'&building_map1='+$('#building_map1').val()
                +'&building_map2='+$('#building_map2').val()+'&building_fax='+$('#building_fax').val()
                +'&building_phone='+$('#building_phone').val()+'&building_email='+$('#building_email').val()
                +'&zone_id='+$('#zone_id').val(),
            success : function(html){
                $('#show-list').html();
                $('#show-list').html(html);
            }
        });
    }
    
</script>
<div style="width:100%;">
    <div class="map-and-list">
        <ul>
            <li class="active" tap="1"><a href="#map-and-list"  >ประวัติส่วนตัว</a></li>
            <li tap="2"><a href="#map-and-list" >ส่งคำร้องขอฝึกฯ</a></li>
            <li tap="3"><a href="#map-and-list" >ผลอนุมัติการฝึกฯ</a></li>
        </ul>
    </div>
    <div class="show-list" id="show-list" style="padding: 15px 0;">

    </div>
</div>

<?php if ($id) { ?>

    <script type="text/javascript">
        function del_photo(id)
        {
            if(confirm('ยินยันการลบ?'))
            {
                window.location.href='<?= $GLOBALS['server'] ?>info/building/<?= $id ?>/del/photos/'+id+'/#photo';      
            }
        }
    </script>

    <div style="width:100%; margin-top: 50px;" id="photo">
        <div class="map-and-list">
            <ul>
                <li class="active" tap="1"><a href="#map-and-list" >การแสดงรูปภาพ</a></li>

            </ul>
        </div>
        <div class="show-list" style="padding: 15px 0;">
            <?php for ($i = 0; $i < sizeof($_SESSION['tmp']['detail']['building_photo']); $i++) { ?>
                <?php if ($i % 3 == 0) { ?>
                    <div class="row-fluid">
                    <?php } ?>
                    <div class="span4" align="center">

                        <?php $img = getimagesize("media/photos/building/" . $_SESSION['tmp']['detail']['building_photo'][$i]['photo_file']); ?>

                        <img src="<?= $GLOBALS['server'] ?>media/photos/building/<?= $_SESSION['tmp']['detail']['building_photo'][$i]['photo_file'] ?>" <?php if ($img[0] > $img[1]) { ?> width="200px" <?php } else { ?> width="167px" <?php } ?>/>
                        <p>&nbsp;</p>
                        <p><a href="#" onclick="del_photo(<?= $_SESSION['tmp']['detail']['building_photo'][$i]['building_photo_id'] ?>)"><img src="<?= $GLOBALS['server'] ?>media/photos/form/del.gif"/></a></p>
                    </div>
                    <?php if ($i % 3 == 2) { ?>
                    </div>
                <?php } ?>

            <?php } ?>
            <?php
            if (sizeof($_SESSION['tmp']['detail']['building_photo']) % 3 != 0) {
                echo '</div>';
            }
            ?>
            <div align="center">
                <?php
                $form = new form('photo');
                $form->control('photos', 'input', array('type' => 'file'), new validate('null','test', $value));
                $form->control('submit_photo','input', array('type'=>'submit','value'=>'Upload'));

                $form->close();
                ?>
            </div>
        </div>
    </div>

<?php } ?>


<!-- InstanceEndEditable --></div>
</div>


<div class="wrapper">
  <div id="copyright" class="clear">
  <div style="width:960px;margin:0 auto; ">
    <p class="fl_left">Copyright (c) 2012 <a href = "http://webhosting.udru.ac.th/~std52040249439" target="_blank">http://www.gen-dev-soft.com/experience-csit/</a> All rights reserved. 
<br>
Design by Nittaya Kakulphin & Benjawan Sriralat @ Udonthani Rajabhat University.</p>
    </div>
  </div>
</div>
</body>
<!-- InstanceEnd --></html>