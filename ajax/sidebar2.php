<?php
require('ajaxHelper.php');

$helper = new ajaxHelper();

$building_type = $helper->select('building_type', ' WHERE root_id="0" ');

$zone = $helper->select('zone');

if (!$_GET['type'])
    $_GET['type'] = 'buy';
?>

<div class="sidebar2">
    <div class="sidebar2-top-menu">
        <ul>
            <li <?= $_GET['type'] == 'buy' ? 'class="active"' : '' ?>><a href="<?= $GLOBALS['server'] ?>info/<?= $_GET['mode'] ?>/?type=buy" >ซื้อ</a></li>
            <li <?= $_GET['type'] == 'rent' ? 'class="active"' : '' ?>><a href="<?= $GLOBALS['server'] ?>info/<?= $_GET['mode'] ?>/?type=rent">เช่า</a></li>
        </ul>    
    </div>
    <div class="sidebar2-type-title">
        <div class="row-fluid">
            <div class="span3" >
                <div class="sidebar2-type-icon">
                    <i class="icon-home icon-white"></i>
                </div>
            </div>
            <div class="span9">
                <div class="sidebar2-type-title2">
                    <p> ประเภทอสังหาริมทรัพย์ </p>
                </div>
            </div>

        </div>
    </div>
    <div class="sidebar2-type-list">
        <?php
        $chk == false;
        for ($i = 0; $i < sizeof($building_type); $i++) {

            if (!$chk) {
                for ($j = 0; $j < sizeof($building_type[$i]['detail']['root']); $j++) {
                    if ($building_type[$i]['detail']['root'][$j]['building_type_id'] == $building_type_id) {
                        $building_type_id = $building_type[$i]['detail']['root'][$j]['root_id'];
                        $chk = true;
                        break;
                    }
                }
            }
            ?>
            <div class="main-sidebar2">
                <div class="row-fluid" >
                    <div class="span2">
                        <div class="main-list-icon">

                            <i class="icon-arrow-<?= ($building_type_id == $building_type[$i]['building_type_id']) ? 'down' : 'right' ?>"></i>
                        </div>
                    </div>
                    <div class="span10">
                        <div class="main-list-name">
                            <a href="<?= $GLOBALS['server'] ?>info/<?= $_GET['mode'] ?>/?<?= $_GET['type'] ? 'type=' . $_GET['type'] . "&" : '' ?>building_type_id=<?= $building_type[$i]['building_type_id'] ?>" ><?= $building_type[$i]['building_type_name'] ?> </a>

                        </div>
                    </div>

                </div>

            </div>
            <?php
            if ($building_type_id == $building_type[$i]['building_type_id']) {

                for ($j = 0; $j < sizeof($building_type[$i]['detail']['root']); $j++) {
                    ?>
                    <div class="sub-sidebar2">
                        <div class="row-fluid" >
                            <div class="span2">
                                <div class="sub-list-icon">
                                    <i class="icon-arrow-right"></i>
                                </div>
                            </div>
                            <div class="span10">
                                <div class="sub-list-name">
                                    <a href="?<?= $_GET['type'] ? 'type=' . $_GET['type'] . "&" : '' ?>building_type_id=<?= $building_type[$i]['detail']['root'][$j]['building_type_id'] ?>" ><?= $building_type[$i]['detail']['root'][$j]['building_type_name'] ?> </a>

                                </div>
                            </div>

                        </div>

                    </div>
                    <?php
                }
            }
            ?>

            <div class="sidebar2-hr">
                <hr>
            </div>

        <?php } ?>
    </div>


    <div class="sidebar2-type-title">
        <div class="row-fluid">
            <div class="span3" >
                <div class="sidebar2-type-icon">
                    <i class="icon-home icon-white"></i>
                </div>
            </div>
            <div class="span9">
                <div class="sidebar2-type-title2">
                    <p> ทำเล </p>
                </div>
            </div>

        </div>
    </div>
    <?php for ($i = 0; $i < sizeof($zone); $i++) { ?>
        <div class="main-sidebar2">
            <div class="row-fluid" >
                <div class="span2">
                    <div class="main-list-icon">
                        <?php if($zone_id == $zone[$i]['zone_id']){ ?>
                        <i class="icon-arrow-right"></i>
                        <?php } ?>
                    </div>
                </div>
                <div class="span10">
                    <div class="main-list-name">
                        <a href="<?= $GLOBALS['server'] ?>info/<?= $_GET['mode'] ?>/?<?= $_GET['type'] ? 'type=' . $_GET['type'] . "&" : '' ?><?= $building_type_id ? 'building_type_id=' . $building_type_id . '&' : '' ?>&zone_id=<?= $zone[$i]['zone_id'] ?>" ><?= $zone[$i]['zone_name'] ?> </a>

                    </div>
                </div>

            </div>

        </div>
    <?php } ?>

    <div class="sidebar2-type-title">
        <div class="row-fluid">
            <div class="span3" >
                <div class="sidebar2-type-icon">
                    <i class="icon-home icon-white"></i>
                </div>
            </div>
            <div class="span9">
                <div class="sidebar2-type-title2">
                    <p> งบประมาณ </p>
                </div>
            </div>

        </div>
    </div>

    <?php for ($i = 1; $i < sizeof($GLOBALS[$type]); $i++) { ?>
        <div class="main-sidebar2">
            <div class="row-fluid" >
                <div class="span2">
                    <div class="main-list-icon">
                        <?php if ($range == $i) { ?>
                            <i class="icon-arrow-right"></i>
                        <?php } ?>
                    </div>
                </div>
                <div class="span10">
                    <div class="main-list-name">
                        <a href="<?= $GLOBALS['server'] ?>info/<?= $_GET['mode'] ?>/?<?= $_GET['type'] ? 'type=' . $_GET['type'] . "&" : '' ?><?= $building_type_id ? 'building_type_id=' . $building_type_id . '&' : '' ?>&<?= $zone_id ? 'zone_id=' . $zone_id . '&' : '' ?>&range=<?= $i ?>" ><?= $GLOBALS[$type][$i] ?> </a>

                    </div>
                </div>

            </div>

        </div>
    <?php } ?>

</div>