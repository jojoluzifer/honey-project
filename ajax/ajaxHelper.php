<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dbms
 *
 * @author Goonerm
 */
ob_start();
session_start();

require_once ('../helper/mysqlConnection.php');
require('../helper/form.php');
require('../helper/table.php');

$test = explode('/', $_SERVER[PHP_SELF]);

$path = "";

for ($i = 0; $i < sizeof($test) - 2; $i++) {
    $path.=$test[$i] . "/";
}

$server = "http://" . $_SERVER[SERVER_NAME] . $path;

/* ------------ select languages ---------- */
$languages = "thai";

if (file_exists("../languages/" . $languages . ".php")) {
    include("../languages/" . $languages . ".php");
} else {
    include("../languages/thai.php");
}

/* -----------end select languages ---------- */



class ajaxHelper extends mysqlConnection {

    
    public function __construct() {
        ob_start();
        session_start();

        include('../model/config.php');

        parent::__construct($config);
        parent::connect();
        
    }

    public function query($sql) {
        parent::query($sql);
        parent::fetch();
        $result = parent::getResult();
        parent::clear();

        return $result;
    }

    public function select($model, $condition = null) {
        require_once('../model/' . $model . "Model.php");
        $class = $model . "Model";
        $tmp = new $class;

        $select = "";

        $table = $tmp->table;

        $join = "";

        foreach ($tmp as $name => $val) {
            if ($name == 'mapping') {

                foreach ($val as $index => $vals) {
                    $tmp_table = "";

                    foreach ($vals as $name => $val) {
                        switch ($name) {
                            case 'table' :
                                $tmp_table = $val['name'];
                                $join.=" " . $val['join'] . " JOIN " . $val['name'] . " ON " . $table . "." . $val['name'] . "_id=" . $val['name'] . ".id";
                                break;
                            case 'select' : {
                                    foreach ($val as $index => $val) {
                                        $select.=$tmp_table . "." . $val . ",";
                                    }
                                }
                                break;
                        }
                    }
                }
            } else if ($name == 'detail') {
                $detail = $val;
            } else if ($name != 'table') {
                $select.= $name . ",";

                if ($name == 'root_id') {
                    $root = true;
                }
            }
        }


        $select = $table . ".id AS " . $table . "_id," . substr($select, 0, strlen($select) - 1);
        //echo "SELECT " . $select . " FROM " . $table . " " . $join. ($condition != '' ? $condition : " ")."<br/>";
        parent::query("SELECT " . $select . " FROM " . $table . " " . $join . " " . $condition);
        parent::fetch();


        $result = parent::getResult();
        parent::clear();

         for ($i = 0; $i < sizeof($result); $i++) {

            if ($detail) {
                for ($j = 0; $j < sizeof($detail); $j++) {
                    $detail_condition = " WHERE " . $detail[$j] . "." . $table . "_id='" . $result[$i][$table . '_id'] . "'";

                    $result_detail = $this->select($detail[$j], $detail_condition);
                    $result[$i]['detail'][$detail[$j]] = $result_detail;
                    
                }
            }

            if ($root) {
                $root_condition = " WHERE root_id='" . $result[$i][$table . '_id'] . "' ";
                $result_root = $this->select($table, $root_condition);
                $result[$i]['detail']['root'] = $result_root;
            }
        }

        return $result;
    }

    public function insert($table, $values) {
        $insert = " INSERT INTO " . $table;


        $data = "(";
        foreach ($values as $index => $value) {
            $key = " (";
            foreach ($value as $name => $val) {
                $key.=$name . ",";
                $data.="'" . $val . "',";
            }
            $data = substr($data, 0, strlen($data) - 1) . "),(";
        }

        $key = substr($key, 0, strlen($key) - 1) . ")";
        $data = substr($data, 0, strlen($data) - 2);

        //echo $insert . $key . " VALUES " . $data;

        parent::query($insert . $key . " VALUES " . $data);
    }

    public function update($table, $value, $condition = '') {
        $update = " UPDATE " . $table . " SET ";

        $data = "";

        foreach ($value as $name => $val) {
            $data.=$name . "='" . $val . "',";
        }

        $data = substr($data, 0, strlen($data) - 1);

        parent::query($update . $data.= $condition != '' ? $condition : "");
    }

    public function delete($table, $condition) {
        $delete = "DELETE FROM " . $table . $condition;
        parent::query($delete);
    }

}

?>
