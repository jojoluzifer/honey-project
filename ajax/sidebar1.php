<?php
require('ajaxHelper.php');

$helper = new ajaxHelper();
$sql = "SELECT review.id,comment,review_comment.user_id,photos
    ,comment_datetime,review_title,username,r_category_id
    FROM review,review_comment,profile,user
    WHERE review.id=review_comment.review_id AND review_comment.user_id=profile.user_id
    AND user.id=profile.user_id
    ORDER BY review_comment.comment_datetime DESC LIMIT 0,3  ";
$post = $helper->query($sql);


$building_type = $helper->select('building_type', ' WHERE root_id="0" ');

function showDate($date) {
    $month = array(
        '1' => 'มกราคม',
        '2' => 'กุมภาพันธ์',
        '3' => 'มีนาคม',
        '4' => 'เมษายน',
        '5' => 'พฤษภาคม',
        '6' => 'มิถุนายน',
        '7' => 'กรกฎาคม',
        '8' => 'สิงหาคม',
        '9' => 'กันยายน',
        '10' => 'ตุลาคม',
        '11' => 'พฤศจิกายน',
        '12' => 'ธันวาคม',
    );

    return (date('d', strtotime($date)) + 0) . " " . $month[(date('m', strtotime($date)) + 0)] . " " . ((date('Y', strtotime($date)) + 0) + 543) . " " . date('H:i น.', strtotime($date));
}
?>
<div id="sidebar">
    <div class="sidebar-banner">
        <div class="sidebar-banner-img"><img src="<?= $GLOBALS['server'] ?>media/photos/building/banner2.png" alt=""/></div>
    </div>
    <div class="sidebar-banner">
        <div class="sidebar-banner-img"><img src="<?= $GLOBALS['server'] ?>media/photos/building/banner3.png" alt=""/></div>
    </div>
    <div class="recent-post">

        <div class = "head-sidebar"><p>โพสต์ล่าสุด</p></div>
        <div class="recent-post-vertical-position">
            <div class = "recent-post-horizon-position">


                <div class="recent-post-list">
                    <?php for ($i = 0; $i < sizeof($post); $i++) { ?>
                        <div class = "recent-post-img">
                            <div class="recent-post-img-horizon-position">
                                <img src ="<?= $GLOBALS['server'] ?>/media/photos/<?= $post[$i]['photos'] ? $post[$i]['photos'] : 'default.png' ?>" alt = "" />
                            </div>
                        </div>
                        <div class="recent-post-content">
                            <div class="recent-post-content-horizon-position">
                                <div class="recent-post-name"><p><?= $post[$i]['username'] ?> comment on <a href="<?= $GLOBALS['server'] ?>info/<?= $post[$i]['r_category_id'] == 1 ? "newProjectInfo" : "reviewInfo" ?>/<?= $post[$i]['id'] ?>"><?= $post[$i]['review_title'] ?></a></p></div>
                                <div class="recent-post-date"><p><?= showDate($post[$i]['comment_datetime']) ?></p></div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="underline-on-recent-post"></div>
                    <?php } ?>




                </div>

            </div>

        </div>
    </div>
    <div class="social-link">
        <div class="head-sidebar"><p>Social Links</p></div>
        <div class="social-link-vertical-position">
            <div class="social-link-horizon-position">
                <div class="social-link-list">
                    <div class="fb-link"><a href="http://www.facebook.com/KhonkaenRealEstate" target="_blank"><img src="<?= $GLOBALS['server'] ?>media/photos/building/fb.png" alt="facebook"></img></a></div>
                    <div class="twitter-link"><a href="https://twitter.com/REstateKhonkaen" target="_blank"><img src="<?= $GLOBALS['server'] ?>media/photos/building/twitter.png" alt="twitter"></img></a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="text-link">
        <div class="head-sidebar"><p>ค้นหา</p></div>
        <div class="text-link-vertical-position">
            <div class="text-link-horizon-position">
                <div class="text-link-list">
                    <ul>
                        <?php for ($i = 0; $i < sizeof($building_type); $i++) { ?>
                            <li><a href="<?= $GLOBALS['server'] ?>info/buildingList/buy/<?= $building_type[$i]['building_type_id'] ?>"><?= $building_type[$i]['building_type_name'] ?></a></li>
                        <?php } ?>
                    </ul>
                </div>

            </div>

        </div>
    </div>
    <div class="text-link">
        <div class="head-sidebar"><p>รีวิวโครงการ</p></div>
        <div class="text-link-vertical-position">
            <div class="text-link-horizon-position">
                <div class="text-link-list">
                    <ul >
                        <li><a href="#">โครงการ X</a></li>
                        <li><a href="#">โครงการ Y</a></li>    
                        <li><a href="#">โครงการ Z</a></li>  
                    </ul>
                </div>

            </div>

        </div>
    </div>




</div>