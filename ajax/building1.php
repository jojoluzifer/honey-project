<?php


?>
<table width="80%" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td><?= $GLOBALS['building_nameTxt'] ?></th><td>:</td>
        <td><input type="text" id="building_name" value="<?=$_SESSION['tmp']['building_name']?>" style="width:150px;"/></td>
    </tr>
    <tr>
        <td><?= $GLOBALS['building_type_nameTxt'] ?></th><td>:</td>
        <td>
            <select id="building_type_id">
                <option value="*"><?=$selectTxt?></option>
                <?php for ($i = 0; $i < sizeof($type); $i++) { ?>
                    <optgroup label="<?=$type[$i]['building_type_name']?>">
                        <?php for($j=0 ; $j<sizeof($type[$i]['detail']['root']) ; $j++){?>
                        <option <?= $_SESSION['tmp']['building_type_id']==$type[$i]['detail']['root'][$j]['building_type_id'] ? 'selected="selected"' : "" ?> value="<?=$type[$i]['detail']['root'][$j]['building_type_id']?>"><?=$type[$i]['detail']['root'][$j]['building_type_name']?></option>
                        <?php } ?>
                    </optgroup>
                <?php } ?>
            </select>
        </td>
    </tr>
    <tr>
        <td><?= $GLOBALS['building_certificateTxt'] ?></th><td>:</td>
        <td><input type="text" id="building_certificate" value="<?=$_SESSION['tmp']['building_certificate']?>" style="width:150px;"/></td>
    </tr>
    <tr>
    <tr>
        <td><?= $GLOBALS['is_rentTxt'] ?></th><td>:</td>
        <td>
           <select id="is_rent">
                <option value="*"><?=$GLOBALS['selectTxt']?></option>
                <option <?= $_SESSION['tmp']['is_rent']==0 ? 'selected' : '' ?> value="0"><?=$GLOBALS['sellTxt']?></option>
                <option <?= $_SESSION['tmp']['is_rent']==1 ? 'selected' : '' ?> value="1"><?=$GLOBALS['leaseTxt']?></option>
           </select>
        </td>
    </tr>
    <tr>
        <td><?= $GLOBALS['building_location_1Txt'] ?></th><td>:</td>
        <td><input type="text" id="building_location_1" value="<?=$_SESSION['tmp']['building_location_1']?>"/></td>
    </tr>
    <tr>
        <td></th><td></td>
        <td><input type="text" id="building_location_2" value="<?=$_SESSION['tmp']['building_location_2']?>" /></td>
    </tr>
    <tr>
        <td><?= $GLOBALS['building_landTxt'] ?></td><td>:</td>
        <td>
            <input type="text" value="<?=$_SESSION['tmp']['building_rai']?>" id="building_rai" style="width:30px;"/> <?= $GLOBALS['building_raiTxt'] ?>
            <input type="text" value="<?=$_SESSION['tmp']['building_ngan']?>" id="building_ngan" style="width:30px;"/> <?= $GLOBALS['building_nganTxt'] ?>
            <input type="text" value="<?=$_SESSION['tmp']['building_qmeter']?>" id="building_qmeter" style="width:30px;"/> <?= $GLOBALS['building_qmeterTxt'] ?>
        </td>
    </tr>
    <tr>
        <td><?= $GLOBALS['building_total_areaTxt'] ?></td><td>:</td>
        <td><input type="text" id="building_total_area" value="<?=$_SESSION['tmp']['building_total_area']?>" style="width:50px;"/> <?= $GLOBALS['building_qmeterTxt']?></td>
    </tr>
    <tr>
        <td><?= $GLOBALS['building_roomTxt'] ?></td><td>:</td>
        <td>
            <input type="text" value="<?=$_SESSION['tmp']['building_bedroom']?>" id="building_bedroom" style="width:30px;"/> <?= $GLOBALS['building_bedroomTxt'] ?>
            <input type="text" value="<?=$_SESSION['tmp']['building_toilet']?>" id="building_toilet" style="width:30px;"/> <?= $GLOBALS['building_toiletTxt'] ?>
            <input type="text" value="<?=$_SESSION['tmp']['building_studio']?>" id="building_studio" style="width:30px;"/> <?= $GLOBALS['building_studioTxt'] ?>
        </td>
    </tr>
    <tr>
        <td><?= $GLOBALS['building_optionTxt'] ?></th><td>:</td>
        <td><input type="text" id="building_option"  size="30" value="<?=$_SESSION['tmp']['building_option']?>" /></td>
    </tr>
    <tr>
        <td><?= $GLOBALS['building_priceTxt'] ?></th><td>:</td>
        <td><input type="text" id="building_price"  size="30" value="<?=$_SESSION['tmp']['building_price']?>" /></td>
    </tr>
</table>
<div align="center">
    <input type="button" value="<?= $GLOBALS['nextTxt'] ?>" onclick="tap2()" class="btn"/>
</div>