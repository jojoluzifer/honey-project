<?php
require('ajaxHelper.php');

foreach ($_GET as $name => $val) {
    if ($val != 'undefined')
        $_SESSION['tmp'][$name] = $val;
}

$ajax = new ajaxHelper();
$building_zone = $ajax->select('zone');

$pos = explode(',', $_SESSION['tmp']['building_map']);
?>


<div class="clear"></div>
<div class="height20"></div>
<style type="text/css">

    #map{
        width: 480px;
        height: 360px;
        border: 1px solid #000000;
        overflow: hidden; 
    }
</style>     

<table width="80%" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td colspan="3" align="center">

            <div id="map">

            </div>



            <script type="text/javascript">
                        
                var building_id = 0;
                        
                var posX = <?= $pos[0] ? $pos[0] : 0 ?>;
                var posY = <?= $pos[1] ? $pos[1] : 0 ?>;
                
                $(document).ready(function(){
                    loadmap(4);
                });
                        
                function loadmap(zone_id) {
        
                    if(zone_id=='-' && (zone_id<=0 && zone_id>=4))
                        return;
                            
                    var zone = [
                        [16.470411,102.8092,14],
                        [16.423983,102.801304,14],
                        [16.460945,102.855806,14],
                        [16.412045,102.844305,14],
                        [16.439779,102.829263,14]
                    ];
        
        
                    var mapOptions = {
                        center: new google.maps.LatLng(zone[zone_id][0],zone[zone_id][1]),
                        zoom: zone[zone_id][2],
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
        
                    var map = new google.maps.Map(document.getElementById("map"),
                    mapOptions);
                            
                    var img = 'map_pin.png';
                            
                            
                    if(posX!=posY && posX!=0)
                        var myLatLng = new google.maps.LatLng(posX,posY);
                    else 
                        var myLatLng = new google.maps.LatLng(zone[zone_id][0],zone[zone_id][1]);
                           
                            
                            
                    var marker = new google.maps.Marker({
                        position : myLatLng,
                        map:map,
                        title : 'Marker',
                        icon : '<?= $GLOBALS['server'] ?>media/photos/map/'+img,
                        draggable : true
                    });
                            
                           
                    google.maps.event.addListener(marker, 'dragend', function(){
                        var pos = marker.getPosition();
                        pos = String(pos);
                        pos = pos.replace('(', '');
                        pos = pos.replace(')', '');
                        pos = pos.replace(' ', '');
                        pos = pos.split(',');
                        $('#building_map1').val(pos[0]);
                        $('#building_map2').val(pos[1]);
                    });
                           

                }
            </script>

        </td>
    </tr>
    <tr valign="top">
        <td><?= $GLOBALS['locationTxt'] ?></td><td>:</td>
        <td>
            <input type="text" size="20" name="building_map1" id="building_map1" readonly="true" value="<?= $pos[0] ?>"/><br/>
            <input type="text" size="20" name="building_map2" id="building_map2" readonly="true"  value="<?= $pos[1] ?>"/>
        </td>
    </tr>
    <tr>
        <td><?= $GLOBALS['zone_nameTxt'] ?></th><td>:</td>
        <td><select name="zone_id" id="zone_id" onchange="loadmap($('#zone_id').val()-1)">
                <option value="-"><?= $GLOBALS['selectTxt'] ?></option>
                <?
                for ($k = 0; $k < sizeof($building_zone); $k++) {
                    ?>	
                    <option value="<?= $building_zone[$k]['zone_id'] ?>" <?= $_SESSION['tmp']['zone_id'] == $building_zone[$k]['zone_id'] ? "selected=\"selected\"" : ""; ?>><?= $building_zone[$k]['zone_name'] ?></option>
                <? } ?>
            </select>
        </td>
    </tr>
    <tr>
        <td><?= $GLOBALS['building_telTxt'] ?></th><td>:</td>
        <td><input type="text" id="building_tel"  size="30" value="<?= $_SESSION['tmp']['building_tel'] ?>" /></td>
    </tr>
    <tr>
        <td><?= $GLOBALS['building_faxTxt'] ?></th><td>:</td>
        <td><input type="text" id="building_fax"  size="30" value="<?= $_SESSION['tmp']['building_fax'] ?>" /></td>
    </tr>
    <tr>
        <td><?= $GLOBALS['building_phoneTxt'] ?></th><td>:</td>
        <td><input type="text" id="building_phone"  size="30" value="<?= $_SESSION['tmp']['building_phone'] ?>" /></td>
    </tr>
    <tr>
        <td><?= $GLOBALS['building_emailTxt'] ?></th><td>:</td>
        <td><input type="text" id="building_email"  size="30" value="<?= $_SESSION['tmp']['building_email'] ?>" /></td>
    </tr>
</table>
<div align="center">
    <input type="button" value="<?= $GLOBALS['backTxt'] ?>" onclick="tap1()" class="btn"/>
    <input type="button" value="<?= $GLOBALS['nextTxt'] ?>" onclick="tap3()" class="btn"/>
</div>