<?
ob_start();
	session_start();
$type_user = $_SESSION['usertype'];

if ($type_user != "admin")
	{
		
		header("Location:user_login.php");
	
		
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="EN" lang="EN" dir="ltr"><!-- InstanceBegin template="/Templates/index_full.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head profile="http://gmpg.orgs/xfn/11">
<title>ระบบสารสนเทศฝึกประสบการณ์วิชาชีพฯ :<?php echo $_SESSION['usertype']; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="imagetoolbar" content="no" />
<link rel="stylesheet" href="css/layout.css" type="text/css" />
<link rel="stylesheet" href="css/layout.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/style-mix.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="style.css" />
   
        
        <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="jquery-1.7.2.min.js"></script>
     

</head>
<body id="top">
<div class="wrapper row1">
  <div id="header" class="clear">
    <div class="fl_left">
    <p><img src="img/logo5.png" width="80" height="100"></p>
    </div>
      <div class="fl_center">
      <div class="fl_right" align="right">
      <ul>
        <li><a href="index.php">หนัาหลัก</a></li>
        <li><a href="Webboard.php">เว็บบอร์ด</a></li>
         <?php if(!$_SESSION['username']){?>
        <li><a href="user_login.php">เข้าสู่ระบบ</a></li>
        <li><a href="submit3.php">สมัครสมาชิก</a></li>
        <?php } else { ?>
        <li class="last"><a href="user_logout.php">ออกจากระบบ</a></li>
        <?php } ?>
      </ul>
    </div>
      <br>
      <br>
      <h1>&nbsp;&nbsp;ระบบสารสนเทศฝึกประสบการณ์วิชาชีพ</h1>
      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;สาขาวิทยาการคอมพิวเตอร์และเทคโนโลยีสารสนเทศ</p>
    </div>
 
  </div>
</div>

<div class="wrapper row2">
  <div id="topnav">
                                    <div class="row-fluid">
                                        <div class="span9">
                                            <ul>

                                        <!-- admin -->
                                        <? if ($_SESSION["usertype"] == "admin") { ?>
                                            <li><a href="show_std.php">จัดการข้อมูลนักศึกษา</a></li>
                                            <li><a href="show_teacher.php">จัดการข้อมูลอาจารย์</a></li>
                                            <li><a href="show_company.php">จัดการข้อมูลหน่วยงาน</a></li>

                                            <!-- company -->
                                        <? } else if ($_SESSION["usertype"] == "company") { ?>
                                         <li><a href="company_detail.php">ข้อมูลหน่วยงาน</a></li>
                                            <li><a href="show_request_company.php">แจ้งความจำนงค์รับนักศึกษา</a></li>
                                            <li><a href="report_StdResume.php">รายงานประวัติส่วนตัวของนักศึกษา</a></li>
                                            <li><a href="manage_score_forcompany.php">กรอกข้อมูลประเมินผล</a></li>

                                        <? } else if ($_SESSION["usertype"] == "officer") { ?>

                                            <!-- officer -->
                                            <li><a href="#">จัดการข้อมูลหนังสือฝึกประสบการณ์วิชาชีพ</a>
                                                <ul>
                                                    <li><a href="manage_send_request.php">หนังสือขอความอนุเคราะห์ฝึกประสบการณ์วิชาชีพ</a></li>
                                                    <li><a href="manage_sendSupervision.php">หนังสือนิเทศนักศึกษา</a></li>
                                                    <li><a href="manage_sendStd.php">หนังสือส่งตัวนักศึกษา</a></li>
                                                    <li><a href="manage_sendStd1.php">หนังสือขอตัวนักศึกษา</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">จัดการข้อมูลหนังสือโครงการ</a>
                                                <ul>
                                                    <li><a href="manage_sendProject.php">โครงการปฐมนิเทศก่อนฝึกประสบการณ์</a></li>
                                                    <li><a href="manage_sendSamana.php">โครงการสัมนาหลังฝึกประสบการณ์</a></li>
                                                </ul>
                                            </li>

                                        <? } else if ($_SESSION["usertype"] == "student") { ?>
                                            <!-- student -->
                                            <li><a href="resume2.php">ประวัติส่วนตัว</a></li>
                                            <?php 
											 include "connect2.php";
										   $sql = "select * from register,student where student.std_id = register.std_id and student.std_id = '".$_SESSION['std_id']."'  ";
										   $rs = mysql_query($sql);
										   $row = mysql_fetch_array($rs);
										   $num_rows = mysql_num_rows($rs);
										   
										   $position = $row['position']; 
										   
											if($num_rows != 0){?>
												
                                            <li><a href="StdRequest2.php">ส่งคำร้องขอฝึกประสบการณ์</a></li>
                                            <li><a href="result_request.php">ผลอนุมัติการฝึกประสบการณ์</a></li>
                                            
                                            <?php  if ($position == 3){?>
                                            
                                            <li><a href="show_diary.php">บันทึกประจำวัน</a></li>
                                            
                                            <?php }
											} ?>

                                        <? } else if ($_SESSION["usertype"] == "teacher_general") { ?>
                                            <!-- teacher_general -->
                                            <li><a href="show_supervision_forTG.php">ตารางการนิเทศ</a></li>
                                            <li><a href="show_std_TG.php">ข้อมูลนักศึกษาฝึกประสบการณ์</a></li>
                                            <li><a href="show_company_TG.php">ข้อมูลแหล่งฝึกประสบการณ์</a></li>
                                            <li><a href="show_news_forteachergeneral.php">จัดการข่าวประชาสัมพันธ์</a></li>

                                        <? } else if ($_SESSION["usertype"] == "teacher") { ?>
                                            <!-- teacher -->
                                            <li><a href="#">จัดการข้อมูลทั่วไป</a>
                                                <ul>
                                                    <li><a href="show_std_forteacher.php">ข้อมูลนักศึกษา</a></li>
                                                    <li><a href="show_company_forteacher.php">ข้อมูลแหล่งฝึกประสบการณ์</a></li>
                                                    <li><a href="show_project.php">ข้อมูลโครงการ</a></li>
                                                    <li><a href="show_group.php">ข้อมูลหมู่เรียน</a></li>
                                                    <li><a href="register_new3.php">จัดการหมู่เรียนนักศึกษา</a></li>
                                                      <li><a href="manage_score.php">กรอกข้อมูลประเมินผล</a></li>
                                                          <li><a href="show_titlescore.php">ข้อมูลหัวข้อการประเมินผล</a></li>
                                                    <li><a href="show_news.php">ข้อมูลข่าวประชาสัมพันธ์</a></li>
                                                    <li><a href="show_webboard.php">ข้อมูลกระทู้</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">จัดการข้อมูลการนิเทศ</a>
                                                <ul>
                                                    <li><a href="show_supervision2.php">ตารางการนิเทศนักศึกษา</a></li>
                                                    <li><a href="show_advice.php">ปัญหาที่พบและข้อเสนอแนะจากการนิเทศนักศึกษา</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="confirmRQ.php">อนุมัติแหล่งฝึกงาน</a></li>
                                            <li><a href="company_comfirmRQ.php">เปลี่ยนสถานะตอบรับการฝึก</a></li>
                                            <li><a href="#">ดูรายงาน</a>
                                                <ul>
                                                 <li><a href="show_std_mange.php">ดูรายชื่อนักศึกษา</a></li>
                                                    <li><a href="report_std+company.php">รายชื่อแหล่งฝึกประสบการณ์
                                                            พร้อมทั้งชื่อนักศึกษา</a></li>
                                                             <li><a href="show_request.php">การแจ้งความจำนงขอรับนักศึกษาฝึกประสบการณ์วิชาชีพ
                                                            </a></li>
                                                    <li><a href="Report_Supervision.php">ตารางนิเทศนักศึกษา</a></li>
                                                    <li><a href="report_diary.php">บันทึกประจำวันของนักศึกษา</a></li>
                                                    <li><a href="report_advice.php">ปัญหาที่พบและ
                                                            ข้อเสนอแนะจากหน่วยงาน</a></li>
                                                    <li><a href="show_stdScore.php">ผลการประเมินจากหน่วยงาน</a></li>
                                                    <li><a href="Report_Score.php">คะแนนของนักศึกษา</a></li>
                                                    <li><a href="Report_ScoreSupervision.php">คะแนนการนิเทศ</a></li>
                                                    <li><a href="Report_ScoreDocument.php">คะแนนการดำเนินงานเอกสาร</a></li>
                                                    <li><a href="Report_ScoreActivity.php">คะแนนเข้าร่วมกิจกรรม</a></li>
                                                    <li><a href="Report_ScoreSendDoc.php">คะแนนการส่งเอกสาร</a></li>
                                                    <li><a href="report_grade.php">ผลการประเมิน</a></li>
                                                </ul>
                                            </li>

                                        <? } else { ?>
                                            <li><a href="index.php">หน้าหลัก</a></li>
                                            <li><a href="Webboard.php">เว็บบอร์ด</a></li>
                                            <?php if (!$_SESSION['username']) { ?>
                                                <li><a href="user_login.php">เข้าสู่ระบบ</a></li>
                                                <li><a href="submit3.php">สมัครสมาชิก</a></li>
                                            <?php } else { ?>
                                                <li><a href="user_logout.php">ออกจากระบบ</a></li>
                                                </li><?php } ?>

                                        <? } ?>

                                    </ul>
                                        </div>
                                        <div class="span3" style="height: 50px" align="right">
                                            <?php
                                            if($_SESSION['username']) {
                                            ?>
                                            <p style="margin-top: 16px;">ยินดีต้อนรับ คุณ  <?= $_SESSION['username'] ?> </p>
                                                <?php
                                            }
                                                ?>
                                        </div>
                                       
                                    </div>
    <div  class="clear"></div>
  </div>
</div>

<div class="wrapper row4">
  <div id="container" class="clear">
  
  <!-- InstanceBeginEditable name="center_body" -->
<form id="form1" name="form1" method="post" action="">

<?php
include("connect2.php");

$sql = "SELECT * FROM student,register WHERE student.std_id = register.std_id ";


if($_POST['Search'])
{

if ($_POST['id']) {

        $id = $_POST['id'];

        $sql .= " and student.std_id like '%$id%' ";
    }
	 if ($_POST['perfix']) {

        $perfix = $_POST['perfix'];
       

        $sql .= " and  perfix like '%$perfix%'";
    }
    if ($_POST['name']) {

        $name = $_POST['name'];

        $sql .= " and std_name like '%$name%'";
    }
    if ($_POST['major']) {

        $major = $_POST['major'];

        $sql .= " and major like '%$major%'";
    }
	 if ($_POST['group']) {

        $group = $_POST['group'];

        $sql .= " and register.groups_num like '%$group%'";
    }
	 if ($_POST['group']) {

        $group = $_POST['group'];

        $sql .= " and register.groups_num like '%$group%'";
    }
	
}

$sql .=" ORDER BY major DESC";
$rs_show = mysql_query($sql) or die(mysql_error());

?>
<div align="center"><strong>จัดการข้อมูลนักศึกษา</strong></div>
<br>
<br>
<a href="insert_std.php" class="head_submit">เพิ่มข้อมูล</a>
<br>
<br>
   <form id="form1" name="form2" method="post" action="">
       
       <div class="form-search">
           <div class="row-fluid">
               <div class="span3">
                   <div class="row-fluid">
                       <div class="span5">รหัสนักศึกษา :</div>
                       <div class="span7">
                           <input type="text" name="id" />
                       </div>
                   </div>
               </div>
                 <div class="span3">
                    <div class="row-fluid">
                       <div class="span5">คำนำหน้า :</div>
                       <div class="span7">
                           <input type="text" name="perfix" />
                       </div>
                   </div>
               </div>
               <div class="span3">
                    <div class="row-fluid">
                       <div class="span5">ชื่อนักศึกษา :</div>
                       <div class="span7">
                           <input type="text" name="name" />
                       </div>
                   </div>
               </div>
             
               <div class="span1">
               <input name="Search" type="submit" value="ค้นหา" />
               </div>
           </div>
           
           <div class="row-fluid">
             <div class="span3">
                    <div class="row-fluid">
                       <div class="span5">สาขา :</div>
                       <div class="span7">
                           <input type="text" name="major" />
                       </div>
                          
                    </div>
               </div>
  			<div class="span3">
                   <div class="row-fluid">
                       <div class="span5">หมู่เรียน :</div>
                       <div class="span7">
                           <input type="text" name="group" />
                       </div>
                   </div>
               </div>
           </div>
           
       </div>
         </form>
      <br>
      
<table width="662" border="1" cellpadding="0" cellspacing="0" bordercolor="#66CCCC" class="bodyresome" >
	<tr align="center">
		<th width="144" bgcolor="#66CCCC">รหัสนักศึกษา</th>
		<th width="189" bgcolor="#66CCCC">ชื่อ -  สกุล</th>
		<th width="168" bgcolor="#66CCCC">สาขา</th>
        <th width="96" bgcolor="#66CCCC">resume</th>
        <th width="96" bgcolor="#66CCCC">picture</th>
		<th width="60" bgcolor="#66CCCC">แก้ไข</th>
        <th width="66" bgcolor="#66CCCC">ลบ</th>

	</tr>
	<?php
	while($row = mysql_fetch_array($rs_show)) {
		echo "<tr>" ;
		echo "<td align='left'>" . $row['std_id'] . "</td>";
		echo "<td align='left'>" . $row['perfix'] . $row['std_name'] . "</td>" ;
		echo "<td align='left'>" . $row['major'] . "</td>" ;
		?>
		<td align="center"><a href="resume/<?=$row[resume]?>">Download</a></td>
		<td align="center"><a href="picture/<?=$row[picture]?>">Download</a></td>
        <?php
		echo "<td align='center'><a href = 'edit_std.php?std_id=$row[std_id]'><img src = 'img/Edit1.jpg' width='30' height='30'></a>";
		echo "<td align='center'><a href='del_std.php?std_id=$row[std_id]'  OnClick='return chkdel();'><img src = 'img/delete1.jpg' width='30' height='30'></a></td>";
		echo "</tr>";
	}
	?>
<script language="JavaScript">
function chkdel()
	{if(confirm('  กรุณายืนยันการลบอีกครั้ง !!!  ')){
	return true;
	}
	
	else
	{
		return false;
	}
	}
</script>
	
</table>
<br>
<br>
 <div align="left"><p><a href="show_std.php">ย้อนกลับ</a></p></div>
</form>
<!-- InstanceEndEditable --></div>
</div>


<div class="wrapper">
  <div id="copyright" class="clear">
  <div style="width:960px;margin:0 auto; ">
    <p class="fl_left">Copyright (c) 2012 <a href = "http://webhosting.udru.ac.th/~std52040249439" target="_blank">http://www.gen-dev-soft.com/experience-csit/</a> All rights reserved. 
<br>
Design by Nittaya Kakulphin & Benjawan Sriralat @ Udonthani Rajabhat University.</p>
    </div>
  </div>
</div>
</body>
<!-- InstanceEnd --></html>




