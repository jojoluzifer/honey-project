-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- โฮสต์: localhost
-- เวลาในการสร้าง: 
-- รุ่นของเซิร์ฟเวอร์: 5.0.51
-- รุ่นของ PHP: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- ฐานข้อมูล: `experience`
-- 

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `company`
-- 

CREATE TABLE `company` (
  `company_id` int(5) unsigned zerofill NOT NULL auto_increment,
  `company_name` varchar(50) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `boos` varchar(30) NOT NULL,
  `email` varchar(50) default NULL,
  `contact_name` varchar(50) NOT NULL,
  `location` varchar(50) NOT NULL,
  `approval` varchar(50) default '0',
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `peradd` varchar(30) NOT NULL default 'company',
  PRIMARY KEY  (`company_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1259 ;

-- 
-- dump ตาราง `company`
-- 

INSERT INTO `company` VALUES (00001, 'ThaiCopperation', '0904564321', 'pop', 'pop@hotmail.com', 'pop', 'thailand', '0', 'pop', 'pop', 'company');
INSERT INTO `company` VALUES (00002, 'FristChoei', '0904564321', 'ft', 'apple@gmail.com', 'ft', 'tt', '0', '33', '33', 'company');
INSERT INTO `company` VALUES (00003, 'EateSofteware', '0850845666', 'ee', 'pukky_do@hotmail.com', 'ee', 'thailand', '1', '55', '55', 'company');
INSERT INTO `company` VALUES (00004, 'LopeWhile', '0850845666', 'lop', 'numnim@gmail.com', 'lop', 'thai', '0', 'lop', 'lop', 'company');
INSERT INTO `company` VALUES (00005, 'TeeneeThai', '0909987677', 'tup', 'master_bank@hotmail.com', 'tup', 'thai', '1', '56', '56', 'company');
INSERT INTO `company` VALUES (00006, 'toyota', '0984455332', 'toyatoi', 'toyata@gmail.com', 'toyatoi', 'thailand', '0', 'toyota', '1234', 'company');
INSERT INTO `company` VALUES (00007, 'er', '0850845666', 'er', 'numnim@gmail.com', 'er', 'th', '0', 'toyata', '123', 'company');
INSERT INTO `company` VALUES (00008, 'Apple', '0904564321', 'Apple', 'pukky_do@hotmail.com', 'Apple', 'yj', '0', 'Apple', 'apple', 'company');
INSERT INTO `company` VALUES (01253, 'บริษัทยูไนเต็ด', '0807411833', 'yy', 'myberry-@hotmail.co.th', 'yy', 'yy', '', 'yy', 'yy', 'company');
INSERT INTO `company` VALUES (01254, 'rr', '0850487069', 'rr', 'rungtiwa.cs@gmail.com', 'rr', 'rr', '', 'rr', 'rr', 'company');
INSERT INTO `company` VALUES (01255, 'lollypop', '0807411833', 'ks', 'jam@hotmail.com', 'ks', 'thailand', '0', '', '', 'company');
INSERT INTO `company` VALUES (01256, 'uu', '0807511838', 'uu', 'uu@hotmail.com', 'uu', 'thai', '0', 'hatyai', '7777', 'company');
INSERT INTO `company` VALUES (01257, 'jojo', '0807511839', 'jojo', 'cutefulllife__honey@windowslive.com', 'jojo', 'thai', '0', '', '', 'student');
INSERT INTO `company` VALUES (01258, 'telcom', '0894455435', 'jojo', 'telcom@hotmail.com', 'jojo', 'thai', '0', '', '', 'student');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `details`
-- 

CREATE TABLE `details` (
  `details_id` int(10) unsigned NOT NULL auto_increment,
  `agencies` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `during` date NOT NULL,
  `update` date NOT NULL,
  `user_id` int(10) default NULL,
  PRIMARY KEY  (`details_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- 
-- dump ตาราง `details`
-- 

INSERT INTO `details` VALUES (1, 'อีสาน', 'หนองคาย', '0807544383', 'เขียนโปรแกรม', '2012-06-04', '2012-06-30', 2);
INSERT INTO `details` VALUES (2, 'พีแท็กเทคโนโลยี', 'ขอนแก่น', '0807511484', 'พัฒนาโปรแกรม', '2012-05-01', '2012-05-07', 3);

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `diary`
-- 

CREATE TABLE `diary` (
  `diary_date` date NOT NULL,
  `work_receive` varchar(50) NOT NULL,
  `practical` text NOT NULL,
  `problem` text NOT NULL,
  `register_id` int(5) unsigned zerofill NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- dump ตาราง `diary`
-- 

INSERT INTO `diary` VALUES ('2012-09-19', 'wewe', 'wewe', 'wewe', 00146);
INSERT INTO `diary` VALUES ('2012-09-02', 'แก้ไขโปรเจค php', 'tyty', 'tyty', 00146);
INSERT INTO `diary` VALUES ('2012-09-19', 'rtrt', 'rtrt', 'rtrt', 00147);
INSERT INTO `diary` VALUES ('2012-09-23', 'ee', 'ee55', 'ee', 00173);
INSERT INTO `diary` VALUES ('2012-09-28', 'cvcv', 'cvcv', 'cvcv', 00153);
INSERT INTO `diary` VALUES ('2012-09-14', 'dfdf', 'dfdf', 'dfdf', 00155);
INSERT INTO `diary` VALUES ('2012-09-15', 'bnbn', 'bnbn', 'bnbn', 00155);
INSERT INTO `diary` VALUES ('2012-09-22', 'ghgh', 'ghgh', 'ghgh', 00143);
INSERT INTO `diary` VALUES ('2012-09-14', 'klkl', 'klkl', 'klkl', 00147);
INSERT INTO `diary` VALUES ('2012-09-27', 'zxzx', 'zxzx', 'zxzx', 00153);
INSERT INTO `diary` VALUES ('2012-09-01', 'opop', 'opop', 'opop', 00157);
INSERT INTO `diary` VALUES ('2012-09-20', 'เขียนโปรแกรม PHP', 'เขียนโปรแกรม PHP', 'เขียนยังไม่คล่อง', 00000);
INSERT INTO `diary` VALUES ('2012-09-20', 'dd', 'dd', 'dd', 00173);
INSERT INTO `diary` VALUES ('2012-09-20', 'dd555', 'dd555', 'dd555', 00173);
INSERT INTO `diary` VALUES ('2012-10-03', 'เขียนโปรแกรม PHP', 'เขียนโปรแกรม PHP', 'เขียนยังไม่คล่อง', 00173);
INSERT INTO `diary` VALUES ('2012-09-20', 'pp', 'pp', 'pp', 00198);
INSERT INTO `diary` VALUES ('2012-09-20', '44', '44', '44', 00000);

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `groups`
-- 

CREATE TABLE `groups` (
  `groups_num` int(5) unsigned zerofill NOT NULL auto_increment,
  `groups` varchar(10) NOT NULL,
  `sub_id` varchar(10) NOT NULL,
  `sub_name` varchar(50) NOT NULL,
  `major` varchar(50) NOT NULL,
  `term` varchar(10) NOT NULL,
  `year` varchar(10) NOT NULL,
  PRIMARY KEY  (`groups_num`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

-- 
-- dump ตาราง `groups`
-- 

INSERT INTO `groups` VALUES (00001, 'E001', 'CS001', 'ฝึกประสบการณ์', 'วิทยาการคอมพิวเตอร์', '1', '2555');
INSERT INTO `groups` VALUES (00002, 'E002', 'IT001', 'ฝึกประสบการณ์', 'เทคโนโลยีสารสนเทศ', '2', '2555');
INSERT INTO `groups` VALUES (00003, 'E003', 'CS002', 'ฝึกประสบการณ์', 'วิทยาการคอมพิวเตอร์', '2', '2555');
INSERT INTO `groups` VALUES (00004, 'E001', 'IT004', 'ฝึกประสบการณ์', 'เทคโนโลยีสารสนเทศ', '1', '2554');
INSERT INTO `groups` VALUES (00005, 'E001', 'IT004', 'ฝึกประสบการณ์', 'เทคโนโลยีสารสนเทศ', '1', '2554');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `news`
-- 

CREATE TABLE `news` (
  `news_id` int(5) unsigned zerofill NOT NULL auto_increment,
  `date` date NOT NULL,
  `detail` text NOT NULL,
  PRIMARY KEY  (`news_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2223 ;

-- 
-- dump ตาราง `news`
-- 

INSERT INTO `news` VALUES (00111, '2012-06-05', 'เข้าร่วมประชุมสัมนาหลังฝึกประสบการณ์');
INSERT INTO `news` VALUES (02222, '2012-10-09', 'สัมนาหลังฝึกประสบการณ์');
INSERT INTO `news` VALUES (00121, '2012-07-29', 'ส่งแบบบันทึกประจำวัน');
INSERT INTO `news` VALUES (00112, '2012-06-05', 'เข้าร่วมประชุมปฐมนิเทศก่อนฝึกประสบการณ์');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `project`
-- 

CREATE TABLE `project` (
  `project_id` int(5) unsigned zerofill NOT NULL auto_increment,
  `project_name` varchar(50) NOT NULL,
  `project_detail` text NOT NULL,
  `t_id` int(5) unsigned zerofill NOT NULL,
  PRIMARY KEY  (`project_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=334 ;

-- 
-- dump ตาราง `project`
-- 

INSERT INTO `project` VALUES (00111, 'ปฐมนิเทศก่อนฝึกประสบการณ์', 'สำหรับนักศึกษาชั้นปีที่ 4 ให้เข้าร่วมประชุมการปฐมนิเทศก่อนฝึกประสบการณ์ ที่ห้องประชุม 1 วันที่ 14 พฤศจิกายน เวลา 8.00 น. - 12.00 น. ', 00000);
INSERT INTO `project` VALUES (00222, 'สัมนาหลังฝึกประสบการณ์', 'สำหรับนักศึกษาชั้นปีที่ 4 ให้เข้าร่วมประชุมสัมนาหลังฝึกประสบการณ์ ที่ห้องประชุม 1 วันที่ 14 พฤศจิกายน เวลา 8.00 น. - 12.00 น. ', 00000);
INSERT INTO `project` VALUES (00333, 'โครงการนิเทศนักศึกษา', 'สำหรับอาจารย์ประจำสาขา ประชุมการนิเทศนักศึกษา ที่ห้อง 931 วันที่ 21 ธันวาคม 2555 เวลา 13.00 - 15.00 น.', 00000);
INSERT INTO `project` VALUES (00223, 'สัมนาหลังฝึกประสบการณ์', 'นักศึกษาชั้นปีที่ 4 สาขาวิทยาการคอมพิวเตอร์และเทคโนโลยีสารสนเทศ', 00000);

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `register`
-- 

CREATE TABLE `register` (
  `register_id` int(5) unsigned zerofill NOT NULL auto_increment,
  `points` decimal(10,2) default '0.00',
  `position` varchar(50) default NULL,
  `std_id` varchar(11) NOT NULL,
  `groups_num` varchar(10) NOT NULL,
  `company_id` int(5) unsigned zerofill default NULL,
  PRIMARY KEY  (`register_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=199 ;

-- 
-- dump ตาราง `register`
-- 

INSERT INTO `register` VALUES (00146, 50.00, 'ฝึกประสบการณ์', '52040249411', '00003', 01253);
INSERT INTO `register` VALUES (00147, 80.00, 'ฝึกประสบการณ์', '52040249412', '00002', 01254);
INSERT INTO `register` VALUES (00173, 0.00, 'ยังไม่ฝึกประสบการณ์', '52040249406', '00002', 00001);
INSERT INTO `register` VALUES (00153, 0.00, 'ฝึกประสบการณ์', '52040240414', '00002', 00002);
INSERT INTO `register` VALUES (00197, 0.00, 'ยังไม่ฝึกประสบการณ์', '52040235201', '00004', NULL);
INSERT INTO `register` VALUES (00155, 0.00, 'ยังไม่ฝึกประสบการณ์', '42424242424', '00003', 00004);
INSERT INTO `register` VALUES (00156, 0.00, 'ยังไม่ฝึกประสบการณ์', '52040249409', '00002', 00001);
INSERT INTO `register` VALUES (00157, 0.00, 'ฝึกประสบการณ์', '52040249408', '00002', 00006);
INSERT INTO `register` VALUES (00159, 0.00, 'ยังไม่ฝึกประสบการณ์', '45756754876', '00004', 00007);
INSERT INTO `register` VALUES (00160, 0.00, 'ฝึกประสบการณ์', '51240234567', '00004', 00008);
INSERT INTO `register` VALUES (00161, 0.00, 'ฝึกประสบการณ์', '51234534321', '00004', 01254);
INSERT INTO `register` VALUES (00162, 0.00, 'ฝึกประสบการณ์', '51040135219', '00004', 01255);
INSERT INTO `register` VALUES (00164, 0.00, 'ฝึกประสบการณ์', '51040354345', '00005', 01257);
INSERT INTO `register` VALUES (00165, 0.00, 'ยังไม่ฝึกประสบการณ์', '52040249333', '00005', 01258);
INSERT INTO `register` VALUES (00180, 0.00, 'ยังไม่ฝึกประสบการณ์', '52040249439', '00003', 00003);
INSERT INTO `register` VALUES (00198, 0.00, 'ยังไม่ฝึกประสบการณ์', '52040323434', '00005', NULL);
INSERT INTO `register` VALUES (00183, 0.00, 'ยังไม่ฝึกประสบการณ์', '52040249307', '00002', 00001);
INSERT INTO `register` VALUES (00195, 0.00, 'ยังไม่ฝึกประสบการณ์', '52040243817', '00001', NULL);
INSERT INTO `register` VALUES (00186, 0.00, 'ยังไม่ฝึกประสบการณ์', '52040249404', '00002', 00001);

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `reply`
-- 

CREATE TABLE `reply` (
  `ReplyID` int(5) unsigned zerofill NOT NULL auto_increment,
  `QuestionID` int(5) unsigned zerofill NOT NULL,
  `CreateDate` datetime NOT NULL,
  `Details` text NOT NULL,
  `Name` varchar(50) NOT NULL,
  PRIMARY KEY  (`ReplyID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

-- 
-- dump ตาราง `reply`
-- 

INSERT INTO `reply` VALUES (00001, 00001, '2012-06-13 17:13:09', '', 'honey');
INSERT INTO `reply` VALUES (00002, 00002, '2012-06-13 17:15:32', 'ฮ่วย', 'กะยังว่าบ่บอก');
INSERT INTO `reply` VALUES (00003, 00003, '2012-06-13 22:36:08', 'ไม่รู้สิ', 'Bam');
INSERT INTO `reply` VALUES (00004, 00004, '2012-06-13 23:25:27', 'นะ', 'นะนะ');
INSERT INTO `reply` VALUES (00005, 00005, '2012-06-14 11:07:45', 'ส่งวันจันทร์หน้า', 'อ.ขวัญชัย');
INSERT INTO `reply` VALUES (00006, 00005, '2012-06-14 11:21:16', 'ใช่หรอค่ะ', 'บุ๋ม');
INSERT INTO `reply` VALUES (00007, 00005, '2012-06-24 17:13:11', 'ใช่ค่ะๆ', 'ยิ้ม');
INSERT INTO `reply` VALUES (00008, 00006, '2012-07-16 10:58:43', 'ไม่ทราบค่ะ', 'non');
INSERT INTO `reply` VALUES (00009, 00007, '2012-09-09 15:11:28', 'ddddddddd', 'ddddddd');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `request`
-- 

CREATE TABLE `request` (
  `request_id` int(5) unsigned zerofill NOT NULL auto_increment,
  `skill` varchar(50) NOT NULL,
  `detail` text NOT NULL,
  `year` varchar(10) NOT NULL,
  `address` varchar(50) NOT NULL,
  `period` varchar(50) NOT NULL,
  `num` int(3) NOT NULL,
  `company_id` int(5) unsigned zerofill default NULL,
  PRIMARY KEY  (`request_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- 
-- dump ตาราง `request`
-- 

INSERT INTO `request` VALUES (00001, 'สามารถเขียนโปรแกรมได้', ' vbvvbfc', '2555', 'bvnb bb', '3 เดือน', 5, 01253);
INSERT INTO `request` VALUES (00002, 'สามารถเขียนโปรแกรมได้', 'fgbfgbgvfv', '2555', 'fghuhkm', '3 เดือน', 5, 01253);
INSERT INTO `request` VALUES (00004, 'สามารถเขียนโปรแกรมได้', 'tettee', '2555', 'ffvvb', '3 เดือน', 5, 01253);
INSERT INTO `request` VALUES (00005, 'สามารถเขียนโปรแกรมได้', 'fghch', '2555', 'dhdh', '3 เดือน', 5, 01253);
INSERT INTO `request` VALUES (00006, 'สามารถเขียนโปรแกรมได้', 'ต้องการนักศึกษาฝึกงาน', '2555', 'ขอนแก่น', '3 เดือน', 5, 00001);
INSERT INTO `request` VALUES (00010, 'เขียนโปรแกรมภาษา PHP ได้', 'ไม่มี', '2554', '55', '3 เดือน', 4, 00001);

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `responsible`
-- 

CREATE TABLE `responsible` (
  `responsible_id` int(10) unsigned NOT NULL auto_increment,
  `project_id` int(10) default NULL,
  `teacher_id` int(10) default NULL,
  `duty` varchar(50) NOT NULL,
  PRIMARY KEY  (`responsible_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- dump ตาราง `responsible`
-- 


-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `resume`
-- 

CREATE TABLE `resume` (
  `std_id` int(11) unsigned NOT NULL auto_increment,
  `std_idgard` varchar(30) NOT NULL,
  `std_bday` date NOT NULL,
  `std_nation` varchar(50) NOT NULL,
  `std_religion` varchar(50) NOT NULL,
  `blood` varchar(50) NOT NULL,
  `std_addr` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) default NULL,
  `std_number` int(10) NOT NULL,
  `talent` varchar(50) NOT NULL,
  `education` varchar(50) NOT NULL,
  `disability` varchar(30) NOT NULL,
  `father_name` varchar(50) NOT NULL,
  `mother_name` varchar(50) NOT NULL,
  `user_id` int(11) default NULL,
  PRIMARY KEY  (`std_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

-- 
-- dump ตาราง `resume`
-- 

INSERT INTO `resume` VALUES (1, '1238429439234', '2004-06-07', 'ไทย', 'พุทธ', 'B', 'ประเทศไทย', '0807511834', 'hun@hotmail.com', 1, 'ดูทีวี', 'ปวช', '-', 'น้ำใส', 'นาโน', 2);
INSERT INTO `resume` VALUES (2, '14320090244802', '2006-06-12', 'ไทย', 'อิสราม', 'A', 'ปากเกล็ด  นนทบุรี', '0804107564', 'gu@hotmail.com', 1, 'เล่นกีฬา', 'ม.6', '-', 'จันทร์', 'น้ำใส', 3);
INSERT INTO `resume` VALUES (3, '1430200144288', '2533-12-29', 'ไทย', 'พุทธ', 'B', 'ไทย', '0909871234', 'master_bank@hotmail.com', 1, 'พูดภาษาจีนได้', 'ปวช', '-', 'สมชาย', 'สมสี', 4);
INSERT INTO `resume` VALUES (4, '1203458938954', '2534-08-03', 'ไทย', 'อิสราม', 'AB', 'จับไม้  หนองคาย', '0807511833', 'sum@hotmail.com', 2, 'เล่นบอลเลย์บอล', 'ปวช', '', 'สมศักดิ์', 'ไหม', 5);
INSERT INTO `resume` VALUES (7, '1234958654321', '2534-12-01', 'ไทย', 'พุทธ', 'O', '\r\n\r\nอุบล', '0807411838', 'hunnyguy@hotmail.com', 3, 'พูดภาษาจีนได้', 'ปวช', '', '', 'นางไหม  สายดี', 0);
INSERT INTO `resume` VALUES (8, '1430200144288', '2533-12-29', 'ไทย', 'พุทธ', 'B', 'thailand', '0909871234', 'cutefulllife__honey@windowslive.com', 3, 'เขียนโปรแกรมphp', 'มัธยมศึกษาปีที่6', '', '', 'เอมอร', 0);

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `score`
-- 

CREATE TABLE `score` (
  `score_id` int(5) unsigned zerofill NOT NULL auto_increment,
  `point` int(3) NOT NULL,
  `t_id` int(5) unsigned zerofill NOT NULL,
  `tscore_id` int(5) unsigned zerofill NOT NULL,
  `register_id` int(5) unsigned zerofill NOT NULL,
  PRIMARY KEY  (`score_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=145 ;

-- 
-- dump ตาราง `score`
-- 

INSERT INTO `score` VALUES (00001, 10, 00000, 00024, 00173);
INSERT INTO `score` VALUES (00002, 10, 00000, 00023, 00173);
INSERT INTO `score` VALUES (00003, 0, 00000, 00016, 00173);
INSERT INTO `score` VALUES (00004, 100, 00000, 00017, 00173);
INSERT INTO `score` VALUES (00005, 0, 00000, 00022, 00173);
INSERT INTO `score` VALUES (00006, 10, 00000, 00020, 00173);
INSERT INTO `score` VALUES (00007, 0, 00000, 00021, 00173);
INSERT INTO `score` VALUES (00008, 10, 00000, 00013, 00173);
INSERT INTO `score` VALUES (00009, 0, 00000, 00014, 00173);
INSERT INTO `score` VALUES (00010, 0, 00000, 00024, 00000);
INSERT INTO `score` VALUES (00011, 0, 00000, 00023, 00000);
INSERT INTO `score` VALUES (00012, 0, 00000, 00016, 00000);
INSERT INTO `score` VALUES (00013, 0, 00000, 00017, 00000);
INSERT INTO `score` VALUES (00014, 0, 00000, 00022, 00000);
INSERT INTO `score` VALUES (00015, 0, 00000, 00020, 00000);
INSERT INTO `score` VALUES (00016, 0, 00000, 00021, 00000);
INSERT INTO `score` VALUES (00017, 0, 00000, 00013, 00000);
INSERT INTO `score` VALUES (00018, 0, 00000, 00014, 00000);
INSERT INTO `score` VALUES (00019, 0, 00000, 00024, 00146);
INSERT INTO `score` VALUES (00020, 0, 00000, 00023, 00146);
INSERT INTO `score` VALUES (00021, 20, 01678, 00016, 00146);
INSERT INTO `score` VALUES (00022, 0, 00000, 00017, 00146);
INSERT INTO `score` VALUES (00023, 50, 01678, 00022, 00146);
INSERT INTO `score` VALUES (00024, 0, 00000, 00020, 00146);
INSERT INTO `score` VALUES (00025, 60, 01678, 00021, 00146);
INSERT INTO `score` VALUES (00026, 0, 00000, 00013, 00146);
INSERT INTO `score` VALUES (00027, 70, 01678, 00014, 00146);
INSERT INTO `score` VALUES (00028, 0, 00000, 00024, 00186);
INSERT INTO `score` VALUES (00029, 0, 00000, 00023, 00186);
INSERT INTO `score` VALUES (00030, 80, 01678, 00016, 00186);
INSERT INTO `score` VALUES (00031, 0, 00000, 00017, 00186);
INSERT INTO `score` VALUES (00032, 80, 01678, 00022, 00186);
INSERT INTO `score` VALUES (00033, 0, 00000, 00020, 00186);
INSERT INTO `score` VALUES (00034, 80, 01678, 00021, 00186);
INSERT INTO `score` VALUES (00035, 0, 00000, 00013, 00186);
INSERT INTO `score` VALUES (00036, 80, 01678, 00014, 00186);
INSERT INTO `score` VALUES (00037, 40, 01678, 00025, 00186);
INSERT INTO `score` VALUES (00038, 40, 01678, 00026, 00186);
INSERT INTO `score` VALUES (00039, 40, 01678, 00027, 00186);
INSERT INTO `score` VALUES (00040, 40, 01678, 00028, 00186);
INSERT INTO `score` VALUES (00041, 0, 00000, 00024, 00156);
INSERT INTO `score` VALUES (00042, 0, 00000, 00023, 00156);
INSERT INTO `score` VALUES (00043, 80, 01678, 00016, 00156);
INSERT INTO `score` VALUES (00044, 0, 00000, 00017, 00156);
INSERT INTO `score` VALUES (00045, 80, 01678, 00022, 00156);
INSERT INTO `score` VALUES (00046, 0, 00000, 00020, 00156);
INSERT INTO `score` VALUES (00047, 80, 01678, 00021, 00156);
INSERT INTO `score` VALUES (00048, 0, 00000, 00013, 00156);
INSERT INTO `score` VALUES (00049, 70, 01678, 00014, 00156);
INSERT INTO `score` VALUES (00050, 40, 01678, 00025, 00156);
INSERT INTO `score` VALUES (00051, 40, 01678, 00026, 00156);
INSERT INTO `score` VALUES (00052, 40, 01678, 00027, 00156);
INSERT INTO `score` VALUES (00053, 30, 01678, 00028, 00156);
INSERT INTO `score` VALUES (00054, 0, 00000, 00024, 00153);
INSERT INTO `score` VALUES (00055, 0, 00000, 00023, 00153);
INSERT INTO `score` VALUES (00056, 80, 01678, 00016, 00153);
INSERT INTO `score` VALUES (00057, 0, 00000, 00017, 00153);
INSERT INTO `score` VALUES (00058, 80, 01678, 00022, 00153);
INSERT INTO `score` VALUES (00059, 0, 00000, 00020, 00153);
INSERT INTO `score` VALUES (00060, 80, 01678, 00021, 00153);
INSERT INTO `score` VALUES (00061, 0, 00000, 00013, 00153);
INSERT INTO `score` VALUES (00062, 60, 01678, 00014, 00153);
INSERT INTO `score` VALUES (00063, 30, 01678, 00025, 00153);
INSERT INTO `score` VALUES (00064, 30, 01678, 00026, 00153);
INSERT INTO `score` VALUES (00065, 30, 01678, 00027, 00153);
INSERT INTO `score` VALUES (00066, 30, 01678, 00028, 00153);
INSERT INTO `score` VALUES (00067, 0, 00000, 00024, 00155);
INSERT INTO `score` VALUES (00068, 0, 00000, 00023, 00155);
INSERT INTO `score` VALUES (00069, 60, 01678, 00016, 00155);
INSERT INTO `score` VALUES (00070, 0, 00000, 00017, 00155);
INSERT INTO `score` VALUES (00071, 60, 01678, 00022, 00155);
INSERT INTO `score` VALUES (00072, 0, 00000, 00020, 00155);
INSERT INTO `score` VALUES (00073, 60, 01678, 00021, 00155);
INSERT INTO `score` VALUES (00074, 0, 00000, 00013, 00155);
INSERT INTO `score` VALUES (00075, 40, 01678, 00014, 00155);
INSERT INTO `score` VALUES (00076, 30, 01678, 00025, 00155);
INSERT INTO `score` VALUES (00077, 20, 01678, 00026, 00155);
INSERT INTO `score` VALUES (00078, 40, 01678, 00027, 00155);
INSERT INTO `score` VALUES (00079, 50, 01678, 00028, 00155);
INSERT INTO `score` VALUES (00080, 0, 00000, 00024, 00183);
INSERT INTO `score` VALUES (00081, 0, 00000, 00023, 00183);
INSERT INTO `score` VALUES (00082, 70, 01678, 00016, 00183);
INSERT INTO `score` VALUES (00083, 0, 00000, 00017, 00183);
INSERT INTO `score` VALUES (00084, 70, 01678, 00022, 00183);
INSERT INTO `score` VALUES (00085, 0, 00000, 00020, 00183);
INSERT INTO `score` VALUES (00086, 70, 01678, 00021, 00183);
INSERT INTO `score` VALUES (00087, 0, 00000, 00013, 00183);
INSERT INTO `score` VALUES (00088, 80, 01678, 00014, 00183);
INSERT INTO `score` VALUES (00089, 50, 01678, 00025, 00183);
INSERT INTO `score` VALUES (00090, 50, 01678, 00026, 00183);
INSERT INTO `score` VALUES (00091, 50, 01678, 00027, 00183);
INSERT INTO `score` VALUES (00092, 40, 01678, 00028, 00183);
INSERT INTO `score` VALUES (00093, 0, 00000, 00024, 00180);
INSERT INTO `score` VALUES (00094, 0, 00000, 00023, 00180);
INSERT INTO `score` VALUES (00095, 100, 01678, 00016, 00180);
INSERT INTO `score` VALUES (00096, 0, 00000, 00017, 00180);
INSERT INTO `score` VALUES (00097, 100, 01678, 00022, 00180);
INSERT INTO `score` VALUES (00098, 0, 00000, 00020, 00180);
INSERT INTO `score` VALUES (00099, 90, 01678, 00021, 00180);
INSERT INTO `score` VALUES (00100, 0, 00000, 00013, 00180);
INSERT INTO `score` VALUES (00101, 60, 01678, 00014, 00180);
INSERT INTO `score` VALUES (00102, 40, 01678, 00025, 00180);
INSERT INTO `score` VALUES (00103, 40, 01678, 00026, 00180);
INSERT INTO `score` VALUES (00104, 30, 01678, 00027, 00180);
INSERT INTO `score` VALUES (00105, 30, 01678, 00028, 00180);
INSERT INTO `score` VALUES (00106, 0, 00000, 00024, 00164);
INSERT INTO `score` VALUES (00107, 0, 00000, 00023, 00164);
INSERT INTO `score` VALUES (00108, 80, 01678, 00016, 00164);
INSERT INTO `score` VALUES (00109, 0, 00000, 00017, 00164);
INSERT INTO `score` VALUES (00110, 70, 01678, 00022, 00164);
INSERT INTO `score` VALUES (00111, 0, 00000, 00020, 00164);
INSERT INTO `score` VALUES (00112, 60, 01678, 00021, 00164);
INSERT INTO `score` VALUES (00113, 0, 00000, 00013, 00164);
INSERT INTO `score` VALUES (00114, 90, 01678, 00014, 00164);
INSERT INTO `score` VALUES (00115, 50, 01678, 00025, 00164);
INSERT INTO `score` VALUES (00116, 40, 01678, 00026, 00164);
INSERT INTO `score` VALUES (00117, 30, 01678, 00027, 00164);
INSERT INTO `score` VALUES (00118, 40, 01678, 00028, 00164);
INSERT INTO `score` VALUES (00119, 90, 00000, 00024, 00157);
INSERT INTO `score` VALUES (00120, 90, 00000, 00023, 00157);
INSERT INTO `score` VALUES (00121, 90, 01678, 00016, 00157);
INSERT INTO `score` VALUES (00122, 90, 00000, 00017, 00157);
INSERT INTO `score` VALUES (00123, 90, 01678, 00022, 00157);
INSERT INTO `score` VALUES (00124, 90, 00000, 00020, 00157);
INSERT INTO `score` VALUES (00125, 90, 01678, 00021, 00157);
INSERT INTO `score` VALUES (00126, 90, 00000, 00013, 00157);
INSERT INTO `score` VALUES (00127, 90, 01678, 00014, 00157);
INSERT INTO `score` VALUES (00128, 50, 01678, 00025, 00157);
INSERT INTO `score` VALUES (00129, 40, 01678, 00026, 00157);
INSERT INTO `score` VALUES (00130, 30, 01678, 00027, 00157);
INSERT INTO `score` VALUES (00131, 30, 01678, 00028, 00157);
INSERT INTO `score` VALUES (00132, 0, 00000, 00024, 00198);
INSERT INTO `score` VALUES (00133, 0, 00000, 00023, 00198);
INSERT INTO `score` VALUES (00134, 0, 00000, 00016, 00198);
INSERT INTO `score` VALUES (00135, 0, 00000, 00017, 00198);
INSERT INTO `score` VALUES (00136, 0, 00000, 00022, 00198);
INSERT INTO `score` VALUES (00137, 0, 00000, 00020, 00198);
INSERT INTO `score` VALUES (00138, 0, 00000, 00021, 00198);
INSERT INTO `score` VALUES (00139, 0, 00000, 00013, 00198);
INSERT INTO `score` VALUES (00140, 0, 00000, 00014, 00198);
INSERT INTO `score` VALUES (00141, 0, 00000, 00025, 00198);
INSERT INTO `score` VALUES (00142, 0, 00000, 00026, 00198);
INSERT INTO `score` VALUES (00143, 0, 00000, 00027, 00198);
INSERT INTO `score` VALUES (00144, 0, 00000, 00028, 00198);

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `student`
-- 

CREATE TABLE `student` (
  `std_id` varchar(11) NOT NULL,
  `std_name` varchar(50) NOT NULL,
  `address` varchar(200) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `major` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `resume` varchar(100) default NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `picture` varchar(100) NOT NULL,
  PRIMARY KEY  (`std_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- dump ตาราง `student`
-- 

INSERT INTO `student` VALUES ('52040249406', 'น้ำใส  ใจสอาด55', 'อุดร      55                  ', '080741183355', 'วิทยาการคอมพิวเตอร์', 'namk@hotmail.com', '52040249406.doc', 'nam', 'nam      ', '52040249406.jpg');
INSERT INTO `student` VALUES ('52040240414', 'ยลดา สมเมตร', 'thai        ', '0984455332', 'เทคโนโลยีสารสนเทศ', 'yy@gmail.com', '52040240414.txt', 'yoly', 'yol123   ', '52040240414.png');
INSERT INTO `student` VALUES ('52040235201', 'กก กก', 'thailand  ', '0809878765', 'เทคโนโลยีสารสนเทศ', 'kk@hotmail.com', '52040235201.doc', 'korkor', '1234', '52040235201.jpg');
INSERT INTO `student` VALUES ('52040249439', 'ขข ขข', 'thailand  ', '0888954564', 'เทคโนโลยีสารสนเทศ', 'kun@hotmail.com', '52040249439.docx', 'gorgor', '4567', '52040249439.JPG');
INSERT INTO `student` VALUES ('52040249409', 'น้ำสาย', 'tt                                                                                                                                                                                                      ', '0909871234', 'วิทยาการคอมพิวเตอร์', 'master_bank@hotmail.com', '52040249409.docx', 'namsay', '4545         ', '52040249409.JPG');
INSERT INTO `student` VALUES ('52040249512', 'ศิริวรรณ มั่นใจ', '5656            ', '0894455435', 'เทคโนโลยีสารสนเทศ', 'uu@hotmail.com', '52040249512.txt', 'ringtone', '5656      ', '52040249512.JPG');
INSERT INTO `student` VALUES ('52040249404', 'น้ำใส  ใจสอาด', 'อุดร    ', '0807411831', 'วิทยาการคอมพิวเตอร์', 'nam@hotmail.com', '52040249404.docx', 'pokko', '7897', '52040249404.JPG');
INSERT INTO `student` VALUES ('52040249408', 'น้ำใจ', 'อุดร    ', '0807411830', 'เทคโนโลยีสารสนเทศ', 'namjai@hotmail.com', '52040249408.docx', 'namjai', 'namjai  ', '52040249408.JPG');
INSERT INTO `student` VALUES ('51240234567', 'สมปอง คลองศักดิ์', 'frffrfe  ', '0807411833', 'วิทยาการคอมพิวเตอร์', 'gfgf@hotmail.com', '51240234567.docx', 'aaa', 'aaa ', '51240234567.JPG');
INSERT INTO `student` VALUES ('52040249307', 'สายน้ำ', 'fjkdiovj    ', '0807411811', 'เทคโนโลยีสารสนเทศ', 'say@hotmail.com', '52040249307.docx', 'song', '9874', '52040249307.JPG');
INSERT INTO `student` VALUES ('51234534321', 'สมศรี มั่งมี', 'thailand', '0850487069', 'เทคโนโลยีสารสนเทศ', 'rungtiwa.cs@gmail.com', '51234534321.docx', 'w', 'w ', '51234534321.JPG');
INSERT INTO `student` VALUES ('51040135219', 'ทดสอบ', 'tt', '0807511838', 'วิทยาการคอมพิวเตอร์', 'pukky-do@hotmail.com', 'สอบเขียนโปรแกรม.pdf', 'hu', '123 ', '');
INSERT INTO `student` VALUES ('51040354345', 'สมชาย', '  dfghjkl', '0807511838', 'วิทยาการคอมพิวเตอร์', 'pukky_do@hotmail.com', 'คู่มือการลงทะเบียนเข้าร่วมโครงการในระบบ.doc', 'hy', 'hy ', '');
INSERT INTO `student` VALUES ('51323445412', '325325', '  sdfghjkl                                            ', '0807511838', 'วิทยาการคอมพิวเตอร์', 'pukky_do@hotmail.com', '51323445412.docx', 'tr', 'tr  ', '51323445412.jpg');
INSERT INTO `student` VALUES ('52040323434', 'student', 'thailand', '0989988765', 'วิทยาการคอมพิวเตอร์', 'student@hotmail.com', '52040323434.docx', 'student', 'student', '52040323434.JPG');
INSERT INTO `student` VALUES ('52040249411', 'รจนา ศาลไทย', 'thailand      ', '0894453212', 'วิทยาการคอมพิวเตอร์', 'rotjana@hotmail.com', '_ SIPA _.pdf', 'rotjana', 'rotjana   ', '');
INSERT INTO `student` VALUES ('52040243817', 'fgh', 'th', '0807511838', 'เทคโนโลยีสารสนเทศ', 'pukky_do@hotmail.com', 'test.txt', 'fgh', 'fgh', '');
INSERT INTO `student` VALUES ('52040132123', '7878', '7878', '0894455435', 'วิทยาการคอมพิวเตอร์', 'pukky_do@hotmail.com', '123.txt', '7878', '7878', '');
INSERT INTO `student` VALUES ('52040235203', 'แก้วการ บุญเกิด', 'thailand    ', '0894455435', 'วิทยาการคอมพิวเตอร์', 'pukky-do@hotmail.com', '52040235203.', 'root', '1234', '52040235203.');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `supervision`
-- 

CREATE TABLE `supervision` (
  `su_id` int(5) unsigned zerofill NOT NULL auto_increment,
  `date` date NOT NULL,
  `advice` text NOT NULL,
  `t_id` int(5) unsigned zerofill NOT NULL,
  `register_id` int(5) unsigned zerofill NOT NULL,
  PRIMARY KEY  (`su_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

-- 
-- dump ตาราง `supervision`
-- 

INSERT INTO `supervision` VALUES (00001, '2012-09-17', 'นักศึกษา', 07788, 00146);
INSERT INTO `supervision` VALUES (00002, '2012-06-14', 'มาไม่ตรงเวลา', 00002, 00147);
INSERT INTO `supervision` VALUES (00003, '2012-04-10', 'นักศึกษาไม่เคารพ', 00003, 00173);
INSERT INTO `supervision` VALUES (00004, '2011-06-14', 'การเดินทาง', 00004, 00153);
INSERT INTO `supervision` VALUES (00011, '2012-09-09', '', 01678, 00181);
INSERT INTO `supervision` VALUES (00010, '2012-09-09', '', 01678, 00153);
INSERT INTO `supervision` VALUES (00012, '2012-09-10', '', 00003, 00169);
INSERT INTO `supervision` VALUES (00013, '2012-09-10', '', 00001, 00173);

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `teacher`
-- 

CREATE TABLE `teacher` (
  `t_id` int(5) unsigned zerofill NOT NULL auto_increment,
  `t_name` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY  (`t_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7789 ;

-- 
-- dump ตาราง `teacher`
-- 

INSERT INTO `teacher` VALUES (00001, 'cc', '0987656543', 'cc@hotmail.com', 'c', '9', 'admin');
INSERT INTO `teacher` VALUES (00002, 'admin', 'd', 'd', 'admin', 'admin', 'admin');
INSERT INTO `teacher` VALUES (00003, 'e', 'e', 'e', 'e', 'e', 'teacher_general');
INSERT INTO `teacher` VALUES (00004, 'f', 'f', 'f', 'f', 'f', 'officer');
INSERT INTO `teacher` VALUES (03344, 'น้ำใจ 55', '0987776652', 'goppy55@gmail.com', 'general', 'general', 'teacher_general');
INSERT INTO `teacher` VALUES (07788, 'สมร', '0871234543', 'samon@gmail.com', 'officer', 'officer', 'officer');
INSERT INTO `teacher` VALUES (01678, 'รัตนา ศรีลศาล', '0893454345', 'rattana@hotmail.com', 'teacher', 'teacher', 'teacher');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `titlescore`
-- 

CREATE TABLE `titlescore` (
  `tscore_id` int(5) unsigned zerofill NOT NULL auto_increment,
  `tscore_name` varchar(50) NOT NULL,
  `marks` varchar(10) NOT NULL,
  `assessor` varchar(50) NOT NULL,
  PRIMARY KEY  (`tscore_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

-- 
-- dump ตาราง `titlescore`
-- 

INSERT INTO `titlescore` VALUES (00024, 'ประเมินปฏิบัติเวลาในการปฏิบัติงาน', '100', 'หน่วยงาน');
INSERT INTO `titlescore` VALUES (00023, 'ประเมินการปฏิบัติงาน', '100', 'หน่วยงาน');
INSERT INTO `titlescore` VALUES (00016, 'ประเมินปฏิบัติเวลาในการปฏิบัติงาน', '100', 'อาจารย์');
INSERT INTO `titlescore` VALUES (00017, 'ประเมินการปฏิบัติงาน', '100', 'หน่วยงาน');
INSERT INTO `titlescore` VALUES (00022, 'ประเมินปฏิบัติเวลาในการปฏิบัติงาน', '100', 'อาจารย์');
INSERT INTO `titlescore` VALUES (00020, 'ประเมินบุคลิกภาพ', '100', 'หน่วยงาน');
INSERT INTO `titlescore` VALUES (00021, 'ประเมินบุคลิกภาพ', '100', 'อาจารย์');
INSERT INTO `titlescore` VALUES (00013, 'ประเมินการปฏิบัติงาน', '100', 'หน่วยงาน');
INSERT INTO `titlescore` VALUES (00014, 'ประเมินบุคลิกภาพ', '90', 'อาจารย์');
INSERT INTO `titlescore` VALUES (00025, 'คะแนนการนิเทศ', '50', 'อาจารย์');
INSERT INTO `titlescore` VALUES (00026, 'คะแนนการดำเนินงานเอกสาร', '50', 'อาจารย์');
INSERT INTO `titlescore` VALUES (00027, 'คะแนนเข้าร่วมกิจกรรม', '50', 'อาจารย์');
INSERT INTO `titlescore` VALUES (00028, 'คะแนนส่งเอกสาร', '50', 'อาจารย์');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `train`
-- 

CREATE TABLE `train` (
  `trn_id` int(5) unsigned zerofill NOT NULL auto_increment,
  `company_id` int(5) unsigned zerofill NOT NULL,
  `std_id` varchar(11) NOT NULL,
  `status` int(11) NOT NULL default '0',
  PRIMARY KEY  (`trn_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

-- 
-- dump ตาราง `train`
-- 

INSERT INTO `train` VALUES (00001, 00001, '52040249439', 1);
INSERT INTO `train` VALUES (00002, 00002, '52040249404', 1);
INSERT INTO `train` VALUES (00003, 00001, '52040249408', 1);
INSERT INTO `train` VALUES (00004, 00003, '52040249406', 2);
INSERT INTO `train` VALUES (00005, 00003, '52040249307', 2);
INSERT INTO `train` VALUES (00006, 00005, '5555555555', 0);
INSERT INTO `train` VALUES (00009, 00005, '25425252', 0);
INSERT INTO `train` VALUES (00008, 01254, '52040244417', 0);
INSERT INTO `train` VALUES (00011, 00002, '52040249409', 1);
INSERT INTO `train` VALUES (00012, 00003, '52040249406', 2);
INSERT INTO `train` VALUES (00013, 00004, '52040235201', 1);
INSERT INTO `train` VALUES (00015, 00001, '52040323434', 1);
INSERT INTO `train` VALUES (00016, 00003, '52040235203', 0);
INSERT INTO `train` VALUES (00017, 00002, '51323445412', 0);

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `user`
-- 

CREATE TABLE `user` (
  `user_id` int(11) unsigned NOT NULL auto_increment,
  `user_name` varchar(50) NOT NULL,
  `email` varchar(50) default NULL,
  `phone` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `pwd` varchar(20) NOT NULL,
  `status` enum('admin','user') NOT NULL default 'user',
  PRIMARY KEY  (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

-- 
-- dump ตาราง `user`
-- 

INSERT INTO `user` VALUES (1, 'มาม่า ต้มยำ', 'mama@gmail.com', '0893454345', 'mama', 'mama1234', 'admin');
INSERT INTO `user` VALUES (2, 'nittayanaruk', 'cutefulllife__honey@windowslive.com', '0801932320', 'honey', 'honey12345', 'user');
INSERT INTO `user` VALUES (3, 'เบญจวรรณ', 'hunny@hotmail.com', '0807511838', 'hunny', 'hunny123', 'user');
INSERT INTO `user` VALUES (4, 'สำเร็จ', 'sumret@hotmail.com', '0983456781', 'sumret', 'sumret123', 'admin');
INSERT INTO `user` VALUES (5, 'กรณิการ์', 'kronika@hotmail.com', '0908764523', 'kron', 'kron123', 'admin');
INSERT INTO `user` VALUES (6, 'รังสิมา', 'rungsima@hotmail.com', '0876745321', 'jam', 'jam555', 'user');
INSERT INTO `user` VALUES (7, 'น้อยใจ', 'noi@hotmail.com', '0454321234', 'noi', 'noi123', 'user');
INSERT INTO `user` VALUES (8, 'แพรว', 'praw@hotmail.com', '0945632123', 'praw', 'praw123', 'admin');
INSERT INTO `user` VALUES (9, 'สมจิต', 'somjit@gmail.com', '0904564321', 'somjit', 'somjit123', 'user');
INSERT INTO `user` VALUES (11, 'jum', 'jumjim@gmail.com', '0897654567', 'jumjim', 'jum1234', 'admin');
INSERT INTO `user` VALUES (14, 'num', 'numnim@gmail.com', '0904564321', 'numnim', 'numnim123', 'admin');
INSERT INTO `user` VALUES (15, 'bell', 'bell@gmail.com', '0945673456', 'bellla', 'bell123', 'user');
INSERT INTO `user` VALUES (16, 'ทะเล', 'tale@yahoo.com', '0893454345', 'tale', 'tale123', 'admin');
INSERT INTO `user` VALUES (17, 'mai', 'mai@yahoo.com', '0904564321', 'maimaya', 'mai123', 'user');
INSERT INTO `user` VALUES (18, 'momo', 'momo@hotmail.com', '0908764523', 'momo', 'momo123', 'user');
INSERT INTO `user` VALUES (19, 'pum', 'pumpim@hotmail.com', '09\\', 'pumpim', 'pum123', 'user');
INSERT INTO `user` VALUES (20, 'young', 'young@hotmail.com', '0906786590', 'young', 'young123', 'user');
INSERT INTO `user` VALUES (23, 'วัชรากร ไชยวาน', 'makro_gak@hotmail.com', '0850845666', 'makro', 'makro123', 'user');
INSERT INTO `user` VALUES (24, 'น้ำใส', 'nam@hotmail.com', '0909871234', 'honey', 'honey12345', 'user');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `webboard`
-- 

CREATE TABLE `webboard` (
  `QuestionID` int(5) unsigned zerofill NOT NULL auto_increment,
  `CreateDate` datetime NOT NULL,
  `Question` varchar(255) NOT NULL,
  `Details` text NOT NULL,
  `Name` varchar(50) NOT NULL,
  `View` int(5) NOT NULL,
  `Reply` int(5) NOT NULL,
  PRIMARY KEY  (`QuestionID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

-- 
-- dump ตาราง `webboard`
-- 

INSERT INTO `webboard` VALUES (00002, '2012-06-13 17:15:01', 'ขอสอบถามเรื่องการฝึกประสบการณ์', 'ฝึกระยะเวลากี่วันค่ะ', 'honey', 4, 1);
INSERT INTO `webboard` VALUES (00007, '2012-07-12 00:00:00', 'รายงานบันทึกประจำวัน', 'ให้นักศึกษาส่งรายงานบันทึกประจำวันให้ตรงกำหนด', 'อ.วิชุดา', 11, 1);
INSERT INTO `webboard` VALUES (00004, '2012-06-13 22:48:52', 'how to make javascript', 'ทำอย่างไร', 'Jumb', 8, 1);
INSERT INTO `webboard` VALUES (00005, '2012-06-14 11:07:01', 'ส่งหนังสือฝึกงานเมื่อไหร่ค่ะ', 'อยากทราบค่ะ', 'ฮันนี่', 10, 3);
INSERT INTO `webboard` VALUES (00006, '2012-03-07 00:00:00', 'ส่งรายชื่อแหล่งฝึกประสบการณ์', 'ต้องส่งรายชื่อแหล่งฝึกประสบการณ์วันไหนค่ะ', 'jum', 3, 1);
