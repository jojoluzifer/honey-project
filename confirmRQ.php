<?
ob_start();
 session_start(); 
 
 $type_user = $_SESSION['usertype'];

if ($type_user != "teacher")
	{
	
		header("Location:user_login.php");
		
		
	}
	
 ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="EN" lang="EN" dir="ltr"><!-- InstanceBegin template="/Templates/index_full.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head profile="http://gmpg.orgs/xfn/11">
<title>ระบบสารสนเทศฝึกประสบการณ์วิชาชีพฯ :<?php echo $_SESSION['usertype']; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="imagetoolbar" content="no" />
<link rel="stylesheet" href="css/layout.css" type="text/css" />
<link rel="stylesheet" href="css/layout.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/style-mix.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="style.css" />
   
        
        <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="jquery-1.7.2.min.js"></script>
     

</head>
<body id="top">
<div class="wrapper row1">
  <div id="header" class="clear">
    <div class="fl_left">
    <p><img src="img/logo5.png" width="80" height="100"></p>
    </div>
      <div class="fl_center">
      <div class="fl_right" align="right">
      <ul>
        <li><a href="index.php">หนัาหลัก</a></li>
        <li><a href="Webboard.php">เว็บบอร์ด</a></li>
         <?php if(!$_SESSION['username']){?>
        <li><a href="user_login.php">เข้าสู่ระบบ</a></li>
        <li><a href="submit3.php">สมัครสมาชิก</a></li>
        <?php } else { ?>
        <li class="last"><a href="user_logout.php">ออกจากระบบ</a></li>
        <?php } ?>
      </ul>
    </div>
      <br>
      <br>
      <h1>&nbsp;&nbsp;ระบบสารสนเทศฝึกประสบการณ์วิชาชีพ</h1>
      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;สาขาวิทยาการคอมพิวเตอร์และเทคโนโลยีสารสนเทศ</p>
    </div>
 
  </div>
</div>

<div class="wrapper row2">
  <div id="topnav">
                                    <div class="row-fluid">
                                        <div class="span9">
                                            <ul>

                                        <!-- admin -->
                                        <? if ($_SESSION["usertype"] == "admin") { ?>
                                            <li><a href="show_std.php">จัดการข้อมูลนักศึกษา</a></li>
                                            <li><a href="show_teacher.php">จัดการข้อมูลอาจารย์</a></li>
                                            <li><a href="show_company.php">จัดการข้อมูลหน่วยงาน</a></li>

                                            <!-- company -->
                                        <? } else if ($_SESSION["usertype"] == "company") { ?>
                                         <li><a href="company_detail.php">ข้อมูลหน่วยงาน</a></li>
                                            <li><a href="show_request_company.php">แจ้งความจำนงค์รับนักศึกษา</a></li>
                                            <li><a href="report_StdResume.php">รายงานประวัติส่วนตัวของนักศึกษา</a></li>
                                            <li><a href="manage_score_forcompany.php">กรอกข้อมูลประเมินผล</a></li>

                                        <? } else if ($_SESSION["usertype"] == "officer") { ?>

                                            <!-- officer -->
                                            <li><a href="#">จัดการข้อมูลหนังสือฝึกประสบการณ์วิชาชีพ</a>
                                                <ul>
                                                    <li><a href="manage_send_request.php">หนังสือขอความอนุเคราะห์ฝึกประสบการณ์วิชาชีพ</a></li>
                                                    <li><a href="manage_sendSupervision.php">หนังสือนิเทศนักศึกษา</a></li>
                                                    <li><a href="manage_sendStd.php">หนังสือส่งตัวนักศึกษา</a></li>
                                                    <li><a href="manage_sendStd1.php">หนังสือขอตัวนักศึกษา</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">จัดการข้อมูลหนังสือโครงการ</a>
                                                <ul>
                                                    <li><a href="manage_sendProject.php">โครงการปฐมนิเทศก่อนฝึกประสบการณ์</a></li>
                                                    <li><a href="manage_sendSamana.php">โครงการสัมนาหลังฝึกประสบการณ์</a></li>
                                                </ul>
                                            </li>

                                        <? } else if ($_SESSION["usertype"] == "student") { ?>
                                            <!-- student -->
                                            <li><a href="resume2.php">ประวัติส่วนตัว</a></li>
                                            <?php 
											 include "connect2.php";
										   $sql = "select * from register,student where student.std_id = register.std_id and student.std_id = '".$_SESSION['std_id']."'  ";
										   $rs = mysql_query($sql);
										   $row = mysql_fetch_array($rs);
										   $num_rows = mysql_num_rows($rs);
										   
										   $position = $row['position']; 
										   
											if($num_rows != 0){?>
												
                                            <li><a href="StdRequest2.php">ส่งคำร้องขอฝึกประสบการณ์</a></li>
                                            <li><a href="result_request.php">ผลอนุมัติการฝึกประสบการณ์</a></li>
                                            
                                            <?php  if ($position == 3){?>
                                            
                                            <li><a href="show_diary.php">บันทึกประจำวัน</a></li>
                                            
                                            <?php }
											} ?>

                                        <? } else if ($_SESSION["usertype"] == "teacher_general") { ?>
                                            <!-- teacher_general -->
                                            <li><a href="show_supervision_forTG.php">ตารางการนิเทศ</a></li>
                                            <li><a href="show_std_TG.php">ข้อมูลนักศึกษาฝึกประสบการณ์</a></li>
                                            <li><a href="show_company_TG.php">ข้อมูลแหล่งฝึกประสบการณ์</a></li>
                                            <li><a href="show_news_forteachergeneral.php">จัดการข่าวประชาสัมพันธ์</a></li>

                                        <? } else if ($_SESSION["usertype"] == "teacher") { ?>
                                            <!-- teacher -->
                                            <li><a href="#">จัดการข้อมูลทั่วไป</a>
                                                <ul>
                                                    <li><a href="show_std_forteacher.php">ข้อมูลนักศึกษา</a></li>
                                                    <li><a href="show_company_forteacher.php">ข้อมูลแหล่งฝึกประสบการณ์</a></li>
                                                    <li><a href="show_project.php">ข้อมูลโครงการ</a></li>
                                                    <li><a href="show_group.php">ข้อมูลหมู่เรียน</a></li>
                                                    <li><a href="register_new3.php">จัดการหมู่เรียนนักศึกษา</a></li>
                                                      <li><a href="manage_score.php">กรอกข้อมูลประเมินผล</a></li>
                                                          <li><a href="show_titlescore.php">ข้อมูลหัวข้อการประเมินผล</a></li>
                                                    <li><a href="show_news.php">ข้อมูลข่าวประชาสัมพันธ์</a></li>
                                                    <li><a href="show_webboard.php">ข้อมูลกระทู้</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">จัดการข้อมูลการนิเทศ</a>
                                                <ul>
                                                    <li><a href="show_supervision2.php">ตารางการนิเทศนักศึกษา</a></li>
                                                    <li><a href="show_advice.php">ปัญหาที่พบและข้อเสนอแนะจากการนิเทศนักศึกษา</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="confirmRQ.php">อนุมัติแหล่งฝึกงาน</a></li>
                                            <li><a href="company_comfirmRQ.php">เปลี่ยนสถานะตอบรับการฝึก</a></li>
                                            <li><a href="#">ดูรายงาน</a>
                                                <ul>
                                                 <li><a href="show_std_mange.php">ดูรายชื่อนักศึกษา</a></li>
                                                    <li><a href="report_std+company.php">รายชื่อแหล่งฝึกประสบการณ์
                                                            พร้อมทั้งชื่อนักศึกษา</a></li>
                                                             <li><a href="show_request.php">การแจ้งความจำนงขอรับนักศึกษาฝึกประสบการณ์วิชาชีพ
                                                            </a></li>
                                                    <li><a href="Report_Supervision.php">ตารางนิเทศนักศึกษา</a></li>
                                                    <li><a href="report_diary.php">บันทึกประจำวันของนักศึกษา</a></li>
                                                    <li><a href="report_advice.php">ปัญหาที่พบและ
                                                            ข้อเสนอแนะจากหน่วยงาน</a></li>
                                                    <li><a href="show_stdScore.php">ผลการประเมินจากหน่วยงาน</a></li>
                                                    <li><a href="Report_Score.php">คะแนนของนักศึกษา</a></li>
                                                    <li><a href="Report_ScoreSupervision.php">คะแนนการนิเทศ</a></li>
                                                    <li><a href="Report_ScoreDocument.php">คะแนนการดำเนินงานเอกสาร</a></li>
                                                    <li><a href="Report_ScoreActivity.php">คะแนนเข้าร่วมกิจกรรม</a></li>
                                                    <li><a href="Report_ScoreSendDoc.php">คะแนนการส่งเอกสาร</a></li>
                                                    <li><a href="report_grade.php">ผลการประเมิน</a></li>
                                                </ul>
                                            </li>

                                        <? } else { ?>
                                            <li><a href="index.php">หน้าหลัก</a></li>
                                            <li><a href="Webboard.php">เว็บบอร์ด</a></li>
                                            <?php if (!$_SESSION['username']) { ?>
                                                <li><a href="user_login.php">เข้าสู่ระบบ</a></li>
                                                <li><a href="submit3.php">สมัครสมาชิก</a></li>
                                            <?php } else { ?>
                                                <li><a href="user_logout.php">ออกจากระบบ</a></li>
                                                </li><?php } ?>

                                        <? } ?>

                                    </ul>
                                        </div>
                                        <div class="span3" style="height: 50px" align="right">
                                            <?php
                                            if($_SESSION['username']) {
                                            ?>
                                            <p style="margin-top: 16px;">ยินดีต้อนรับ คุณ  <?= $_SESSION['username'] ?> </p>
                                                <?php
                                            }
                                                ?>
                                        </div>
                                       
                                    </div>
    <div  class="clear"></div>
  </div>
</div>

<div class="wrapper row4">
  <div id="container" class="clear">
  
  <!-- InstanceBeginEditable name="center_body" -->
  
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" ></meta>
                                    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" media="screen" />
    <?php
                                    include('connect2.php');

                                    if ($_POST['submit']) {
                                        for ($i = 0; $i < sizeof($_POST['company_id']); $i++) {
                                            $sql = "UPDATE register SET position='" . $_POST['position'][$i] . "' WHERE company_id='" . $_POST['company_id'][$i] . "' ";
                                            $rs = mysql_query($sql);
                                        }
                                    }


                                    $sql = "SELECT *
FROM company, student, register
WHERE company.company_id = register.company_id
AND student.std_id = register.std_id
AND position < 3
ORDER BY company.company_id";

                                    $rs = mysql_query($sql);
									
									
                                    $request = array();
                                    $i = 0;
                                    $j = 0;

                                    $company_id = 0;

                                    while ($row = mysql_fetch_array($rs)) {
                                        
                                       

                                        if ($company_id != $row['company_id']) {

                                            if ($company_id != 0)
                                                $i++;
                                            $j = 0;
                                            $company_id = $row['company_id'];
                                            $request[$i]['company_id'] = $row['company_id'];
                                            $request[$i]['company_name'] = $row['company_name'];
                                            $request[$i]['sts'][$j]['sts_name'] = $row['position'];
                                            $request[$i]['std'][$j]['std_name'] = $row['std_name'];
                                            $request[$i]['register'][$j]['register_id'] = $row['register_id'];
                                        } else if ($company_id == $row['company_id']) {

                                            $j++;
                                            $request[$i]['register'][$j]['register_id'] = $row['register_id'];
                                            $request[$i]['sts'][$j]['sts_name'] = $row['position'];
                                            $request[$i]['std'][$j]['std_name'] = $row['std_name'];
                                        }
                                    }
                                    ?>
                                    <div class="well" id="home">
                                        <p align="center"><strong>อนุมัติคำร้องขอฝึกประสบการณ์</strong></p>
                                      <p>&nbsp;</p>
                                      
                                         <br>
                                        </p>
                                        <table class="table table-bordered">
                                            <tr bgcolor="#00CCFF">

                                                <th style="text-align: center;">หน่วยงาน</th>
                                                <th style="text-align: center;">ชื่อนักศึกษา</th>                                              
                                                <th style="text-align: center;">สถานะ</th>
                                                <th style="text-align: center;">แก้ไขสถานะ</th>
                                                

                                          </tr>


                                            <tr>
                                                <?php for ($i = 0; $i < sizeof($request); $i++) { ?>
                                          <tr valign="top">
                                                        <td style="text-align: center;">
                                                            <input type="hidden" value="<?= $request[$i]['company_id'] ?>" name="company_id[]"/>
                                                            <b> <?= $request[$i]['company_name'] ?></b>
                                                        </td>
                                                        <td >

                                                            <table class="table-teacher" border="0" cellspacing="0" cellpadding="0" width="100%"  >
                                                                <?php for ($j = 0; $j < sizeof($request[$i]['std']); $j++) { ?>
                                                                    <tr><td><p style="text-align: center;"><?= $request[$i]['std'][$j]['std_name'] ?></p></td></tr>
                                                                <?php } ?>
                                                            </table>
                                                        </td>
                                                        <td>
                                                            <table  border="0" cellspacing="0" cellpadding="0" width="100%" >
                                                                <?php for ($j = 0; $j < sizeof($request[$i]['std']); $j++) { ?>
                                                                    <tr><td ><?php
                                                            if ($request[$i]['sts'][$j]['sts_name'] == 0) {

                                                                echo "<p style='color:#838383;text-align:center;'>รอการอนุมัติ</p>";
                                                            } else if ($request[$i]['sts'][$j]['sts_name'] == 1) {
                                                                echo "<p style='color:red;text-align:center;'>ไม่อนุมัติ</p>";
                                                            } else  if ($request[$i]['sts'][$j]['sts_name'] == 2) {
                                                                echo "<p style='color:green;text-align:center;'>อนุมัติ</p>";
                                                            }
                                                                    ?></td></tr>
                                                                        <?php } ?>
                                                            </table>

                                                        </td>
                                                        <td>
                                                            <table  border="0" cellspacing="0" cellpadding="0" width="100%" >
                                                                <?php for ($j = 0; $j < sizeof($request[$i]['std']); $j++) { ?>
                                                                <tr><th style="text-align: center;"><div  style="height: 30px;"><a href="editRQ.php?register_id=<?=$request[$i]['register'][$j]['register_id'] ?>" ><i   class="icon-pencil"></i></a></div></th></tr>
                                                                 <?php } ?>
                                                            </table>

                                                        </td>
                                          </tr>
<?php } ?>
                                                


                                      </table> 
    </div>


 
  <!-- InstanceEndEditable --></div>
</div>


<div class="wrapper">
  <div id="copyright" class="clear">
  <div style="width:960px;margin:0 auto; ">
    <p class="fl_left">Copyright (c) 2012 <a href = "http://webhosting.udru.ac.th/~std52040249439" target="_blank">http://www.gen-dev-soft.com/experience-csit/</a> All rights reserved. 
<br>
Design by Nittaya Kakulphin & Benjawan Sriralat @ Udonthani Rajabhat University.</p>
    </div>
  </div>
</div>
</body>
<!-- InstanceEnd --></html>